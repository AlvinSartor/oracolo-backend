# Oracolo

Oracolo aims to become one of those must-have tools that allow a quick and effective visual analysis of large codebases.   
At the moment it still finds itself in an embryo stage, building solid bases to prepare it for the huge responsibilities a program like this must face.   


# Preview
![preview](Previews/OracoloPreview.png)

# Content

It currently comprends the following projects:

- AdvancedGraph: graph library that support dynamic graphs and pathfinding algorithms
- CodeAnalysis: core of the solution, contains algorithms to analyse, model and store in data structures the entities contained in the codebase
- Extensions: contains shared functionality