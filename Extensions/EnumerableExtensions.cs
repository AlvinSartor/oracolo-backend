﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Extensions
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Executes an action for each element of the collection.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the collection.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="action">The action to execute.</param>
        public static void ForEach<T>([NotNull] this IEnumerable<T> collection, [NotNull] Action<T> action)
        {
            collection.AssertNotNull(nameof(collection));
            action.AssertNotNull(nameof(action));

            foreach (var element in collection)
            {
                action(element);
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <typeparam name="T">The type of objects in the IEnumerable.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="converter">The function to obtain a string from T.</param>
        [NotNull]
        public static string ToString<T>([NotNull] this IEnumerable<T> collection, [NotNull] Func<T, string> converter)
        {
            var result = "";
            collection.ForEach(x => result += converter(x));
            return result;
        }

        /// <summary>
        /// Divides the specified collection using the given discriminative.
        /// </summary>
        /// <typeparam name="T">The type of objects in the IEnumerable.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="discriminative">The discriminative function.</param>
        /// <returns>A tuple containing the original collection divided using the discriminative.</returns>
        public static (IEnumerable<T> positive, IEnumerable<T> negative) Divide<T>(
            [NotNull] this IEnumerable<T> collection, 
            [NotNull] Func<T, bool> discriminative)
        {
            var groups = collection.GroupBy(discriminative).ToList();
            return (groups.Single(x => x.Key).AsEnumerable(), groups.Single(x => !x.Key).AsEnumerable());
        }

        /// <summary>
        /// Returns the maximum element of a collection using a function to calculate its value
        /// </summary>
        /// <typeparam name="T">The type of object in the list.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="valueExtractor">The function to get the value.</param>
        public static T MaxBy<T>([NotNull] this IEnumerable<T> enumerable, [NotNull] Func<T, double> valueExtractor)
        {
            var collection = enumerable.ToList();          
            T currentMax = collection.First();
            double currentMaxValue = valueExtractor(currentMax);

            foreach (var number in collection)
            {
                double tmpValue = valueExtractor(number);
                if (tmpValue > currentMaxValue)
                {
                    currentMax = number;
                    currentMaxValue = tmpValue;
                }
            }

            return currentMax;
        }

        /// <summary>
        /// Returns the minimum element of a collection using a function to calculate its value
        /// </summary>
        /// <typeparam name="T">The type of object in the collection.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="valueExtractor">The function to get the value.</param>
        public static T MinBy<T>([NotNull] this IEnumerable<T> enumerable, [NotNull] Func<T, double> valueExtractor)
        {
            var collection = enumerable.ToList();
            T currentMin = collection.First();
            double currentMinValue = valueExtractor(currentMin);

            foreach (var number in collection)
            {
                double tmpValue = valueExtractor(number);
                if (tmpValue < currentMinValue)
                {
                    currentMin = number;
                    currentMinValue = tmpValue;
                }
            }

            return currentMin;
        }
    }
}