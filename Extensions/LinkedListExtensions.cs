﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Extensions
{
    public static class LinkedListExtensions
    {
        /// <summary>
        /// Pushes a collection of elements on the list, adding at the beginning.
        /// </summary>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="elements">The elements.</param>
        public static void PushFirst<T>([NotNull] this LinkedList<T> list, [NotNull] IEnumerable<T> elements)
        {
            list.AssertNotNull(nameof(list));
            elements.AssertNotNull(nameof(elements));

            foreach (var element in elements.Reverse())
            {
                list.AddFirst(element);
            }
        }

        /// <summary>
        /// Pushes a collection of elements on the list, adding at the end.
        /// </summary>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="elements">The elements.</param>
        public static void PushLast<T>([NotNull] this LinkedList<T> list, [NotNull] IEnumerable<T> elements)
        {
            list.AssertNotNull(nameof(list));
            elements.AssertNotNull(nameof(elements));

            foreach (var element in elements)
            {
                list.AddLast(element);
            }
        }

        /// <summary>
        /// Adds the elment to the ordered list.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="list">The list.</param>s
        /// <param name="element">The element to add.</param>
        /// <param name="comparer">The comparer.</param>
        public static void AddOrdered<T>(
            [NotNull] this LinkedList<T> list,
            [NotNull] T element,
            [NotNull] Func<T, double> comparer)
        {
            list.AssertNotNull(nameof(list));
            element.AssertNotNull(nameof(element));
            comparer.AssertNotNull(nameof(comparer));

            double tValue = comparer(element);
            if (list.Count == 0 || comparer(list.First.Value) > tValue)
            {
                list.AddFirst(element);
            }
            else
            {
                LinkedListNode<T> temp = list.First;
                while (temp != null)
                {
                    if (temp.Next == null || comparer(temp.Next.Value) > tValue)
                    {
                        list.AddAfter(temp, element);
                        temp = null;
                    }
                    else
                    {
                        temp = temp.Next;
                    }
                }
            }
        }

        /// <summary>
        /// Pops the first element of the list.
        /// </summary>
        /// <typeparam name="T">Type of the elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The first element of the list.</returns>
        public static T PopFirst<T>([NotNull] this LinkedList<T> list)
        {
            list.AssertNotNull(nameof(list));

            var element = list.First;
            list.RemoveFirst();
            return element.Value;
        }

        /// <summary>
        /// Pops the last element of the list.
        /// </summary>
        /// <typeparam name="T">Type of the elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The last element of the list.</returns>
        public static T PopLast<T>([NotNull] this LinkedList<T> list)
        {
            list.AssertNotNull(nameof(list));

            var element = list.Last;
            list.RemoveLast();
            return element.Value;
        }

    }
}