﻿using UnityEngine;

namespace Extensions.Geometry
{
    public struct Segment
    {
        public readonly Vector3 A;
        public readonly Vector3 B;

        public Segment(Vector3 a, Vector3 b)
        {
            A = a;
            B = b;
        }

        public Segment Swap()
        {
            return new Segment(B, A);
        }

        public Vector3 TrasposeToOrigin => B - A;

        public Vector3 LerpClamped(float value) => Vector3.Lerp(A, B, value);

        public override string ToString() => $"{A} - {B}";

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is Segment s && Equals(s);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return (A.GetHashCode() * 397) ^ B.GetHashCode();
            }
        }

        public bool Equals(Segment other)
        {
            return other.A.Equals(A) && other.B.Equals(B) || other.A.Equals(B) && other.B.Equals(A);
        }        
    }
}
