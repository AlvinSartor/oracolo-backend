﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace Extensions.Geometry
{
    public static class Geometry
    {
        public static (Vector3, Vector3) GetConnectionPointsBetweenRectangles(Vector3 startCenter, Vector3 endCenter, Vector2 sizes)
        {
            var line = new Segment(startCenter, endCenter);
            Vector3 firstPoint = GetImageSegments(startCenter, sizes)
                                  .Select(s => GetIntersection(s, line))
                                  .FirstOrDefault(s => s != null)
                                  ?? GetImageSegments(startCenter, sizes).First().LerpClamped(0.5f);

            Vector3 secondPoint = GetImageSegments(endCenter, sizes)
                                   .Select(s => GetIntersection(s, line))
                                   .FirstOrDefault(s => s != null)
                                   ?? GetImageSegments(endCenter, sizes).First().LerpClamped(0.5f);
            
            return (firstPoint, secondPoint);
        }

        [NotNull]
        public static IEnumerable<Segment> GetImageSegments(Vector3 center, Vector2 bounds)
        {
            float wid = bounds.x;
            float hei = bounds.y;

            var p1 = new Vector3(center.x - wid / 2, center.y + hei / 2, center.z);
            var p2 = new Vector3(center.x + wid / 2, center.y + hei / 2, center.z);
            yield return new Segment(p1, p2);

            p1 = new Vector3(center.x + wid / 2, center.y - hei / 2, center.z);
            p2 = new Vector3(center.x + wid / 2, center.y + hei / 2, center.z);
            yield return new Segment(p1, p2);

            p1 = new Vector3(center.x - wid / 2, center.y - hei / 2, center.z);
            p2 = new Vector3(center.x + wid / 2, center.y - hei / 2, center.z);
            yield return new Segment(p1, p2);

            p1 = new Vector3(center.x - wid / 2, center.y - hei / 2, center.z);
            p2 = new Vector3(center.x - wid / 2, center.y + hei / 2, center.z);

            yield return new Segment(p1, p2);
        }

        public static Vector3? GetIntersection(Segment edge, Segment line)
        {
            // p + t * r = q + u * s
            // t = (q − p) × s / (r × s)
            // u = (q − p) × r / (r × s)
            // res = p + t * r

            Vector3 p = edge.A;
            Vector3 r = edge.TrasposeToOrigin;

            Vector3 q = line.A;
            Vector3 s = line.TrasposeToOrigin;

            float dotrs = Dot(r, s);
            float dotqps = Dot(q - p, s);
            float dotqpr = Dot(q - p, r);

            if (dotrs == 0 && dotqpr != 0)
            {
                // The two segments are parallel.
                return null;
            }

            float t = dotqps / dotrs;
            float u = dotqpr / dotrs; 
            if (t > 1 || t < 0 || u > 1 || u < 0)
            {
                // The two segments do not intersect.
                return null;
            }

            return p + t * r;
        }

        private static float Dot(Vector3 v1, Vector3 v2) => v1.x * v2.y - v2.x * v1.y;

        public static (Vector3, Vector3, Vector3, Vector3) GetArrow(Vector3 origin, Vector3 arrowPoint, float arrowBaseSize, float arrowLengthSize)
        {
            Vector3 slope = (arrowPoint - origin);

            float lineLenght = Vector3.Distance(origin, arrowPoint);
            Vector3 arrowBase = Vector3.Lerp(origin, arrowPoint, (lineLenght - arrowLengthSize) / lineLenght);

            Vector3 perpendicularSlope1 = slope;
            perpendicularSlope1.x *= -1;
            Vector3 perpendicularSlope2 = slope;
            perpendicularSlope2.y *= -1;

            Vector3 base1 = arrowBase + perpendicularSlope1.normalized * arrowBaseSize / 2;
            Vector3 base2 = arrowBase + perpendicularSlope2.normalized * arrowBaseSize / 2;

            return (arrowPoint, base1, base2, arrowPoint);
        } 
    }
}