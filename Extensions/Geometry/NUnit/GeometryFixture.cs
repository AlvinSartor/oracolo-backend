using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;

namespace Extensions.Geometry.NUnit
{
    [TestFixture]
    internal sealed class GeometryFixture
    {
        [Test]
        public void IntersectionBetweenSegmentsIsFound()
        {
            var edge = new Segment(new Vector3(0, 0, 0), new Vector3(0, 10, 0));
            var line = new Segment(new Vector3(5, 5, 0), new Vector3(-5, 5, 0));

            Vector3 intersection = Geometry.GetIntersection(edge, line).GetValueOrDefault();
            Assert.That(intersection, Is.EqualTo(new Vector3(0, 5, 0)));

            Vector3 inverse = Geometry.GetIntersection(line, edge).GetValueOrDefault();
            Assert.That(inverse, Is.EqualTo(new Vector3(0, 5, 0)));

            Assert.That(inverse, Is.EqualTo(intersection));
        }

        [Test]
        public void ErrorIsThrownWhenThereIsNoIntersection()
        {
            var edge = new Segment(new Vector3(0, 0, 0), new Vector3(0, -10, 0));
            var line = new Segment(new Vector3(5, 5, 0), new Vector3(-5, 5, 0));

            Assert.IsNull(Geometry.GetIntersection(edge, line));

            var paralleleLine = new Segment(new Vector3(1, 0, 0), new Vector3(1, -10, 0));

            Assert.IsNull(Geometry.GetIntersection(edge, paralleleLine));
        }

        [Test]
        public void TheDepthOfTheSegmentIsTakenForTheIntersection()
        {
            var edge = new Segment(new Vector3(0, 0, 10), new Vector3(0, 10, 10));
            var line = new Segment(new Vector3(5, 5, 0), new Vector3(-5, 5, 0));

            Vector3 intersection = Geometry.GetIntersection(edge, line).GetValueOrDefault();
            Assert.That(intersection, Is.EqualTo(new Vector3(0, 5, 10)));

            Vector3 inverse = Geometry.GetIntersection(line, edge).GetValueOrDefault();
            Assert.That(inverse, Is.EqualTo(new Vector3(0, 5, 0)));

            Assert.That(inverse, Is.Not.EqualTo(intersection));
        }

        [Test]
        public void RectSidesAreCalculatedCorrectly()
        {
            var center = new Vector3(0, 0, 0);
            var size = new Vector2(100, 60);

            List<Segment> sides = Geometry.GetImageSegments(center, size).ToList();
            List<Segment> expectedSides = new List<Segment>
            {
                new Segment(new Vector3(-50, 30, 0), new Vector3(50, 30, 0)),
                new Segment(new Vector3(50, 30, 0), new Vector3(50, -30, 0)),
                new Segment(new Vector3(-50, -30, 0), new Vector3(50, -30, 0)),
                new Segment(new Vector3(-50, 30, 0), new Vector3(-50, -30, 0))
            };

            CollectionAssert.AreEquivalent(expectedSides, sides);
        }

        [Test]
        public void ConnectionPointsAreDetectedIfRectsAreInDisposedVertically()
        {
            var size = new Vector2(100, 50);

            var centerRect1 = new Vector3(0, 0, 0);
            var centerRect2 = new Vector3(0, 100, 0);

            (Vector3 p1, Vector3 p2) = Geometry.GetConnectionPointsBetweenRectangles(centerRect1, centerRect2, size);

            Assert.That(p1, Is.EqualTo(new Vector3(0, 25, 0)));
            Assert.That(p2, Is.EqualTo(new Vector3(0, 75, 0)));
        }

        [Test]
        public void ConnectionPointsAreDetectedIfRectsAreDisposedHorizontally()
        {
            var size = new Vector2(100, 50);

            var centerRect1 = new Vector3(0, 25, 0);
            var centerRect2 = new Vector3(150, 25, 0);

            (Vector3 p1, Vector3 p2) = Geometry.GetConnectionPointsBetweenRectangles(centerRect1, centerRect2, size);

            Assert.That(p1, Is.EqualTo(new Vector3(50, 25, 0)));
            Assert.That(p2, Is.EqualTo(new Vector3(100, 25, 0)));
        }

        [Test]
        public void ConnectionPointsAreDetectedIfRectsAreInDisposedVerticallyWithDepth()
        {
            var size = new Vector2(100, 50);

            var centerRect1 = new Vector3(0, 0, 60);
            var centerRect2 = new Vector3(0, 100, -30);

            (Vector3 p1, Vector3 p2) = Geometry.GetConnectionPointsBetweenRectangles(centerRect1, centerRect2, size);

            Assert.That(p1, Is.EqualTo(new Vector3(0, 25, 60)));
            Assert.That(p2, Is.EqualTo(new Vector3(0, 75, -30)));
        }

        [Test]
        public void ConnectionPointsAreDetectedIfRectsAreDisposedHorizontallyWithDepth()
        {
            var size = new Vector2(100, 50);

            var centerRect1 = new Vector3(0, 25, 60);
            var centerRect2 = new Vector3(150, 25, -30);

            (Vector3 p1, Vector3 p2) = Geometry.GetConnectionPointsBetweenRectangles(centerRect1, centerRect2, size);

            Assert.That(p1, Is.EqualTo(new Vector3(50, 25, 60)));
            Assert.That(p2, Is.EqualTo(new Vector3(100, 25, -30)));
        }

        [Test]
        public void IfThereIsNoIntersectionThenTheUpperSideIsUsed()
        {
            var size = new Vector2(100, 50);

            var centerRect1 = new Vector3(0, 25, 60);
            var centerRect2 = new Vector3(5, 30, -30);

            (Vector3 p1, Vector3 p2) = Geometry.GetConnectionPointsBetweenRectangles(centerRect1, centerRect2, size);

            Assert.That(p1, Is.EqualTo(new Vector3(0, 50, 60)));
            Assert.That(p2, Is.EqualTo(new Vector3(5, 55, -30)));
        }

        [Test]
        public void ArrowIsDrawnOnPerpendicularPoints()
        {
            var origin = new Vector3(0, 0, 0);
            var arrowPoint = new Vector3(4, 4, 4);

            const float arrowLengthSize = 1;
            const float arrowBaseSize = 1;

            (Vector3, Vector3, Vector3, Vector3) arrow = Extensions.Geometry.Geometry.GetArrow(origin, arrowPoint, arrowBaseSize, arrowLengthSize);
            
            Assert.That(arrow.Item1, Is.EqualTo(arrowPoint));
            Assert.That(arrow.Item4, Is.EqualTo(arrowPoint));
        }
    }
}