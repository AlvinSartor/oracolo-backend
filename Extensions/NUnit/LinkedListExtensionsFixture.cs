﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Extensions.NUnit
{
    [TestFixture]
    internal sealed class LinkedListExtensionsFixture
    {
        [Test]
        public void PushTest()
        {
            var list = new LinkedList<int>(new[] {1, 2, 3});
            list.PushFirst(new []{-2, -1, 0});

            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { -2, -1, 0, 1, 2, 3 })));

            list = new LinkedList<int>(new[] { 1, 2, 3 });
            list.PushLast(new[] { 4, 5, 6 });

            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { 1, 2, 3, 4, 5, 6 })));
        }

        [Test]
        public void PopTest()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3 });
            int element = list.PopFirst();

            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { 2, 3 })));
            Assert.That(element, Is.EqualTo(1));

            list = new LinkedList<int>(new[] { 1, 2, 3 });
            element = list.PopLast();

            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { 1, 2 })));
            Assert.That(element, Is.EqualTo(3));
        }

        [Test]
        public void AddOrderedTest()
        {
            var list = new LinkedList<int>(new[] { 1, 2, 3 });
           
            list.AddOrdered(0, i => i);
            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { 0, 1, 2, 3 })));

            list.AddOrdered(4, i => i);
            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { 0, 1, 2, 3, 4 })));

            list.AddOrdered(2, i => i);
            Assert.That(list, Is.EqualTo(new LinkedList<int>(new[] { 0, 1, 2, 2, 3, 4 })));
        }

    }
}
