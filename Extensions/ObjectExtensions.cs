﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using JetBrains.Annotations;

namespace Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Puts the specified element in an IEnumerable.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="element">The element.</param>
        [NotNull]
        public static IEnumerable<T> Enumerate<T>([NotNull] this T element)
        {
            return new [] {element};
        }

        /// <summary>
        /// Puts the specified element in an ImmutableList.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="element">The element.</param>
        [NotNull]
        public static ImmutableList<T> Enlist<T>([NotNull] this T element)
        {
            return new[] { element }.ToImmutableList();
        }

        /// <summary>
        /// Applies the specified action to the element.
        /// Function that allows concatenation of actions.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="element">The element.</param>
        /// <param name="action">The action to apply using the element.</param>
        public static void Apply<T>([NotNull] this T element, [NotNull] Action<T> action)
        {
            action(element);
        }

        /// <summary>
        /// Applies the specified function to the element.
        /// Function that allows concatenation of functions.
        /// </summary>
        /// <typeparam name="TInput">The element type.</typeparam>
        /// <typeparam name="TOutput">The element type.</typeparam>
        /// <param name="element">The element.</param>
        /// <param name="function">The function to apply using the element.</param>
        public static TOutput Apply<TInput, TOutput>([NotNull] this TInput element, [NotNull] Func<TInput, TOutput> function)
        {
            return function(element);
        }
    }
}
