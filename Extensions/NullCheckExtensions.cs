﻿using System;
using JetBrains.Annotations;
// ReSharper disable ParameterOnlyUsedForPreconditionCheck.Global

namespace Extensions
{
    public static class NullCheckExtensions
    {
        /// <summary>
        /// Asserts that the instance is not null.
        /// </summary>
        /// <param name="instance">The instance to check.</param>
        /// <param name="variableName">Name of the variable, for the message.</param>
        /// <exception cref="ArgumentException">The element is null.</exception>
        [NotNull, Pure]
        public static T NotNull<T>(this T instance, [NotNull] string variableName)
        {
            return instance != null ? instance : throw new ArgumentException($"{variableName} is null.");
        }

        /// <summary>
        /// Asserts that the instance is not null.
        /// </summary>
        /// <param name="instance">The instance to check.</param>
        /// <param name="variableName">Name of the variable, for the message.</param>
        /// <exception cref="ArgumentException">The element is null.</exception>
        public static void AssertNotNull<T>([NoEnumeration] this T instance, [NotNull] string variableName)
        {
            if (instance == null)
            {
                throw new ArgumentException($"{variableName} is null.");
            }
        }
    }
}