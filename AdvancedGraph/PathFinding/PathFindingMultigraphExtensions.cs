﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph.PathFinding
{
    public static class PathFindingMultigraphExtensions
    {
        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="multigraph">The collection of graphs.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="final">The final value.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        [NotNull]
        public static async Task<Path<T>> AStarSearch<T>(
            [NotNull] this ImmutableList<IGraph<T>> multigraph,
            [NotNull] T initial,
            [NotNull] T final,
            [NotNull] Func<T, double> getHeuristic)
        {
            ThrowIfNodeDoesNotBelongToGraph(multigraph, initial);

            Dictionary<INode<T>, double> pathCosts = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, double> pathHeuristics = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, INode<T>> parents = new Dictionary<INode<T>, INode<T>>();

            LinkedList<INode<T>> open = new LinkedList<INode<T>>();
            HashSet<INode<T>> closed = new HashSet<INode<T>>();

            INode<T> initialNode = multigraph.First(graph => graph.Contains(initial))[initial];

            open.AddFirst(initialNode);
            pathCosts[initialNode] = 0;
            pathHeuristics[initialNode] = getHeuristic(initial);
            parents[initialNode] = null;

            while (open.Count > 0)
            {
                INode<T> actual = open.PopFirst();
                closed.Add(actual);

                if (actual.Value.Equals(final))
                {
                    return new PathMultigraph<T>(multigraph, parents, actual);
                }

                IEnumerable<IEdge<T>> neighbors = await GetNeighborsInMultigraph(multigraph, actual);

                foreach (var edge in neighbors)
                {
                    double gq = pathCosts[actual] + edge.Weight;

                    if (!pathCosts.ContainsKey(edge.DestinationNode) || gq < pathCosts[edge.DestinationNode])
                    {
                        parents[edge.DestinationNode] = actual;
                        pathCosts[edge.DestinationNode] = gq;
                        pathHeuristics[edge.DestinationNode] = gq + getHeuristic(edge.DestinationNode.Value);
                        open.AddOrdered(edge.DestinationNode, e => pathHeuristics[e]);
                        if (closed.Contains(edge.DestinationNode))
                        {
                            closed.Remove(edge.DestinationNode);
                        }
                    }
                }
            }

            return PathMultigraph<T>.Empty;
        }

        private static async Task<IEnumerable<IEdge<T>>> GetNeighborsInMultigraph<T>(
            [NotNull] ImmutableList<IGraph<T>> multigraph, 
            [NotNull] INode<T> actual)
        {
            var neighbors = new Dictionary<T, IEdge<T>>();
            foreach (var graph in multigraph)
            {
                if (graph.Contains(actual.Value))
                {
                    var graphNeighbors = await graph[actual.Value].GetAllNeighbors();
                    graphNeighbors.ForEach(neighbor =>
                    {
                        if (!neighbors.ContainsKey(neighbor.Value))
                        {
                            neighbors.Add(neighbor.Value, graph[actual.Value].Edges[neighbor.Value]);
                        }
                    });
                }
            }

            return neighbors.Values;
        }

        private static void ThrowIfNodeDoesNotBelongToGraph<T>([NotNull] ImmutableList<IGraph<T>> multigraph, [NotNull] T value)
        {
            if (!multigraph.Any(graph => graph.Contains(value)))
            {
                throw new ArgumentException($"The item {value} does not belong to the multigraph.");
            }
        }
    }
}