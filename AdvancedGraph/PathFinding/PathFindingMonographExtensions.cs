﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph.PathFinding
{
    public static class PathFindingMonographExtensions
    {
        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        [Obsolete("This implementation has higher computational complexity. " +
                  "If tests show that the newer implementation has the same results, this will be retired.")]
        public static async Task<Path<T>> AStarSearchOld<T>(
            [NotNull] this IGraph<T> graph,
            [NotNull] INode<T> initial,
            [NotNull] INode<T> final,
            [NotNull] Func<INode<T>, double> getHeuristic)
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            Dictionary<INode<T>, double> pathCosts = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, double> pathHeuristics = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, INode<T>> parents = new Dictionary<INode<T>, INode<T>>();

            LinkedList<INode<T>> open = new LinkedList<INode<T>>();
            HashSet<INode<T>> closed = new HashSet<INode<T>>();

            open.AddFirst(initial);
            pathCosts[initial] = 0;
            parents[initial] = null;

            while (open.Count > 0)
            {
                INode<T> actual = open.PopFirst();
                closed.Add(actual);

                if (actual.Equals(final))
                {
                    return new PathMonograph<T>(parents, final);
                }

                ImmutableHashSet<INode<T>> successors = (await actual.GetAllNeighbors()).ToImmutableHashSet();
                while (successors.Count > 0)
                {
                    INode<T> q = successors.First();
                    successors = successors.Remove(q);

                    double gq = pathCosts[actual] + actual.Edges[q.Value].Weight;

                    bool openContainsQ = open.Contains(q);
                    bool closedContainsQ = closed.Contains(q);

                    if (!openContainsQ && !closedContainsQ)
                    {
                        parents[q] = actual;
                        pathHeuristics[q] = gq + getHeuristic(q);
                        pathCosts[q] = gq;
                        open.AddOrdered(q, e => pathHeuristics[e]);
                    }
                    else if (pathCosts.ContainsKey(q) && gq < pathCosts[q])
                    {
                        parents[q] = actual;
                        pathCosts[q] = gq;
                        pathHeuristics[q] = gq + getHeuristic(q);
                        if (closedContainsQ)
                        {
                            closed.Remove(q);
                            open.AddOrdered(q, e => pathHeuristics[e]);
                        }
                    }
                }
            }

            return Path<T>.Empty;
        }

        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        [NotNull]
        public static async Task<Path<T>> AStarSearch<T>(
            [NotNull] this IGraph<T> graph,
            [NotNull] INode<T> initial,
            [NotNull] INode<T> final,
            [NotNull] Func<T, double> getHeuristic)
        {
            return await AStarSearch(graph, initial.Value, final.Value, getHeuristic);
        }

        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="final">The final value.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        [NotNull]
        public static async Task<Path<T>> AStarSearch<T>(
            [NotNull] this IGraph<T> graph,
            [NotNull] T initial,
            [NotNull] T final,
            [NotNull] Func<T, double> getHeuristic)
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            Dictionary<INode<T>, double> pathCosts = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, double> pathHeuristics = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, INode<T>> parents = new Dictionary<INode<T>, INode<T>>();

            LinkedList<INode<T>> open = new LinkedList<INode<T>>();
            HashSet<INode<T>> closed = new HashSet<INode<T>>();

            INode<T> initialNode = graph[initial];

            open.AddFirst(initialNode);
            pathCosts[initialNode] = 0;
            pathHeuristics[initialNode] = getHeuristic(initial);
            parents[initialNode] = null;

            while (open.Count > 0)
            {
                INode<T> actual = open.PopFirst();
                closed.Add(actual);

                if (actual.Value.Equals(final))
                {
                    return new PathMonograph<T>(parents, actual);
                }

                var neighbours = await actual.GetAllNeighbors();
                foreach (var q in neighbours)
                {
                    double gq = pathCosts[actual] + actual.Edges[q.Value].Weight;

                    if (!pathCosts.ContainsKey(q) || gq < pathCosts[q])
                    {
                        parents[q] = actual;
                        pathCosts[q] = gq;
                        pathHeuristics[q] = gq + getHeuristic(q.Value);
                        open.AddOrdered(q, e => pathHeuristics[e]);
                        if (closed.Contains(q))
                        {
                            closed.Remove(q);
                        }
                    }
                }
            }

            return Path<T>.Empty;
        }

        /// <summary>
        /// Performs a best-first search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        [NotNull]
        public static async Task<Path<T>> BestFirstSearch<T>(
            [NotNull] this IGraph<T> graph,
            [NotNull] INode<T> initial,
            [NotNull] INode<T> final)
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            Dictionary<INode<T>, double> pathCosts = new Dictionary<INode<T>, double>();
            Dictionary<INode<T>, INode<T>> parents = new Dictionary<INode<T>, INode<T>>();

            LinkedList<INode<T>> open = new LinkedList<INode<T>>();
            HashSet<INode<T>> closed = new HashSet<INode<T>>();

            open.AddFirst(initial);
            pathCosts[initial] = 0;
            parents[initial] = null;

            while (open.Count > 0)
            {
                INode<T> actual = open.PopFirst();
                closed.Add(actual);

                if (actual.Equals(final))
                {
                    return new PathMonograph<T>(parents, final);
                }

                var neighbours = await actual.GetAllNeighbors();
                foreach (var q in neighbours)
                {
                    double gq = pathCosts[actual] + actual.Edges[q.Value].Weight;

                    if (!pathCosts.ContainsKey(q) || gq < pathCosts[q])
                    {
                        parents[q] = actual;
                        pathCosts[q] = gq;
                        open.AddOrdered(q, e => pathCosts[e]);
                        if (closed.Contains(q))
                        {
                            closed.Remove(q);
                        }
                    }
                }
            }

            return Path<T>.Empty;
        }

        /// <summary>
        /// Performs a first-depth search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        [NotNull]
        public static async Task<Path<T>> FirstDepthSearch<T>(
            [NotNull] this IGraph<T> graph,
            [NotNull] INode<T> initial,
            [NotNull] INode<T> final)
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            LinkedList<INode<T>> open = new LinkedList<INode<T>>();
            LinkedList<INode<T>> closed = new LinkedList<INode<T>>();

            open.AddFirst(initial);

            while (open.Count > 0)
            {
                INode<T> actual = open.PopFirst();
                closed.AddLast(actual);

                if (actual.Equals(final))
                {
                    return new PathMonograph<T>(closed);
                }

                var neighbours = await actual.GetAllNeighbors();
                IEnumerable<INode<T>> successors = neighbours.Except(open.Union(closed));
                open.PushFirst(successors);
            }

            return Path<T>.Empty;
        }

        private static void ThrowIfNodeDoesNotBelongToGraph<T>([NotNull] IGraph<T> graph, [NotNull] INode<T> node)
        {
            if (!graph.Contains(node.Id))
            {
                throw new ArgumentException($"The node {node} does not belong to the graph {graph}.");
            }
        }

        private static void ThrowIfNodeDoesNotBelongToGraph<T>([NotNull] IGraph<T> graph, [NotNull] T value)
        {
            if (!graph.Contains(value))
            {
                throw new ArgumentException($"The item {value} does not belong to the graph {graph}.");
            }
        }
    }
}