﻿using System.Collections.Generic;
using System.Collections.Immutable;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph.PathFinding
{
    public sealed class PathMonograph<T> : Path<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Path{T}"/> class.
        /// </summary>
        /// <param name="edges">The edges.</param>
        internal PathMonograph([NotNull] ImmutableList<IEdge<T>> edges)
        {
            Edges = edges;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Path{T}"/> class.
        /// </summary>
        /// <param name="nodes">The explored nodes. The last node of the list is considered the destination and the first is the start.</param>
        internal PathMonograph([NotNull] LinkedList<INode<T>> nodes)
        {
            var result = new List<IEdge<T>>();
            INode<T> destination = nodes.PopLast();

            while (nodes.Count > 0)
            {
                INode<T> actual = nodes.PopLast();
                if (actual.IsConnectedTo(destination))
                {
                    result.Add(actual.Edges[destination.Value]);
                    destination = actual;
                }
            }

            Edges = result.ToImmutableList().Reverse();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Path{T}"/> class.
        /// </summary>
        /// <param name="parents">A dictionary containing the parents (values) of the nodes (keys).</param>
        /// <param name="destination">The destination node.</param>
        internal PathMonograph([NotNull] IReadOnlyDictionary<INode<T>, INode<T>> parents, [NotNull] INode<T> destination)
        {
            var result = new List<IEdge<T>>();
            INode<T> actual = destination;
            INode<T> previous = parents[actual];

            while (previous != null)
            {
                result.Add(previous.Edges[actual.Value]);

                actual = previous;
                previous = parents[actual];
            }

            Edges = result.ToImmutableList().Reverse();
        }
    }
}