﻿using System.Collections.Generic;
using System.Collections.Immutable;
using AdvancedGraph.Interfaces;
using JetBrains.Annotations;
using NUnit.Framework;

namespace AdvancedGraph.PathFinding.NUnit
{
    [TestFixture]
    internal sealed class PathMonographFixture : PathFixture
    {
        /// <inheritdoc />
        protected override Path<int> CreatePath(int initialNodeValue, params (int node, double weight)[] nodes)
        {
            var graph = new Graph<int>();
            Node<int> previous = graph.Add(initialNodeValue);
            
            var edges = new List<IEdge<int>>();
            foreach ((var nodeValue, var weight) in nodes)
            {
                Node<int> node = graph.Add(nodeValue);
                previous.AddConnection(node, weight);

                edges.Add(previous.Edges[nodeValue]);
                previous = node;
            }

            return new PathMonograph<int>(edges.ToImmutableList());
        }

        /// <summary>
        /// Function to check multiple constructors.
        /// It expects the path: 42 ->(10) 38 ->(7) 12
        /// </summary>
        /// <param name="path">The path.</param>
        private void PathContainsExpectedValues([NotNull] Path<int> path)
        {            
            Assert.That(path.Cost, Is.EqualTo(17));
            Assert.That(path.Edges.Count, Is.EqualTo(2));
            Assert.That(path.Nodes.Count, Is.EqualTo(3));

            Assert.That(path.Nodes[0].Value, Is.EqualTo(42));
            Assert.That(path.Nodes[1].Value, Is.EqualTo(38));
            Assert.That(path.Nodes[2].Value, Is.EqualTo(12));
        }

        [Test]
        public void PathInitializedWithLinkedListContainsCorrectValues()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);
            Node<int> node12 = graph.Add(12);

            node42.AddConnection(node38, 10);
            node38.AddConnection(node12, 7);

            Path<int> path = new PathMonograph<int>(new LinkedList<INode<int>>(new[]
            {
                node42,
                node38,
                node12
            }));

            PathContainsExpectedValues(path);            
        }

        [Test]
        public void PathInitializedWithDictionaryContainsCorrectValues()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);
            Node<int> node12 = graph.Add(12);
            Node<int> node99 = graph.Add(99);

            node42.AddConnection(node38, 10);
            node38.AddConnection(node12, 7);

            var path = new PathMonograph<int>(new Dictionary<INode<int>, INode<int>>
            {
                {node42, null },
                {node38, node42 },
                {node12, node38 },
                {node99, node99 }
            }, node12);

            PathContainsExpectedValues(path);
        }
    }
}
