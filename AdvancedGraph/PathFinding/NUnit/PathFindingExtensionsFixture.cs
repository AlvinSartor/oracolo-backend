﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using NUnit.Framework;

namespace AdvancedGraph.PathFinding.NUnit
{
    [TestFixture]
    internal sealed class PathFindingExtensionsFixture
    {
        private static readonly Dictionary<char, double> s_Heuristic = new Dictionary<char, double>
        {
            {'A', 40d},
            {'B', 30d},
            {'C', 25d},
            {'D', 20d},
            {'E', 9d},
            {'F', 10d},
            {'G', 18d},
            {'H', 0d}
        };

        private static double GetHeuristic(char value)
        {
            return s_Heuristic[value];
        }

        [NotNull]
        private static Graph<char> GetGraph()
        {
            var graph = new Graph<char>();

            Node<char> nodeA = graph.Add('A');
            Node<char> nodeB = graph.Add('B');
            Node<char> nodeC = graph.Add('C');
            Node<char> nodeD = graph.Add('D');
            Node<char> nodeE = graph.Add('E');
            Node<char> nodeF = graph.Add('F');
            Node<char> nodeG = graph.Add('G');
            Node<char> nodeH = graph.Add('H');

            nodeA.AddConnection(nodeB, 5d);
            nodeA.AddConnection(nodeC, 8d);

            nodeB.AddConnection(nodeD, 6d);
            nodeB.AddConnection(nodeF, 10d);

            nodeC.AddConnection(nodeD, 2d);
            nodeC.AddConnection(nodeG, 7d);

            nodeD.AddConnection(nodeE, 4d);

            nodeE.AddConnection(nodeF, 2d);
            nodeE.AddConnection(nodeH, 5d);
            nodeE.AddConnection(nodeG, 3d);

            nodeF.AddConnection(nodeH, 9d);

            nodeG.AddConnection(nodeH, 6d);

            return graph;
        }


        // DEPTH FIRST

        [Test]
        public void DepthFirstThrowsIfNodeIsNotInGraph()
        {
            Graph<int> graph = new Graph<int>();
            Node<int> node1 = new Node<int>(graph, 1);
            Node<int> node2 = new Node<int>(graph, 2);

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await graph.FirstDepthSearch(node1, node2);
            });
            Assert.That(exception.Message.Contains("does not belong"));

            Node<int> node3 = graph.Add(3);
            node3.AddConnection(node2);
            Assert.DoesNotThrowAsync(async () =>
            {
                var _ = await graph.FirstDepthSearch(node3, node2);
            });
        }

        [Test]
        public async Task DepthFirstReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var graph = new Graph<int>();
            Node<int> node1 = graph.Add(1);
            Node<int> node2 = graph.Add(2);

            var path = await graph.FirstDepthSearch(node1, node2);            
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task DepthFirstGetsTheRightPath()
        {
            Graph<char> graph = GetGraph();
            Node<char> initial = graph.GetNode('A');
            Node<char> final = graph.GetNode('H');

            var path = await graph.FirstDepthSearch(initial, final);
            for (int i = 0; i < path.Nodes.Count - 1; i++)
            {
                var current = path.Nodes[i];
                var next = path.Nodes[i + 1];

                Assert.That(current.IsConnectedTo(next));
            }

            Assert.That(path.Nodes.First().Value == 'A');
            Assert.That(path.Nodes.Last().Value == 'H');
        }

        // A Star

        [Test]
        public void AStarThrowsIfNodeIsNotInGraph()
        {
            var graph = new Graph<int>();
            Node<int> node1 = new Node<int>(graph, 1);
            Node<int> node2 = new Node<int>(graph, 2);

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await graph.AStarSearch(node1, node2, n => 0);
            });
            Assert.That(exception.Message.Contains("does not belong"));

            Node<int> node3 = graph.Add(3);
            node3.AddConnection(node2);
            Assert.DoesNotThrowAsync(async () =>
            {
                var _ = await graph.AStarSearch(node3, node2, n => 1);
            });
        }

        [Test]
        public async Task AStarReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var graph = new Graph<int>();
            Node<int> node1 = graph.Add(1);
            Node<int> node2 = graph.Add(2);

            var path = await graph.AStarSearch(node1, node2, node => 1);
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task AStarGetsTheRightPath()
        {
            Graph<char> graph = GetGraph();
            Node<char> initial = graph.GetNode('A');
            Node<char> final = graph.GetNode('H');

            var path = await graph.AStarSearch(initial, final, GetHeuristic);

            Assert.That(path.ToString(x => x.ToString()), Is.EqualTo("A -> C -> D -> E -> H"));
            Assert.That(path.Cost, Is.EqualTo(19));
        }

        // BEST FIRST

        [Test]
        public void BestFirstThrowsIfNodeIsNotInGraph()
        {
            Graph<int> graph = new Graph<int>();
            Node<int> node1 = new Node<int>(graph, 1);
            Node<int> node2 = new Node<int>(graph, 2);

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await graph.BestFirstSearch(node1, node2);
            });
            Assert.That(exception.Message.Contains("does not belong"));

            Node<int> node3 = graph.Add(3);
            node3.AddConnection(node2);
            Assert.DoesNotThrowAsync(async () =>
            {
                var _ = await graph.BestFirstSearch(node3, node2);
            });
        }

        [Test]
        public async Task BestFirstReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var graph = new Graph<int>();
            Node<int> node1 = graph.Add(1);
            Node<int> node2 = graph.Add(2);

            var path = await graph.BestFirstSearch(node1, node2);
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task BestFirstGetsTheRightPath()
        {
            Graph<char> graph = GetGraph();
            Node<char> initial = graph.GetNode('A');
            Node<char> final = graph.GetNode('H');

            var path = await graph.BestFirstSearch(initial, final);

            Assert.That(path.ToString(x => x.ToString()), Is.EqualTo("A -> C -> D -> E -> H"));
            Assert.That(path.Cost, Is.EqualTo(19));
        }
    }
}