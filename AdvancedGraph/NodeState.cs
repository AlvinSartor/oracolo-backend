﻿using System;
using JetBrains.Annotations;

namespace AdvancedGraph
{
    internal sealed class NodeState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NodeState"/> class.
        /// </summary>
        /// <param name="isDirty">if set to <c>true</c> the node is dirty (needs to be explored).</param>
        /// <param name="lastUpdate">The time of the last update.</param>
        /// <param name="dateAdded">The addition time.</param>
        public NodeState(bool isDirty, DateTime lastUpdate, DateTime dateAdded)
        {
            IsDirty = isDirty;
            LastUpdate = lastUpdate;
            DateAdded = dateAdded;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public bool IsDirty { get; }

        /// <summary>
        /// Gets the time of the last update.
        /// </summary>
        public DateTime LastUpdate { get; }

        /// <summary>
        /// Gets the time this node has been addede added.
        /// </summary>
        public DateTime DateAdded { get; }

        /// <summary>
        /// Returns a new state with the specified dirty value.
        /// </summary>
        /// <param name="dirty">if set to <c>true</c> the node is dirty.</param>
        [NotNull, Pure]
        public NodeState WithUpdate(bool dirty)
        {
            return new NodeState(dirty, DateTime.UtcNow, DateAdded);
        }
    }
}