﻿using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace AdvancedGraph.Interfaces
{
    public interface INode<T> : IEquatable<INode<T>>
    {
        /// <summary>
        /// Gets the node value.
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Gets the node identifier.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the graph that holds this node.
        /// </summary>
        [NotNull] IGraph<T> Graph { get; }

        /// <summary>
        /// Gets the edges of this node.
        /// </summary>
        [NotNull] ImmutableDictionary<T, IEdge<T>> Edges { get; }

        /// <summary>
        /// Gets the connections of this node.
        /// </summary>
        [NotNull] ImmutableList<INode<T>> CurrentNeighbors { get; }

        /// <summary>
        /// Gets all the connections of this node, expanding the node if it is possible.
        /// </summary>
        [NotNull]
        Task<ImmutableList<INode<T>>> GetAllNeighbors();

        /// <summary>
        /// Gets the degree (the number of outgoing edges) of the node.
        /// </summary>
        int OutDegree { get; }

        /// <summary>
        /// Determines whether the current node is connected to the specified node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>
        ///   <c>true</c> if there is an edge to the specified node; otherwise, <c>false</c>.
        /// </returns>
        bool IsConnectedTo([NotNull] INode<T> node);

        /// <summary>
        /// Determines whether the current node is connected to a node with the specified value.
        /// </summary>
        /// <param name="value">The value to seek in the connections.</param>
        /// <returns>
        ///   <c>true</c> if there is an edge to the specified value; otherwise, <c>false</c>.s
        /// </returns>
        bool IsConnectedTo([NotNull] T value);

        /// <summary>
        /// Function used to spot cycles in the graph.
        /// </summary>
        /// <param name="encounteredNodes">The encountered nodes.</param>
        bool CyclesFound([NotNull] ImmutableHashSet<Guid> encounteredNodes);

        /// <summary>
        /// Occurs when a connection is added.
        /// </summary>
        event EventHandler<T> OnConnectionAdded;

        /// <summary>
        /// Occurs when a connection is removed.
        /// </summary>
        event EventHandler<T> OnConnectionRemoved;
    }
}
