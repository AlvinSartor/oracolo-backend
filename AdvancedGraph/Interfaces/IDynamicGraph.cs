﻿using System.Collections.Immutable;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace AdvancedGraph.Interfaces
{
    public interface IDynamicGraph<T> : IGraph<T>
    {
        /// <summary>
        /// Sets the node containing the specified value as dirty.
        /// </summary>
        /// <param name="value">The value to set as dirty.</param>
        void SetDirty(T value);

        /// <summary>
        /// Sets the specified node as dirty.
        /// </summary>
        /// <param name="node">The node to set as dirty.</param>
        void SetDirty([NotNull] INode<T> node);

        /// <summary>
        /// Sets the dirty flag on all nodes.
        /// </summary>
        void SetAllDirty();

        /// <summary>
        /// Expands the specified node and adds its children to the graph.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>A list containing the newly found nodes.</returns>
        [NotNull]
        Task<ImmutableList<INode<T>>> Expand([NotNull] INode<T> node);

        [NotNull]
        Task<ImmutableList<INode<T>>> Expand(T value);       
    }
}
