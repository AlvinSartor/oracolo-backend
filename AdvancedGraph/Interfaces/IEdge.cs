﻿using System;
using JetBrains.Annotations;

namespace AdvancedGraph.Interfaces
{
    public interface IEdge<T> : IEquatable<IEdge<T>>
    {
        /// <summary>
        /// The destination node.
        /// </summary>
        [NotNull] INode<T> DestinationNode { get; }

        /// <summary>
        /// The start node. 
        /// </summary>
        [NotNull] INode<T> StartNode { get; }

        /// <summary>
        /// The weight.
        /// </summary>
        double Weight { get; }
    }
}
