using System;
using System.Collections.Generic;
using System.Linq;
using AdvancedGraph.Interfaces;
using JetBrains.Annotations;
using NUnit.Framework;

namespace AdvancedGraph.NUnit
{
    [TestFixture]
    internal abstract class GenericGraphFixture<TGraph>
        where TGraph : Graph<int>
    {
        [NotNull]
        protected abstract TGraph CreateGraph();

        [Test]
        public void GraphIsInitializedEmpty()
        {
            TGraph graph = CreateGraph();

            Assert.That(graph.Count, Is.EqualTo(0));
            Assert.That(graph.Nodes.Count, Is.EqualTo(0));
            Assert.That(graph.Values.Count, Is.EqualTo(0));
        }

        [Test]
        public void TheCountOfElementsIncreasesAfterAdditionAndDecreasesAfterDeltion()
        {
            TGraph graph = CreateGraph();
            const int value = 42;

            Assert.That(graph.Count, Is.EqualTo(0));

            graph.Add(value);
            Assert.That(graph.Count, Is.EqualTo(1));
            Assert.That(graph.Contains(value));

            graph.Remove(value);
            Assert.That(graph.Count, Is.EqualTo(0));
        }

        [Test]
        public void GraphThrowsIfTryingToAddAnAlreadyExistingValue()
        {
            TGraph graph = CreateGraph();
            const int value = 42;

            graph.Add(value);
            Assert.That(graph.Contains(value));

            Assert.Throws<ArgumentException>(() => graph.Add(value));
        }

        [Test]
        public void GraphThrowsIfTryingToDeleteAValueNotInTheCollection()
        {
            TGraph graph = CreateGraph();
            const int value = 42;

            Assert.That(!graph.Contains(value));
            Assert.Throws<ArgumentException>(() => graph.Remove(value));
        }

        [Test]
        public void GraphThrowsIfTryingToGetAValueNotInTheCollection()
        {
            TGraph graph = CreateGraph();
            const int value = 42;

            Assert.That(!graph.Contains(value));
            Assert.Throws<ArgumentException>(() => graph.GetNode(value));
            Assert.Throws<ArgumentException>(() =>
            {
                INode<int> _ = graph[value];
            });

            Assert.DoesNotThrow(() => graph.TryGetNode(value, out _));
        }

        [Test]
        public void GraphContainsAreFindingExistingNodes()
        {
            TGraph graph = CreateGraph();
            const int value = 42;

            Assert.That(!graph.Contains(value));
            Assert.That(!graph.Contains(new Guid()));

            graph.Add(value);

            Assert.That(graph.Contains(value));
            Assert.That(graph.Contains(graph[value].Id));
        }

        [Test]
        public void CyclomaticConnectionsAreSpotted()
        {
            TGraph graph = CreateGraph();
            const int alpha = 38;
            const int beta = 39;
            const int gamma = 40;
            const int delta = 41;
            const int omega = 42;

            Node<int> nodeAlpha = graph.Add(alpha);
            Node<int> nodeBeta = graph.Add(beta);
            Node<int> nodeGamma = graph.Add(gamma);
            Node<int> nodeDelta = graph.Add(delta);
            Node<int> nodeOmega = graph.Add(omega);

            Assert.False(graph.HasCycles());

            nodeAlpha.AddConnection(nodeBeta);
            nodeBeta.AddConnection(nodeGamma);
            nodeDelta.AddConnection(nodeOmega);
            nodeGamma.AddConnection(nodeOmega);

            Assert.False(graph.HasCycles());

            nodeGamma.AddConnection(nodeAlpha);
            Assert.True(graph.HasCycles());

            nodeGamma.RemoveConnection(nodeAlpha);
            Assert.False(graph.HasCycles());
        }

        [Test]
        public void BidirectionalConnectionsImmediatelyCreateCycles()
        {
            TGraph graph = CreateGraph();
            const int alpha = 38;
            const int beta = 39;

            Node<int> nodeAlpha = graph.Add(alpha);
            Node<int> nodeBeta = graph.Add(beta);

            Assert.False(graph.HasCycles());

            nodeAlpha.AddBidirectionalConnection(nodeBeta);
            Assert.True(graph.HasCycles());

            nodeBeta.RemoveConnection(nodeAlpha);
            Assert.False(graph.HasCycles());
        }

        [Test]
        public void MergingTwoGraphsResultsInAddingConnectionsToTheActualGraph()
        {
            TGraph graphA = CreateGraph();
            const int alpha = 38;
            const int beta = 39;

            Node<int> nodeAlpha = graphA.Add(alpha);
            Node<int> nodeBeta = graphA.Add(beta);
            nodeAlpha.AddConnection(nodeBeta);

            Assert.That(graphA.Count, Is.EqualTo(2));
            Assert.That(graphA.Contains(alpha));
            Assert.That(graphA.Contains(beta));
            Assert.That(graphA[alpha].IsConnectedTo(graphA[beta]));

            TGraph graphB = CreateGraph();
            const int gamma = 40;
            const int delta = 41;

            Node<int> nodeAlphaB = graphB.Add(alpha);
            Node<int> nodeGamma = graphB.Add(gamma);
            Node<int> nodeDelta = graphB.Add(delta);
            nodeGamma.AddConnection(nodeDelta);
            nodeAlphaB.AddConnection(nodeGamma);

            Assert.That(graphB.Count, Is.EqualTo(3));
            Assert.That(graphB.Contains(alpha));
            Assert.That(graphB.Contains(gamma));
            Assert.That(graphB.Contains(delta));
            Assert.That(graphB[alpha].IsConnectedTo(graphB[gamma]));
            Assert.That(graphB[gamma].IsConnectedTo(graphB[delta]));

            Assert.That(graphB[alpha].Id != graphA[alpha].Id);


            graphA.Merge(graphB);

            Assert.That(graphA.Count, Is.EqualTo(4));
            Assert.That(graphA.Contains(alpha));
            Assert.That(graphA.Contains(beta));
            Assert.That(graphA.Contains(gamma));
            Assert.That(graphA.Contains(delta));
            Assert.That(graphA[alpha].IsConnectedTo(graphA[beta]));
            Assert.That(graphA[alpha].IsConnectedTo(graphA[gamma]));
            Assert.That(graphA[gamma].IsConnectedTo(graphA[delta]));

            Assert.That(graphB[alpha].Id != graphA[alpha].Id);
            Assert.That(graphB[gamma].Id != graphA[gamma].Id);
            Assert.That(graphB[delta].Id != graphA[delta].Id);
        }

        [Test]
        public void IfAConnectionAlreadyExistsThenMergingChangesIt()
        {
            TGraph graphA = CreateGraph();
            TGraph graphB = CreateGraph();

            const int alpha = 38;
            const int beta = 39;

            graphA.Add(alpha);
            graphA.Add(beta);
            graphA.GetNode(alpha).AddConnection(graphA.GetNode(beta), 15);

            Assert.That(graphA[alpha].IsConnectedTo(graphA[beta]));
            Assert.That(graphA[alpha].Edges[beta].Weight, Is.EqualTo(15));

            graphB.Add(alpha);
            graphB.Add(beta);
            graphB.GetNode(alpha).AddConnection(graphB.GetNode(beta), 43);

            Assert.That(graphB[alpha].IsConnectedTo(graphB[beta]));
            Assert.That(graphB[alpha].Edges[beta].Weight, Is.EqualTo(43));


            graphA.Merge(graphB);

            Assert.That(graphA[alpha].Edges[beta].Weight, Is.EqualTo(43));
        }


        [Test]
        public void IfANodeIsConnectedToAnotherGraphThenMergingKeepsTheOriginalConnection()
        {
            TGraph graphA = CreateGraph();
            TGraph graphB = CreateGraph();
            TGraph graphC = CreateGraph();

            const int alpha = 38;
            const int beta = 39;

            graphA.Add(alpha);
            graphC.Add(alpha);
            graphB.Add(beta);

            graphB.GetNode(beta).AddConnection(graphC.GetNode(alpha), 15);

            graphA.Merge(graphB);

            List<INode<int>> neighboursOfBeta = graphA[beta].CurrentNeighbors.ToList();
            Assert.That(neighboursOfBeta.Count, Is.EqualTo(1));
            Assert.That(neighboursOfBeta.First().Value, Is.EqualTo(alpha));
            Assert.That(neighboursOfBeta.First().Graph, Is.EqualTo(graphC));
        }

        [Test]
        public void EventIsRaisedWhenNodeIsCreated()
        {
            TGraph graph = CreateGraph();

            int resultFromCallback = 0;
            void Callback(object o, int node) => resultFromCallback = node;

            graph.OnNodeAdded += Callback;

            try
            {
                graph.Add(12345);
                Assert.That(resultFromCallback, Is.EqualTo(12345));

                graph.TryAdd(555);
                Assert.That(resultFromCallback, Is.EqualTo(555));

                graph.TryAdd(12345);
                Assert.That(resultFromCallback, Is.EqualTo(555));
            }
            finally
            {
                graph.OnNodeAdded -= Callback;                
            }
        }

        [Test]
        public void EventIsRaisedWhenNodeIsRemoved()
        {
            TGraph graph = CreateGraph();

            int resultFromCallback = 0;
            void Callback(object o, int node) => resultFromCallback = node;

            graph.OnNodeRemoved += Callback;

            try
            {
                graph.Add(1);
                graph.Add(2);
                graph.Add(3);
                graph.Add(4);

                graph.Remove(3);
                Assert.That(resultFromCallback, Is.EqualTo(3));

                try
                {
                    graph.Remove(5);
                }
                catch (Exception)
                {
                    // exception is expected
                }

                Assert.That(resultFromCallback, Is.EqualTo(3));                
            }
            finally
            {
                graph.OnNodeRemoved -= Callback;
            }
        }
    }
}