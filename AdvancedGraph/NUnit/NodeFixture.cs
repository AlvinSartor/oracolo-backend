﻿using System;
using System.Collections.Immutable;
using System.Linq;
using NUnit.Framework;

namespace AdvancedGraph.NUnit
{
    [TestFixture]
    internal sealed class NodeFixture
    {
        [Test]
        public void NodeIsInitializedCorrectly()
        {
            var graph = new Graph<int>();
            Node<int> node = graph.Add(42);

            Assert.That(node.Value, Is.EqualTo(42));
            Assert.That(node.Graph, Is.EqualTo(graph));
            Assert.That(node.OutDegree, Is.EqualTo(0));
        }

        [Test]
        public void TwoNodeHaveDifferentId()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.False(node42.Equals(node38));        
            
            var graph2 = new Graph<int>();
            Node<int> node42Bis = graph2.Add(42);

            Assert.True(node42.Value == node42Bis.Value);

            Assert.False(node42.Equals(node42Bis));       
            Assert.That(node42.GetHashCode() != node42Bis.GetHashCode());
        }

        [Test]
        public void NodesWithTheSameContentAreNotEqualIfTheyAreNotTheSameNode()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.True(node42.Id != node38.Id);
        }

        [Test]
        public void AddingAConnectionIsReflectedOnInternalDataStructures()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);
            
            Assert.That(node42.OutDegree, Is.EqualTo(0));

            node42.AddConnection(node38, 7.0);

            Assert.That(node42.OutDegree, Is.EqualTo(1));
            Assert.That(node42.Edges.Values.Count, Is.EqualTo(1));
            Assert.That(node42.Edges.Values.First().DestinationNode, Is.EqualTo(node38));
            Assert.That(node42.Edges.Values.First().StartNode, Is.EqualTo(node42));
            Assert.That(node42.Edges.Values.First().Weight, Is.EqualTo(7.0));

            node38.AddConnection(node42, 42.0);

            Assert.That(node38.OutDegree, Is.EqualTo(1));
            Assert.That(node38.Edges.Values.Count, Is.EqualTo(1));
            Assert.That(node38.Edges.Values.First().DestinationNode, Is.EqualTo(node42));
            Assert.That(node38.Edges.Values.First().StartNode, Is.EqualTo(node38));
            Assert.That(node38.Edges.Values.First().Weight, Is.EqualTo(42.0));
        }

        [Test]
        public void DefaultWeightValueForEdgesIsOne()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.That(node42.OutDegree, Is.EqualTo(0));

            node42.AddConnection(node38);
            
            Assert.That(node42.OutDegree, Is.EqualTo(1));
            Assert.That(node42.Edges.Values.First().Weight, Is.EqualTo(1.0));
        }

        [Test]
        public void AddingABidirectionalConnectionCreatesAConnectionInBothNodes()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.That(node42.OutDegree, Is.EqualTo(0));
            Assert.That(node38.OutDegree, Is.EqualTo(0));

            node42.AddBidirectionalConnection(node38);

            Assert.That(node42.OutDegree, Is.EqualTo(1));
            Assert.That(node38.OutDegree, Is.EqualTo(1));
        }

        [Test]
        public void RemovingConnectionsIsReflectedOnInternalDataStructures()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            node42.AddConnection(node38);
            Assert.That(node42.OutDegree, Is.EqualTo(1));

            node42.RemoveConnection(node38);
            Assert.That(node42.OutDegree, Is.EqualTo(0));

            node42.AddConnection(node38);
            Assert.That(node42.OutDegree, Is.EqualTo(1));

            node42.ClearConnections();
            Assert.That(node42.OutDegree, Is.EqualTo(0));
        }

        [Test]
        public void RemovingAConnectionDoesntAffectOtherNodes()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            node42.AddBidirectionalConnection(node38);
            Assert.That(node42.OutDegree, Is.EqualTo(1));
            Assert.That(node38.OutDegree, Is.EqualTo(1));

            node42.RemoveConnection(node38);
            Assert.That(node42.OutDegree, Is.EqualTo(0));
            Assert.That(node38.OutDegree, Is.EqualTo(1));
        }

        [Test]
        public void AddConnectionMethodThrowsIfThereIsAlreadyAConnection()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            node42.AddBidirectionalConnection(node38);

            Assert.Throws<ArgumentException>(() => node42.AddConnection(node38));
            Assert.DoesNotThrow(() => node42.TryAddConnection(node38));
            Assert.DoesNotThrow(() => node42.AddOrModifyConnection(node38, 12.0));
        }

        [Test]
        public void AddOrModifyConnectionModifiesTheWeightOfAConnectionIfThisExists()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.That(node42.Edges.Count, Is.EqualTo(0));

            node42.AddConnection(node38);

            Assert.That(node42.OutDegree, Is.EqualTo(1));
            Assert.That(node42.Edges.Values.First().Weight, Is.EqualTo(1.0));

            node42.AddOrModifyConnection(node38, 12);

            Assert.That(node42.OutDegree, Is.EqualTo(1));
            Assert.That(node42.Edges.Values.First().Weight, Is.EqualTo(12.0));
        }

        [Test]
        public void RemoveConnectionMethodThrowsIfTheConnectionDoesNotExist()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.Throws<ArgumentException>(() => node42.RemoveConnection(node38));
            Assert.DoesNotThrow(() => node42.TryRemoveConnection(node38));
        }

        [Test]
        public void BidirectionalConnectionCausesCycles()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);
            Node<int> node38 = graph.Add(38);

            Assert.False(node42.CyclesFound(ImmutableHashSet<Guid>.Empty));
            Assert.False(node38.CyclesFound(ImmutableHashSet<Guid>.Empty));

            node42.AddConnection(node38);

            Assert.False(node42.CyclesFound(ImmutableHashSet<Guid>.Empty));
            Assert.False(node38.CyclesFound(ImmutableHashSet<Guid>.Empty));

            node38.AddConnection(node42);

            Assert.True(node42.CyclesFound(ImmutableHashSet<Guid>.Empty));
            Assert.True(node38.CyclesFound(ImmutableHashSet<Guid>.Empty));
        }

        [Test]
        public void ToStringReturnsAStringRepresentationOfTheNode()
        {
            var graph = new Graph<int>();
            Node<int> node42 = graph.Add(42);

            string expectedResult = $"Node ID: {node42.Id.ToString()}, Value: 42";

            Assert.That(node42.ToString(), Is.EqualTo(expectedResult));
            Console.WriteLine(node42.ToString());
        }

        [Test]
        public void EventIsRaisedWhenConnectionIsAdded()
        {
            var graph = new Graph<int>();

            Node<int> node0 = graph.Add(0);
            Node<int> node1 = graph.Add(1);

            Node<int> node2 = graph.Add(2);
            Node<int> node3 = graph.Add(3);
            Node<int> node4 = graph.Add(4);

            int lastNodeAdded = int.MinValue;
            int numberCallbacks = 0;
            void Callback(object o, int arg)
            {
                lastNodeAdded = arg;
                numberCallbacks++;
            }

            void OtherNodesCallback(object o, int arg) => Assert.Fail($"The node {((Node<int>)o).Value} should not raise any event.");

            node0.OnConnectionAdded += Callback;
            node1.OnConnectionAdded += Callback;
            node2.OnConnectionAdded += OtherNodesCallback;
            node3.OnConnectionAdded += OtherNodesCallback;
            node4.OnConnectionAdded += OtherNodesCallback;

            try
            {
                node0.AddConnection(node2);
                Assert.That(lastNodeAdded, Is.EqualTo(2));
                Assert.That(numberCallbacks, Is.EqualTo(1));

                node0.TryAddConnection(node3);
                Assert.That(lastNodeAdded, Is.EqualTo(3));
                Assert.That(numberCallbacks, Is.EqualTo(2));

                node0.AddOrModifyConnection(node4);
                Assert.That(lastNodeAdded, Is.EqualTo(4));
                Assert.That(numberCallbacks, Is.EqualTo(3));

                node0.TryAddConnection(node3);
                Assert.That(lastNodeAdded, Is.EqualTo(4));
                Assert.That(numberCallbacks, Is.EqualTo(3));

                node0.AddBidirectionalConnection(node1);
                //result of second callback:
                Assert.That(lastNodeAdded, Is.EqualTo(0)); 
                Assert.That(numberCallbacks, Is.EqualTo(5));
            }
            finally
            {
                node0.OnConnectionAdded -= Callback;
                node1.OnConnectionAdded -= Callback;
                node2.OnConnectionAdded -= OtherNodesCallback;
                node3.OnConnectionAdded -= OtherNodesCallback;
                node4.OnConnectionAdded -= OtherNodesCallback;            
            }
        }

        [Test]
        public void EventIsRaisedWhenConnectionIsRemoved()
        {
            var graph = new Graph<int>();

            Node<int> node0 = graph.Add(0);

            Node<int> node2 = graph.Add(2);
            Node<int> node3 = graph.Add(3);
            Node<int> node4 = graph.Add(4);

            node0.AddConnection(node2);
            node0.AddConnection(node3);
            node0.AddConnection(node4);

            int lastNodeRemoved = int.MinValue;
            void Callback(object o, int arg) => lastNodeRemoved = arg;

            void OtherNodesCallback(object o, int arg) => Assert.Fail($"The node {((Node<int>)o).Value} should not raise any event.");

            node0.OnConnectionRemoved += Callback;
            node2.OnConnectionRemoved += OtherNodesCallback;
            node3.OnConnectionRemoved += OtherNodesCallback;
            node4.OnConnectionRemoved += OtherNodesCallback;

            try
            {
                node0.RemoveConnection(node2);
                Assert.That(lastNodeRemoved, Is.EqualTo(2));

                node0.TryRemoveConnection(node3);
                Assert.That(lastNodeRemoved, Is.EqualTo(3));

                node0.TryRemoveConnection(node0);
                Assert.That(lastNodeRemoved, Is.EqualTo(3));

                node0.AddOrModifyConnection(node4);
                Assert.That(lastNodeRemoved, Is.EqualTo(4));
            }
            finally
            {
                node0.OnConnectionRemoved -= Callback;
                node2.OnConnectionRemoved -= OtherNodesCallback;
                node3.OnConnectionRemoved -= OtherNodesCallback;
                node4.OnConnectionRemoved -= OtherNodesCallback;
            }
        }

        [Test]
        public void AddOrModifyConnectionRaisesTwoEvents()
        {
            var graph = new Graph<int>();

            Node<int> node0 = graph.Add(0);
            Node<int> node2 = graph.Add(2);
            Node<int> node4 = graph.Add(4);

            node0.AddConnection(node2);

            var hasAdded = false;
            var hasRemoved = false;

            void AdditionCallback(object o, int arg) => hasAdded = true;
            void RemotionCallback(object o, int arg) => hasRemoved = true;

            node0.OnConnectionAdded += AdditionCallback;
            node0.OnConnectionRemoved += RemotionCallback;

            try
            {
                node0.AddOrModifyConnection(node2);
                Assert.IsTrue(hasRemoved);
                Assert.IsTrue(hasAdded);

                hasRemoved = false;
                hasAdded = false;

                node0.AddOrModifyConnection(node4);
                Assert.IsFalse(hasRemoved);
                Assert.IsTrue(hasAdded);
            }
            finally
            {
                node0.OnConnectionAdded-= AdditionCallback;
                node0.OnConnectionRemoved -= RemotionCallback;
            }
        }
    }
}
