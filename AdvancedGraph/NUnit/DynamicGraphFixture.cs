﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using AdvancedGraph.PathFinding;
using JetBrains.Annotations;
using NUnit.Framework;

namespace AdvancedGraph.NUnit
{
    [TestFixture]
    internal sealed class DynamicGraphFixture : GenericGraphFixture<DynamicGraph<int>>
    {
        private const int s_NumberOfChildrenForEachNode = 2;

        [NotNull]
        private static Task<ImmutableList<(int, double)>> ExpandFunction(int nodeValue)
        {
            int firstChild = nodeValue * s_NumberOfChildrenForEachNode + 1;
            List<(int, double)> children = new List<(int, double)>();
            for (int i = 0; i < s_NumberOfChildrenForEachNode; i++)
            {
                children.Add((firstChild + i, 1d));
            }

            return Task.FromResult(children.ToImmutableList());
        }

        /// <inheritdoc />
        protected override DynamicGraph<int> CreateGraph()
        {            
            return new DynamicGraph<int>(ExpandFunction);
        }

        [Test]
        public async Task PathFindingTest()
        {
            DynamicGraph<int> graph = CreateGraph();
            graph.Add(0);

            Path<int> path0To10 = await graph.AStarSearch(0, 100, (val) => Math.Abs(100 - val));
            string result = path0To10.ToString(x => x.ToString());

            Assert.That(result, Is.EqualTo("0 -> 2 -> 5 -> 11 -> 24 -> 49 -> 100"));
        }
    }
}
