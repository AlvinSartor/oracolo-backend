using NUnit.Framework;

namespace AdvancedGraph.NUnit
{
    [TestFixture]
    internal sealed class GraphFixture : GenericGraphFixture<Graph<int>>
    {
        /// <inheritdoc />
        protected override Graph<int> CreateGraph()
        {
            return new Graph<int>();
        }
    }
}