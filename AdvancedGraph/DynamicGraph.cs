﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph
{
    public sealed class DynamicGraph<T> : Graph<T>, IDynamicGraph<T>
    {
        [NotNull] private readonly Func<T, Task<ImmutableList<(T, double)>>> m_ExpandFunction;
        [NotNull] private readonly Dictionary<INode<T>, NodeState> m_NodesState;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicGraph{T}"/> class.
        /// </summary>
        /// <param name="expandFunction">The expand function.</param>
        public DynamicGraph([NotNull] Func<T, Task<ImmutableList<(T, double)>>> expandFunction)
            : base()
        {
            m_ExpandFunction = expandFunction;
            m_NodesState = new Dictionary<INode<T>, NodeState>();
        }       

        /// <inheritdoc />
        public override Node<T> Add(T item)
        {
            Node<T> addedNode = base.Add(item);
            m_NodesState.Add(addedNode, new NodeState(true, DateTime.UtcNow, DateTime.UtcNow));
            return addedNode;
        }

        /// <inheritdoc />
        public override Node<T> TryAdd(T item)
        {
            Node<T> addedNode = base.TryAdd(item);

            if (addedNode != null)
            {
                m_NodesState.Add(addedNode, new NodeState(true, DateTime.UtcNow, DateTime.UtcNow));
            }

            return addedNode;
        }

        /// <inheritdoc />
        public override void Remove(T item)
        {
            item.AssertNotNull(nameof(item));

            if (!Contains(item))
            {
                throw new ArgumentException("Item not in graph");
            }

            m_NodesState.Remove(this[item]);
            base.Remove(item);
        }

        /// <inheritdoc />
        public void SetDirty(INode<T> node)
        {
            node.AssertNotNull(nameof(node));
            SetDirty(node.Value);
        }

        /// <inheritdoc />
        public void SetAllDirty()
        {
            foreach (var node in m_NodesState.Keys)
            {
                m_NodesState[node] = m_NodesState[node].WithUpdate(true);
            }
        }

        /// <inheritdoc />
        public void SetDirty(T value)
        {
            value.AssertNotNull(nameof(value));

            if (!Contains(value))
            {
                throw new ArgumentException($"Item '{value}' not in graph");
            }

            m_NodesState[this[value]] = m_NodesState[this[value]].WithUpdate(true);
        }

        /// <inheritdoc />
        public async Task<ImmutableList<INode<T>>> Expand(INode<T> node)
        {
            node.AssertNotNull(nameof(node));
            return await Expand(node.Value);
        }

        /// <inheritdoc />
        public async Task<ImmutableList<INode<T>>> Expand(T value)
        {
            value.AssertNotNull(nameof(value));

            if (!Contains(value))
            {
                throw new ArgumentException($"Item '{value}' not in graph");
            }

            Node<T> modifiableNode = GetNode(value);

            if (m_NodesState[modifiableNode].IsDirty)
            {
                await InternalExpand(modifiableNode);
            }

            return modifiableNode.CurrentNeighbors;
        }

        private async Task InternalExpand([NotNull] Node<T> modifiableNode)
        {
            ImmutableList<(T value, double connectionWeight)> expandedChildren = await m_ExpandFunction(modifiableNode.Value);
            modifiableNode.ClearConnections();

            foreach ((var childValue, var connectionWeight) in expandedChildren)
            {
                Node<T> addedNode = TryAdd(childValue)
                                    // if node already exists:
                                    ?? GetNode(childValue);

                modifiableNode.AddConnection(addedNode, connectionWeight);
            }

            m_NodesState[modifiableNode] = m_NodesState[this[modifiableNode.Value]].WithUpdate(false);
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            return other is IGraph<T> graph && graph.Id == Id;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}