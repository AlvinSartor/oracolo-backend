﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using AdvancedGraph.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace AdvancedGraph
{
    /// <summary>
    /// Class that represents nodes in graphs.
    /// </summary>
    public sealed class Node<T> : INode<T>
    {
        private readonly Guid m_Id;

        [NotNull] private readonly T m_Value;
        [NotNull] private readonly IGraph<T> m_Graph;


        [NotNull] private ImmutableDictionary<T, IEdge<T>> m_Edges;

        [CanBeNull] private readonly DynamicGraph<T> m_DynamicGraph;

        /// <summary>
        /// Initializes a new instance of the <see cref="Node{T}"/> class.
        /// </summary>
        internal Node([NotNull] IGraph<T> graph, [NotNull] T value)
        {
            m_Id = Guid.NewGuid();
            m_Value = value.NotNull(nameof(value));
            m_Graph = graph.NotNull(nameof(graph));
            m_Edges = ImmutableDictionary<T, IEdge<T>>.Empty;

            if (graph is DynamicGraph<T> dynamicGraph)
            {
                m_DynamicGraph = dynamicGraph;
            }
        }

        /// <inheritdoc />
        public T Value => m_Value;

        /// <inheritdoc />
        public IGraph<T> Graph => m_Graph;

        /// <summary>
        /// Gets the edges of this node.
        /// </summary>
        public ImmutableDictionary<T, IEdge<T>> Edges => m_Edges;

        /// <inheritdoc />
        public ImmutableList<INode<T>> CurrentNeighbors => m_Edges.Values.Select(x => x.DestinationNode).ToImmutableList();

        /// <inheritdoc />
        public async Task<ImmutableList<INode<T>>> GetAllNeighbors() => m_DynamicGraph == null
            ? CurrentNeighbors
            : await m_DynamicGraph.Expand(Value);


        /// <inheritdoc />
        public int OutDegree => m_Edges.Count;

        /// <inheritdoc />
        public Guid Id => m_Id;

        /// <summary>
        /// Adds a directional edge from this node to the specified one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weigth">The weight of the edge.</param>
        /// <exception cref="ArgumentException">The selected node already has this edge.</exception>
        public void AddConnection([NotNull] INode<T> node, double weigth = 1)
        {
            node.AssertNotNull(nameof(node));

            if (IsConnectedTo(node))
            {
                throw new ArgumentException("The selected node already has this edge.");
            }

            var edge = new Edge<T>(this, node, weigth);
            m_Edges = m_Edges.Add(node.Value, edge);
            OnConnectionAdded?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Adds a directional edge from this node to the specified one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weigth">The weight of the edge.</param>
        public void TryAddConnection([NotNull] INode<T> node, double weigth = 1)
        {
            node.AssertNotNull(nameof(node));

            if (IsConnectedTo(node))
            {
                return;
            }

            var edge = new Edge<T>(this, node, weigth);
            m_Edges = m_Edges.Add(node.Value, edge);
            OnConnectionAdded?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Adds a directional edge from this node to the specified one.
        /// If the edge already exists it is replaced with the new one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weigth">The weight of the edge.</param>
        public void AddOrModifyConnection([NotNull] INode<T> node, double weigth = 1)
        {
            node.AssertNotNull(nameof(node));
            var edge = new Edge<T>(this, node, weigth);

            if (IsConnectedTo(node))
            {
                m_Edges = m_Edges.Remove(node.Value);
                OnConnectionRemoved?.Invoke(this, node.Value);
            }

            m_Edges = m_Edges.Add(node.Value, edge);
            OnConnectionAdded?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Adds a bidirectional edge between this node and the specified one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weigth">The weight of the edge from this to node.</param>
        /// <param name="weigthReturn">The weight of the edge from node to this.</param>
        public void AddBidirectionalConnection([NotNull] Node<T> node, double weigth = 1, double weigthReturn = 1)
        {
            node.AssertNotNull(nameof(node));
            AddConnection(node);
            node.AddConnection(this);
        }

        /// <summary>
        /// Removes the edge from this node to the specified one.
        /// </summary>
        /// <param name="node">The node to remove the edge to.</param>
        /// <exception cref="ArgumentException">The edge with the specified node hasn't been found.</exception>
        public void RemoveConnection([NotNull] INode<T> node)
        {
            node.AssertNotNull(nameof(node));

            if (!IsConnectedTo(node))
            {
                throw new ArgumentException("The Edge with the specified node hasn't been found.");
            }

            m_Edges = m_Edges.Remove(node.Value);
            OnConnectionRemoved?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Tries the remove the edge to the specified node.
        /// </summary>
        /// <param name="node">The node to remove the edge to.</param>
        public void TryRemoveConnection([NotNull] INode<T> node)
        {
            node.AssertNotNull(nameof(node));

            if (IsConnectedTo(node))
            {
                m_Edges = m_Edges.Remove(node.Value);
                OnConnectionRemoved?.Invoke(this, node.Value);
            }
        }

        /// <summary>
        /// Removes all edges of this node.
        /// </summary>
        public void ClearConnections()
        {
            m_Edges.Keys.ForEach(value => OnConnectionRemoved?.Invoke(this, value));
            m_Edges = ImmutableDictionary<T, IEdge<T>>.Empty;
        }

        /// <inheritdoc />
        public bool IsConnectedTo(INode<T> node)
        {
            node.AssertNotNull(nameof(node));
            return m_Edges.ContainsKey(node.Value);
        }

        /// <inheritdoc />
        public bool IsConnectedTo(T value)
        {
            value.AssertNotNull(nameof(value));
            return m_Edges.ContainsKey(value);
        }

        /// <inheritdoc />
        public bool CyclesFound(ImmutableHashSet<Guid> encounteredNodes)
        {
            encounteredNodes.AssertNotNull(nameof(encounteredNodes));

            if (encounteredNodes.Contains(m_Id))
            {
                return true;
            }

            encounteredNodes = encounteredNodes.Add(m_Id);
            return m_Edges.Any(x => x.Value.DestinationNode.CyclesFound(encounteredNodes));
        }

        /// <inheritdoc />
        public event EventHandler<T> OnConnectionAdded;

        /// <inheritdoc />
        public event EventHandler<T> OnConnectionRemoved;

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is Node<T> node && node.Id == m_Id;
        }

        /// <inheritdoc />
        public bool Equals(INode<T> other)
        {
            return other != null && Id == other.Id;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = m_Id.GetHashCode();
                hashCode = (hashCode * 397) ^ m_Graph.GetHashCode();
                return hashCode;
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"Node ID: {Id.ToString()}, Value: {Value.ToString()}";
        }
    }
}