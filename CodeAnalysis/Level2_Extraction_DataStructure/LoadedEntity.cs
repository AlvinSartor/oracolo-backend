﻿using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Level2_Extraction_DataStructure
{
    public sealed class LoadedEntity : IEntity
    {

        public LoadedEntity([NotNull] string fullName, [NotNull] string assembly, [NotNull] string filePath, EntityType type)
        {
            Type = type;
            FullName = fullName.NotNull(nameof(fullName));
            Assembly = assembly.NotNull(nameof(assembly));
            FilePath = filePath.NotNull(nameof(filePath));
            Name = fullName.Substring(fullName.LastIndexOf('.'));
        }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public string FullName { get; }

        /// <inheritdoc />
        public string Assembly { get; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        [NotNull] public string FilePath { get; }

        /// <inheritdoc />
        public EntityStatus Status => EntityStatus.LoadedAndInternal;

        /// <inheritdoc />
        public EntityType Type { get; }

        /// <inheritdoc />
        public bool Equals(IEntity other)
        {
            return other is LoadedEntity solutionEntity
                   && solutionEntity.Type == Type
                   && solutionEntity.Assembly.Equals(Assembly)
                   && solutionEntity.FullName.Equals(FullName);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is LoadedEntity other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Assembly.GetHashCode();
                hashCode = (hashCode * 397) ^ FullName.GetHashCode();
                return hashCode;
            }
        }
    }
}