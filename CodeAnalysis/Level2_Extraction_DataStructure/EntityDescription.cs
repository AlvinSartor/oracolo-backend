﻿using System;
using System.Linq;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Level2_Extraction_DataStructure
{
    public sealed class EntityDescription : IEquatable<EntityDescription>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDescription"/> class.
        /// </summary>
        /// <param name="filePath">The path of the file containing the entity.</param>
        /// <param name="fullName">Full name of the entity, comprehensive of namespace.</param>
        internal EntityDescription([NotNull] string filePath, [NotNull] string fullName)
        {
            FilePath = filePath.NotNull(nameof(filePath));
            FullName = fullName.NotNull(nameof(fullName));
            Name = fullName.Split('.').Last();
        }      

        /// <summary>
        /// Gets the entity file path.
        /// </summary>
        [NotNull] public string FilePath { get; }

        /// <summary>
        /// The entity full name.
        /// </summary>
        [NotNull] public string FullName { get; }

        /// <summary>
        /// The entity name.
        /// </summary>
        [NotNull] public string Name { get; }


        /// <inheritdoc />
        public bool Equals(EntityDescription other)
        {
            if (other is null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(FilePath, other.FilePath) && string.Equals(FullName, other.FullName);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is EntityDescription other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = FilePath.GetHashCode();
                hashCode = (hashCode * 397) ^ FullName.GetHashCode();
                hashCode = (hashCode * 397) ^ Name.GetHashCode();
                return hashCode;
            }
        }
    }
}
