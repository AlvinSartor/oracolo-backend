﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeAnalysis.Level0_Mapping.Helpers;
using CodeAnalysis.Level0_Mapping_DataStructure.Interfaces;
using Extensions;
using JetBrains.Annotations;
using Microsoft.CodeAnalysis;

namespace CodeAnalysis.Level1_Mining_DataStructure
{
    /// <summary>
    /// The CompilationUnit holds references to the Csproj it is compiled from and
    /// allows to fetch syntax trees and semantic models of files in it contained
    /// </summary>
    public sealed class CompilationUnit
    {
        /// <summary>
        /// Gets the compilation.
        /// </summary>
        [NotNull] public Compilation Compilation { get; }

        /// <summary>
        /// Gets the csproj.
        /// </summary>
        [NotNull] public ICsprojDescriptor Csproj { get; }


        [NotNull] private readonly ImmutableDictionary<string, SyntaxTree> m_PathsToSyntaxTreesMapping;
        [NotNull] private readonly ImmutableDictionary<string, string> m_EntitiesToPathsMapping;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompilationUnit"/> class.
        /// </summary>
        /// <param name="compilation">The compilation.</param>
        /// <param name="csproj">The csproj.</param>
        public CompilationUnit([NotNull] Compilation compilation, [NotNull] ICsprojDescriptor csproj)
        {
            Compilation = compilation.NotNull(nameof(compilation));
            Csproj = csproj.NotNull(nameof(csproj));

            m_PathsToSyntaxTreesMapping = Compilation.SyntaxTrees.ToImmutableDictionary(tree => tree.FilePath, tree => tree);
            m_EntitiesToPathsMapping = m_PathsToSyntaxTreesMapping.Keys.SelectMany(GetEntitiesFromFiles)
                .ToImmutableDictionary(e => e.entityName, e => e.filePath);

            CheckThatCsprojDescriptorAndCompilationMatch();
        }

        private void CheckThatCsprojDescriptorAndCompilationMatch()
        {
            string missingFiles = string.Empty;
            foreach (var fileDescriptor in Csproj.Files)
            {
                if (!m_PathsToSyntaxTreesMapping.ContainsKey(fileDescriptor.FilePath))
                {
                    missingFiles += fileDescriptor.FilePath + "\n";
                }
            }

            if (!string.IsNullOrEmpty(missingFiles.Trim()))
            {
                throw new ArgumentException("The syntax trees in the compilation do not match with the files in the csproj descriptor.\n" +
                                            $"The missing files are: {missingFiles}");
            }
        }

        private IEnumerable<(string filePath, string entityName)> GetEntitiesFromFiles([NotNull] string filePath)
        {
            return EntityNameFinder.ExtractEntityNameFromFile(filePath).Select(e => (filePath, e));
        }

        /// <summary>
        /// Determines whether the compilation contains the specified entity.
        /// </summary>
        public bool ContainsEntity([CanBeNull] string entityFullName)
        {
            return !string.IsNullOrEmpty(entityFullName) && m_EntitiesToPathsMapping.ContainsKey(entityFullName);
        }

        /// <summary>
        /// Gets the path of the specified entity.
        /// </summary>
        /// <param name="entityFullName">Entity full name (namespace + entity name).</param>
        /// <exception cref="ArgumentException">The entity does not belong to the Csproj</exception>
        [NotNull]
        public string GetPathOfEntity([NotNull] string entityFullName)
        {
            return m_EntitiesToPathsMapping.TryGetValue(entityFullName, out string entityPath)
                ? entityPath
                : throw new ArgumentException($"The entity '{entityFullName}' does not belong to the Csproj '{Csproj.CsprojName}'.");            
        }

        /// <summary>
        /// Determines whether the compilation contains the specified file path.
        /// </summary>
        public bool ContainsFile([CanBeNull] string filePath)
        {
            return !string.IsNullOrEmpty(filePath) && m_PathsToSyntaxTreesMapping.ContainsKey(filePath);
        }

        /// <summary>
        /// Gets the syntax tree of the specified file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <exception cref="ArgumentException">The filepath does not belong to the Csproj.</exception>
        [NotNull]
        public SyntaxTree GetSyntaxTreeOf([NotNull] string filePath)
        {
            CheckFilePath(filePath);
            return m_PathsToSyntaxTreesMapping[filePath];
        }

        /// <summary>
        /// Gets the semantic model of the specified file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <exception cref="ArgumentException">The filepath does not belong to the Csproj.</exception>
        [NotNull]
        public SemanticModel GetSemanticModelOf([NotNull] string filePath)
        {
            CheckFilePath(filePath);
            return Compilation.GetSemanticModel(GetSyntaxTreeOf(filePath));
        }

        [ContractAnnotation("filePath : NULL => halt")]
        private void CheckFilePath([CanBeNull] string filePath)
        {
            if (!ContainsFile(filePath))
            {
                throw new ArgumentException($"The filepath '{filePath}' does not belong to the Csproj '{Csproj.CsprojName}'.");
            }
        }
    }
}
