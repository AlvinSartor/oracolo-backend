﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using CodeAnalysis.Mapper.Helpers;
using CodeAnalysis.ProjectMapping;
using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper
{
    /// <summary>
    /// The Mapper holds a reference to the structure descriptors (Solution, Projects, Files and References).
    /// <para>
    /// This can be used by upper layers to have knowledge about paths of entities belonging to the structure,
    /// if an assembly is internal or not, where these are deployed.
    /// </para>
    /// </summary>
    public sealed class Mapper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Mapper"/> class.
        /// </summary>
        /// <param name="slnPath">The SLN path.</param>
        /// <exception cref="FileNotFoundException">The specified file has not been found</exception>
        public Mapper([NotNull] string slnPath)
        {
            slnPath.AssertNotNull(nameof(slnPath));

            if (!File.Exists(slnPath))
            {
                throw new FileNotFoundException("The specified file has not been found", slnPath);
            }

            SlnDescriptor = new SlnDescriptor(slnPath,
                                              SlnAnaliser.GetAssembliesNamesAndPathsFromSlnFile,
                                              CsprojAnaliser.GetOutputPathsFromCsproj,
                                              CsprojAnaliser.GetAllCSharpFilePathsFromCsproj,
                                              CsprojAnaliser.GetAssembliesPathsFromCsproj);

            try
            {
                DependenciesOf = SlnDescriptor.Csprojs.ToImmutableDictionary(x => x.Value, x => SlnDescriptor.InternalReferencesOf(x.Value)); 
                DependatsBy = CalculateInverseDependencies(SlnDescriptor);
                Csprojs = CsprojsNames.ToImmutableDictionary(x => x.Value.FilePath, x => x.Value);

                Files = Csprojs.Values.SelectMany(csproj => csproj.Files).ToImmutableDictionary(file => file.FilePath, file => file);
                FileNames = Csprojs.Values.SelectMany(csproj => csproj.Files).ToLookup(file => file.FileName);
            }
            catch (Exception exception)
            {
                throw new Exception(CraftProperMessage(exception));
            }
        }

        [NotNull]
        private static ImmutableDictionary<ICsprojDescriptor, ImmutableList<ICsprojDescriptor>> CalculateInverseDependencies(
            [NotNull] ISlnDescriptor slnDescriptor)
        {
            Dictionary<ICsprojDescriptor, List<ICsprojDescriptor>> tmpLookup =
                slnDescriptor.Csprojs.Values.ToDictionary(x => x, x => new List<ICsprojDescriptor>());

            foreach (var csprojDescriptor in slnDescriptor.Csprojs.Values)
            {
                foreach (var dependency in slnDescriptor.InternalReferencesOf(csprojDescriptor))
                {
                    tmpLookup[dependency].Add(csprojDescriptor);
                }
            }

            return tmpLookup.ToImmutableDictionary(x => x.Key, x => x.Value.ToImmutableList());
        }
        
        /// <summary>
        /// Gets the SLN descriptor.
        /// </summary>
        [NotNull]
        public SlnDescriptor SlnDescriptor { get; }

        /// <summary>
        /// Gets a [csprojPath to Csproj] dictionary (key = csproj Path).
        /// </summary>
        [NotNull]
        public ImmutableDictionary<string, ICsprojDescriptor> Csprojs { get; private set; }

        /// <summary>
        /// Gets a [csprojName to Csproj] dictionary (key = csproj Name).
        /// </summary>
        [NotNull]
        public ImmutableDictionary<string, ICsprojDescriptor> CsprojsNames => SlnDescriptor.Csprojs;

        /// <summary>
        /// Gets a [Csproj to Enumerable of Csproj] dictionary describing the assemblies that the specified csproj depends on (key = start csproj).
        /// </summary>
        [NotNull]
        public ImmutableDictionary<ICsprojDescriptor, ImmutableList<ICsprojDescriptor>> DependenciesOf { get; }

        /// <summary>
        /// Gets a [Csproj to Enumerable of Csproj] dictionary describing the assemblies that depend on a specified one (key = target csproj).
        /// </summary>
        [NotNull]
        public ImmutableDictionary<ICsprojDescriptor, ImmutableList<ICsprojDescriptor>> DependatsBy { get; }

        /// <summary>
        /// Gets a [filepath to File] dictionary (key = filePath).
        /// </summary>
        [NotNull]
        public ImmutableDictionary<string, FileDescriptor> Files { get; private set; }

        /// <summary>
        /// Gets a [filename to Enumerable of Files] lookup (key = fileName).
        /// </summary>
        [NotNull]
        public ILookup<string, FileDescriptor> FileNames { get; private set; }

        /// <summary>
        /// Determines whether the specified csproj belongs to the solution.
        /// </summary>
        /// <param name="csprojName">Name of the csproj.</param>
        /// <returns>
        ///   <c>true</c> if the csproj belongs to the solution]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsCsprojInSolution([NotNull] string csprojName)
        {
            return SlnDescriptor.Csprojs.ContainsKey(csprojName);
        }

        [NotNull]
        private string CraftProperMessage([NotNull] Exception exception)
        {
            if (exception is ArgumentException argumentException
                && argumentException.Message.Contains("The given key already exists in the dictionary"))
            {
                var message = "More than one csproj is referencing to the same file.";
                message += $"\nFile: {argumentException.ParamName}";

                string csprojList = Csprojs.Values.Where(
                                               csproj => csproj.Files.Any(file => file.FileName.EndsWith("DataSet1.cs")))
                                           .Aggregate("", (a, b) => a + $"\n{b}");

                message += $"\nCsprojs: {csprojList}";

                return message;
            }

            return exception.Message;
        }

        /// <summary>
        /// Given a C# file path, retrieve the Csproj this belongs to.
        /// </summary>
        /// <param name="csharpFilePath">The csproj which references we are interested in.</param>
        /// <returns>The Csproj containing the file if this belongs to the solution; null otherwise.</returns>
        [NotNull]
        public ICsprojDescriptor GetCsprojOf([NotNull] string csharpFilePath)
        {
            csharpFilePath.AssertNotNull(nameof(csharpFilePath));
            if (!Files.ContainsKey(csharpFilePath))
            {
                throw new ArgumentException("The specified path does not belong to the solution (only c# paths allowed).");
            }

            return Files[csharpFilePath].Csproj;
        }
    }
}