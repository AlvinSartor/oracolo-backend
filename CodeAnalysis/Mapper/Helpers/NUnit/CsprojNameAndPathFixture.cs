﻿using NUnit.Framework;

namespace CodeAnalysis.Mapper.Helpers.NUnit
{
    [TestFixture]
    internal sealed class CsprojNameAndPathFixture
    {
        [Test]
        public void ClassIsInitializedAsExpected()
        {
            var csprojNameAndPath = new CsprojNameAndPath("foo", $@"foo\bar\foo.csproj");

            Assert.That(csprojNameAndPath.Name, Is.EqualTo("foo"));
            Assert.That(csprojNameAndPath.Path, Is.EqualTo($@"foo\bar\foo.csproj"));
        }
    }
}
