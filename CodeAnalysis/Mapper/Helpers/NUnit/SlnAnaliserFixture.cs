﻿using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using NUnit.Framework;

namespace CodeAnalysis.Mapper.Helpers.NUnit
{
    [TestFixture]
    internal sealed class SlnAnaliserFixture
    {
        [NotNull]
        private string TestSlnContent()
        {
            return "Microsoft Visual Studio Solution File, Format Version 12.00 \n" +
                   "# Visual Studio 15 \n" +
                   "VisualStudioVersion = 15.0.27703.2047\n" +
                   "MinimumVisualStudioVersion = 10.0.40219.1\n" +
                   $"Project(\"{{...}}\") = \"AdvancedGraph\", \"AdvancedGraph\\AdvancedGraph.csproj\", \"{{...}} \"\n" +
                   "EndProject\n" +
                   $"Project(\"{{...}}\") = \"Extensions\", \"Extensions\\Extensions.csproj\", \"{{...}}\"\n" +
                   "EndProject\n" +
                   $"Project(\"{{...}}\") = \"CodeAnalysis\", \"CodeAnalysis\\CodeAnalysis.csproj\", \"{{...}}\"\n" +
                   "EndProject";
        }

        [Test]
        public void ProjectsAreCorrectlyFetchedFromTheSlnFile()
        {
            string tmpDirectoryPath = Path.GetTempPath();
            string tmpSlnFilePath = Path.Combine(tmpDirectoryPath, "tmpSln.sln");
            File.WriteAllText(tmpSlnFilePath, TestSlnContent());

            IEnumerable<CsprojNameAndPath> result; 

            try
            {
                result = SlnAnaliser.GetAssembliesNamesAndPathsFromSlnFile(tmpSlnFilePath);
            }
            finally
            {
                File.Delete(tmpSlnFilePath);
            }
           
            List<CsprojNameAndPath> expectedResult = new List<CsprojNameAndPath>{
                new CsprojNameAndPath("AdvancedGraph", Path.Combine(tmpDirectoryPath, $"AdvancedGraph\\AdvancedGraph.csproj")),
                new CsprojNameAndPath("Extensions", Path.Combine(tmpDirectoryPath, $"Extensions\\Extensions.csproj")),
                new CsprojNameAndPath("CodeAnalysis", Path.Combine(tmpDirectoryPath, $"CodeAnalysis\\CodeAnalysis.csproj"))
            };
            
            Assert.NotNull(result);
            CollectionAssert.AreEqual(expectedResult, result);
        }
    }
}