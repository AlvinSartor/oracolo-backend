﻿using System;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper.Helpers
{
    public sealed class CsprojNameAndPath : IEquatable<CsprojNameAndPath>
    {
        /// <summary>
        /// Gets the csproj name.
        /// </summary>
        [NotNull] public string Name { get; }

        /// <summary>
        /// Gets the csproj path.
        /// </summary>
        [NotNull] public string Path { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CsprojNameAndPath"/> class.
        /// </summary>
        /// <param name="name">The name of the csproj file.</param>
        /// <param name="path">The path of the csproj file.</param>
        public CsprojNameAndPath([NotNull] string name, [NotNull] string path)
        {
            Name = name.NotNull(nameof(name));
            Path = path.NotNull(nameof(path));
        }

        /// <inheritdoc />
        public bool Equals(CsprojNameAndPath other)
        {
            return other != null
                   && other.Name.Equals(Name)
                   && other.Path.Equals(Path);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is CsprojNameAndPath csprojNameAndPath
                && csprojNameAndPath.Equals(this);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return (Name.GetHashCode() * 397) ^ Path.GetHashCode();
            }
        }
    }
}