﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper.Helpers
{
    internal static class SlnAnaliser
    {
        /// <summary>
        /// Gets the assemblies names and paths from a SLN file.
        /// </summary>
        /// <param name="slnFilePath">The SLN file path.</param>
        /// <returns>An array containing assemblies and relative paths in the format:
        /// {assemblyA, pathAssemblyA, assemblyB, pathAssemblyB, ...} 
        /// </returns>
        /// <exception cref="ArgumentException">The specified file does not exist</exception>
        /// <exception cref="ArgumentException">The given path does not lead to a '.sln' file</exception>
        /// <exception cref="ArgumentException">Impossible to get the folder of the specified file</exception>
        [NotNull]
        public static ImmutableList<CsprojNameAndPath> GetAssembliesNamesAndPathsFromSlnFile([NotNull] string slnFilePath)
        {
            string slnFileFolder = Path.GetDirectoryName(slnFilePath);

            if (!File.Exists(slnFilePath))
            {
                throw new ArgumentException($"The specified file does not exist: '{slnFilePath}'");
            }

            if (!slnFilePath.EndsWith("sln"))
            {
                throw new ArgumentException($"The given path does not lead to a '.sln' file: '{slnFilePath}'");
            }

            if (slnFileFolder == null)
            {
                throw new ArgumentException($"Impossible to get the folder of the specified file : '{slnFilePath}'");
            }            

            // The lines we are interested in look like this:
            // Project("{...}") = "Fugro.Anchors", "Fugro\Anchors\Fugro.Anchors.csproj", "{...}"
            IEnumerable<string[]> projectLines = File.ReadLines(slnFilePath)
                .Where(x => x.StartsWith("Project"))
                .Select(x => x.Substring(x.IndexOf('=') + 1).Split(','))
                .Select(x => x.Take(2)).Select(x => x.Select(s => s.Trim(' ', '"')).ToArray());

            // Now we have a IEnumerable<string[]> containing all project lines.
            // We want to filter out the 'Project' entries that do not lead to a Csproj file and finally complete the path.
            IEnumerable<CsprojNameAndPath> onlyMeaningful = projectLines
                .Where(x => x[1].EndsWith("csproj"))
                .Select(x => new[] {x[0], Path.Combine(slnFileFolder, x[1])})
                .Select(x => new CsprojNameAndPath(x[0], x[1]));

            return onlyMeaningful.ToImmutableList();
        }        
    }
}
