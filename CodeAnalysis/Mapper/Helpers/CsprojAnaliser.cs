﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.Mapper.Helpers
{
    /// <summary>
    /// This class is used to extract information from the CSPROJ files.
    /// </summary>
    internal static class CsprojAnaliser
    {
        /// <summary>
        /// Gets the output paths from csproj.
        /// </summary>
        /// <param name="csprojPath">The csproj file path.</param>
        /// <param name="csprojName">The csproj name.</param>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        [NotNull]
        public static string GetOutputPathsFromCsproj([NotNull] string csprojPath, [NotNull] string csprojName)
        {
            csprojPath.AssertNotNull(nameof(csprojPath));
            ThrowIfCsprojIsInvalid(csprojPath, out string parentDirectory);

            string outputPath = XDocument.Load(csprojPath)
                .Descendants()
                .First(x => x.Name.LocalName == "OutputPath")
                .Value;

            return Path.GetFullPath(Path.Combine(parentDirectory, outputPath, csprojName + ".dll"));
        }

        /// <summary>
        /// Given a '.csproj' file, it fetches the assemblies path.
        /// </summary>
        /// <remarks>Only references with a name and path to dll are retrieved, the others are excluded.</remarks>
        /// <param name="csprojFile">The csproj file.</param>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        [NotNull]
        public static ImmutableList<DllNameAndPath> GetAssembliesPathsFromCsproj([NotNull] string csprojFile)
        {
            csprojFile.AssertNotNull(nameof(csprojFile));
            ThrowIfCsprojIsInvalid(csprojFile, out string parentDirectory);

            ImmutableList<XElement> xmlDescendants = XDocument.Load(csprojFile).Descendants().ToImmutableList();

            // references to DLLs            
            IEnumerable<DllNameAndPath> dllNameAndPaths =
                from element in xmlDescendants.Where(x => x.Name.LocalName == "Reference")
                where element.Attribute("Include") != null && element.Descendants().Any(d => d.Name.LocalName == "HintPath")
                let name = element.Attribute("Include")?.Value.Split(',').First()
                let path = element.Descendants().First(d => d.Name.LocalName == "HintPath")?.Value
                select new DllNameAndPath(name, Path.GetFullPath(Path.Combine(parentDirectory, path)));

            // references to solution assemblies
            IEnumerable<DllNameAndPath> solutionReferencesNameAndPaths =
                from element in xmlDescendants.Where(x => x.Name.LocalName == "ProjectReference")
                where element.Attribute("Include") != null
                let path = element.Attribute("Include")?.Value
                let name = element.Descendants().First(d => d.Name.LocalName == "Name")?.Value
                select new DllNameAndPath(name, Path.GetFullPath(Path.Combine(parentDirectory, path)));

            return dllNameAndPaths.Concat(solutionReferencesNameAndPaths).ToImmutableList();
        }

        /// <summary>
        /// Given a '.csproj' file, it fetches the paths of all (C#) files it contains.
        /// </summary>
        /// <param name="csprojFile">The csproj file.</param>
        /// <exception cref="FileNotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        [NotNull]
        public static ImmutableList<string> GetAllCSharpFilePathsFromCsproj([NotNull] string csprojFile)
        {
            csprojFile.AssertNotNull(nameof(csprojFile));
            ThrowIfCsprojIsInvalid(csprojFile, out string parentDirectory);

            return XDocument.Load(csprojFile)
                .Descendants()
                .Where(x => x.Name.LocalName == "Compile")
                .Where(x => x.Descendants().All(d => d.Name.LocalName != "Link"))
                .Select(x => x.Attribute("Include")?.Value)
                .Where(x => x != null)
                .Select(path => Path.GetFullPath(Path.Combine(parentDirectory, path)))
                .ToImmutableList();
        }

        private static void ThrowIfCsprojIsInvalid([NotNull] string csprojFile, [NotNull] out string parentDirectory)
        {
            if (!File.Exists(csprojFile))
            {
                throw new FileNotFoundException($"Could not find the file at the specified path: {csprojFile}");
            }

            if (!csprojFile.EndsWith(".csproj"))
            {
                throw new ArgumentException($"It seems that the file passed is not a csproj. File: {Path.GetFileName(csprojFile)}");
            }

            parentDirectory = Path.GetDirectoryName(csprojFile)
                              ?? throw new ArgumentException($"Could not find the directory of: {Path.GetFileName(csprojFile)}");
        }
    }
}