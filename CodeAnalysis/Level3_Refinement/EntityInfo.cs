﻿using System.Collections.Generic;
using System.Collections.Immutable;
using CodeAnalysis.Level2_Extraction_DataStructure;
using JetBrains.Annotations;

namespace CodeAnalysis.Level3_Refinement
{
    /// <summary>
    /// Class used to store information about loaded entities.
    /// This information will be used to build graph connections.
    /// </summary>
    public sealed class EntityInfo
    {

        public EntityInfo(
            [NotNull] LoadedEntity loadedEntity, 
            [NotNull] IEnumerable<IEntity> inheritances, 
            [NotNull] IEnumerable<IEntity> dependencies)
        {
            LoadedEntity = loadedEntity;            
            Inheritances = inheritances.ToImmutableList();
            Dependencies = dependencies.ToImmutableList();
        }

        /// <summary>
        /// Gets the loaded entity this information belongs to.
        /// </summary>
        [NotNull] public LoadedEntity LoadedEntity { get; }
       
        /// <summary>
        /// The entity inheritances.
        /// </summary>
        [NotNull] public ImmutableList<IEntity> Inheritances { get; }

        /// <summary>
        /// The entity dependencies.
        /// </summary>
        [NotNull] public ImmutableList<IEntity> Dependencies { get; }



    }
}
