﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeAnalysis.NUnitExtensions;
using NUnit.Framework;

namespace CodeAnalysis.Level3_Refinement.NUnit
{
    [TestFixture]
    internal sealed class RefinementManagerFixture
    {
        private RefinementManager m_RefinementManager;

        [SetUp]
        public void SetUp()
        {
            m_RefinementManager = new RefinementManager();
        }

        [TearDown]
        public void TearDown()
        {
            m_RefinementManager.Dispose();
        }

        [Test]
        public void DirectoriesAreCorrectlyAdded()
        {
            string directoryPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Operators\");
            ImmutableList<EntityInfo> refinementResult = m_RefinementManager.RefineDirectory(directoryPath);

            List<string> expectedEntities = new List<string>
            {
                "Crossover",
                "Mutation",
                "Selection",
                "Replacement",
                "Initialization",
                "CrossoverFixture",
                "MutationFixture",
                "SelectionFixture",
                "ReplacementFixture",
                "InitializationFixture"
            };

            CollectionAssert.AreEquivalent(expectedEntities, refinementResult);         
        }

        [Test]
        public void MultipleFilesAreCorrectlyAdded()
        {
            ImmutableList<string> filePaths = new List<string>
            {
                TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs"),
                TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Population.cs")
            }.ToImmutableList();
            ImmutableList<EntityInfo> refinementResult = m_RefinementManager.RefineFiles(filePaths);

            List<string> expectedEntities = new List<string>
            {
                "Individual<T>",
                "Population<T>"
            };

            CollectionAssert.AreEquivalent(expectedEntities, refinementResult);
        }

        [Test]
        public void MultipleFilesAreCorrectlyAdded2()
        {
            ImmutableList<string> filePaths = new List<string>
            {
                $@"D:\Development\Projects\Starfix NG\Source\Fugro\Libraries\GeneticAlgorithms\Components\PopulationArchive.cs",
            }.ToImmutableList();
            ImmutableList<EntityInfo> refinementResult = m_RefinementManager.RefineFiles(filePaths);

            List<string> expectedEntities = new List<string>
            {
                "Individual<T>",
                "Population<T>"
            };

            CollectionAssert.AreEquivalent(expectedEntities, refinementResult);
        }

        [Test]
        public void AddingMultipleTimesTheSameFileDoesntChangeTheState()
        {
            string individualEntityPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs");
            ImmutableList<EntityInfo> result = m_RefinementManager.RefineFiles(new[] { individualEntityPath }.ToImmutableList());

            Assert.That(result.Count, Is.EqualTo(1));

            result = m_RefinementManager.RefineFiles(new[] { individualEntityPath }.ToImmutableList());

            Assert.That(result.Count, Is.EqualTo(0));
        }

        [Test]
        public void EntitiesAreStoredUsingTheirFullName()
        {
            string individualEntityPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs");
            ImmutableList<EntityInfo> result = m_RefinementManager.RefineFiles(new[] { individualEntityPath }.ToImmutableList());

            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.First().LoadedEntity.FullName, Is.EqualTo("Fugro.GeneticAlgorithms.Components.Individual<T>"));
        }

        [Test]
        public void InheritancesAreFoundAndStored()
        {
            ImmutableList<string> filePaths = new List<string>
            {
                TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Implementations\NumericalPopulation.cs")
            }.ToImmutableList();

            ImmutableList<EntityInfo> refinementResult = m_RefinementManager.RefineFiles(filePaths);
            var inheritances = refinementResult.Single().Inheritances;

            List<string> expectedInheritances = new List<string>
            {
                "Fugro.GeneticAlgorithms.Components.Population<T>"
            };

            CollectionAssert.AreEquivalent(expectedInheritances, inheritances);
        }

        [Test]
        public void DependenciesAreFoundAndStored()
        {
            ImmutableList<string> filePaths = new List<string>
            {
                TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs")
            }.ToImmutableList();

            ImmutableList<EntityInfo> refinementResult = m_RefinementManager.RefineFiles(filePaths);
            var dependencies = refinementResult.Single().Dependencies;

            List<string> expectedDependencies = new List<string>
            {
                "T",
                "string",
                "System.Guid",
                "Fugro.ArgumentHelper",
                "Fugro.Collections.ImmutableList<T>",
                "System.Collections.Generic.IEnumerable<T>"
            };

            CollectionAssert.AreEquivalent(expectedDependencies, dependencies);
        }
    }
}
