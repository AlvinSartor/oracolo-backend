﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using JetBrains.Annotations;
using CodeAnalysis.Level1_Mining;
using CodeAnalysis.Level1_Mining_DataStructure;
using CodeAnalysis.Level2_Extraction.Helpers;
using CodeAnalysis.Level2_Extraction_DataStructure;
using Extensions;
using Microsoft.CodeAnalysis;

namespace CodeAnalysis.Level2_Extraction
{
    /// <summary>
    /// The extraction manager gathers information from the compilation units
    /// </summary>
    internal sealed class ExtractionManager : IDisposable
    {
        [NotNull] private readonly MiningManager m_MiningManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtractionManager"/> class.
        /// </summary>
        public ExtractionManager()
        {
            m_MiningManager = new MiningManager();            
        }

        public async Task<IEnumerable<IEntity>> Load([NotNull] string filePath)
        {
            filePath.AssertNotNull(nameof(filePath));

            CompilationUnit compilationUnit = await m_MiningManager.LoadFromFile(filePath);
            SyntaxTree syntaxTree = compilationUnit.GetSyntaxTreeOf(filePath);

            return EntityBuilder.BuildEntity(filePath, compilationUnit);
        }

        /// <summary>
        /// Loads the specified entity name.
        /// </summary>
        /// <param name="entityName">Name of the entity.</param>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <exception cref="ArgumentException">The specified entity does not belong to the given assembly.</exception>
        public async Task<IEnumerable<IEntity>> Load(string entityName, string assemblyName)
        {
            assemblyName.AssertNotNull(nameof(assemblyName));
            entityName.AssertNotNull(nameof(entityName));

            CompilationUnit compilationUnit = await m_MiningManager.LoadFromAssembly(assemblyName);

            if (!compilationUnit.ContainsEntity(entityName))
            {
                throw new ArgumentException($"The specified entity '{entityName}' does not belong to the given assembly '{assemblyName}'.");
            }

            string entityPath = compilationUnit.GetPathOfEntity(entityName);
            return EntityBuilder.BuildEntity(entityPath, compilationUnit);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            m_MiningManager.Dispose();
        }
    }
}