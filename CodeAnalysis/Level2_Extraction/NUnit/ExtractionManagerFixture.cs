﻿using NUnit.Framework;

namespace CodeAnalysis.Level2_Extraction.NUnit
{
    [TestFixture]
    internal sealed class ExtractionManagerFixture
    {
        //private ExtractionManager m_ExtractionManager;

        //[SetUp]
        //public void SetUp()
        //{
        //    m_ExtractionManager = new ExtractionManager();
        //}

        //[TearDown]
        //public void TearDown()
        //{
        //    m_ExtractionManager.Dispose();
        //}

        //[Test]
        //public void ExtractionManagerIsInitializedEmpty()
        //{
        //    Assert.That(m_ExtractionManager.SyntaxTrees.Values, Is.Empty);
        //    Assert.That(m_ExtractionManager.ProjectCompilation.SyntaxTrees, Is.Empty);
        //    Assert.That(m_ExtractionManager.ProjectCompilation.AssemblyName, Is.EqualTo("CodeAnalysisCompilation"));
        //}

        //[Test]
        //public void DirectoriesAreCorrectlyAdded()
        //{
        //    string directoryPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Operators\");
        //    var extractionResult = m_ExtractionManager.ExtractDirectory(directoryPath);

        //    Assert.That(m_ExtractionManager.Contains(TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Operators\Crossover.cs")));
        //    Assert.That(m_ExtractionManager.Contains(TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Operators\NUnit\CrossoverFixture.cs")));

        //    Assert.That(m_ExtractionManager.SyntaxTrees.Values.Count(), Is.EqualTo(33));
        //    Assert.That(extractionResult.Values.Count(), Is.EqualTo(10));
        //}

        //[Test]
        //public void MultipleFilesAreCorrectlyAdded()
        //{
        //    string individualEntityPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs");
        //    string populationEntityPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Population.cs");
        //    var extractionResult = m_ExtractionManager.Extract(new[] {individualEntityPath, populationEntityPath}.ToImmutableList());

        //    Assert.That(m_ExtractionManager.Contains(individualEntityPath));
        //    Assert.That(m_ExtractionManager.Contains(populationEntityPath));

        //    Assert.That(m_ExtractionManager.SyntaxTrees.Values.Count(), Is.EqualTo(33));
        //    Assert.That(m_ExtractionManager.ProjectCompilation.SyntaxTrees.Count(), Is.EqualTo(33));

        //    Assert.That(extractionResult.Values.Count(), Is.EqualTo(2));
        //}

        //[Test]
        //public void AddingMultipleTimesTheSameFileDoesntChangeTheState()
        //{
        //    string individualEntityPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs");
        //    var extractionResult = m_ExtractionManager.Extract(new[] { individualEntityPath }.ToImmutableList());

        //    Assert.That(m_ExtractionManager.Contains(individualEntityPath));
        //    Assert.That(extractionResult.Values.Count(), Is.EqualTo(1));
        //    Assert.That(m_ExtractionManager.SyntaxTrees.Values.Count(), Is.EqualTo(33));

        //    m_ExtractionManager.Extract(new[] {individualEntityPath}.ToImmutableList());
        //    m_ExtractionManager.Extract(new[] {individualEntityPath}.ToImmutableList());
        //    m_ExtractionManager.Extract(new[] {individualEntityPath}.ToImmutableList());

        //    Assert.That(m_ExtractionManager.Contains(individualEntityPath));
        //    Assert.That(extractionResult.Values.Count(), Is.EqualTo(1));
        //    Assert.That(m_ExtractionManager.SyntaxTrees.Values.Count(), Is.EqualTo(33));
        //}

        //[Test]
        //public void ItIsPossibleToRetreiveEntitiesUsingTheirPath()
        //{
        //    string individualEntityPath = TestClassesFetcher.GetPathOfTestData($@"GeneticAlgorithms\Components\Individual.cs");
        //    m_ExtractionManager.Extract(new[] { individualEntityPath }.ToImmutableList());

        //    Assert.That(m_ExtractionManager.Contains(individualEntityPath));
        //    SyntaxTree tree = m_ExtractionManager[individualEntityPath];

        //    Assert.That(tree.GetText().ToString(), Is.EqualTo(File.ReadAllText(individualEntityPath)));
        //}
    }
}
