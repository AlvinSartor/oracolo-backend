﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeAnalysis.Level2_Extraction_DataStructure;
using Extensions;
using JetBrains.Annotations;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeAnalysis.Level2_Extraction.Helpers
{
    internal static class SyntaxTreeAnaliser
    {
        [NotNull]
        public static string ExtractSyntaxNodeFullName([NotNull] SyntaxNode syntaxNode, [NotNull] SemanticModel model)
        {
            return model.GetDeclaredSymbol(syntaxNode).OriginalDefinition.ToString();
        }

        [NotNull]
        public static ImmutableList<(string assembly, string fullName, EntityType type)> GetInheritances(
            [NotNull] SyntaxNode typeDeclarationNode, 
            [NotNull] SemanticModel model)
        {
            SyntaxNode inheritancesList = typeDeclarationNode.ChildNodes().OfType<BaseListSyntax>().SingleOrDefault();

            if (inheritancesList == null)
            {
                return ImmutableList<(string assembly, string fullName, EntityType type)>.Empty;
            }

            //return inheritancesList
            //    .ChildNodes().OfType<SimpleBaseTypeSyntax>()
            //    .Select(node => NodeAnaliser.GetNodesDefinitonForInheritances(node.ChildNodes().First(), model))
            //    .ToImmutableList();
            throw new NotImplementedException();
        }       

        [NotNull]
        public static ImmutableList<(string assembly, string fullName, EntityType type)> GetDependencies(
            [NotNull] SyntaxNode typeDeclarationNode, 
            [NotNull] SemanticModel model)
        {
            List<SyntaxNode> children = typeDeclarationNode.ChildNodes().ToList();            

            IEnumerable<SyntaxNode> inheritancesList = children.OfType<BaseListSyntax>();
            IEnumerable<SyntaxNode> fields = children.OfType<FieldDeclarationSyntax>();
            IEnumerable<SyntaxNode> propertyDependencies = children.OfType<PropertyDeclarationSyntax>();
            IEnumerable<SyntaxNode> methodsDependencies = children.OfType<MethodDeclarationSyntax>();
            methodsDependencies = methodsDependencies.Concat(children.OfType<ConstructorDeclarationSyntax>());

            //return inheritancesList.SelectMany(x => GetDependenciesInInheritancesList(x, model))
            //    .Concat(fields.SelectMany(x => GetDependencyInField(x, model)))
            //    .Concat(methodsDependencies.SelectMany(x => GetDependenciesInMethods(x, model)))
            //    .Concat(propertyDependencies.SelectMany(x => GetDependenciesInProperties(x, model)))
            //    .Distinct()
            //    .ToImmutableList();
            throw new NotImplementedException();
        }

        [NotNull]
        private static ImmutableList<string> GetDependenciesInInheritancesList([NotNull] SyntaxNode inheritancesList, [NotNull] SemanticModel model)
        {
            return inheritancesList
                .ChildNodes().OfType<SimpleBaseTypeSyntax>()
                .Where(node => node.ChildNodes().First() is GenericNameSyntax)
                .SelectMany(node => NodeAnaliser.GetInnerTypeOfGenericNode((GenericNameSyntax) node.ChildNodes().First(), model))
                .ToImmutableList();
        }

        [NotNull]
        private static ImmutableList<string> GetDependencyInField([NotNull] SyntaxNode declaration, [NotNull] SemanticModel model)
        {
            return declaration
                .ChildNodes().OfType<VariableDeclarationSyntax>().Single()
                .ChildNodes().First()
                .Apply(node => NodeAnaliser.GetNodeOriginalDefinitions(node, model));            
        }

        [NotNull]
        private static IEnumerable<string> GetDependenciesInProperties([NotNull] SyntaxNode declaration, [NotNull] SemanticModel model)
        {
            List<SyntaxNode> children = declaration.ChildNodes().ToList();

            IEnumerable<string> identifier = children.OfType<IdentifierNameSyntax>().SelectMany(x => NodeAnaliser.GetNodeOriginalDefinitions(x, model));
            IEnumerable<string> generic = children.OfType<GenericNameSyntax>().SelectMany(x => NodeAnaliser.GetNodeOriginalDefinitions(x, model));
            IEnumerable<string> blockTypes = new List<string>();

            IEnumerable<ArrowExpressionClauseSyntax> arrowExpression = children.OfType<ArrowExpressionClauseSyntax>();
            IEnumerable<AccessorListSyntax> accessorListExpression = children.OfType<AccessorListSyntax>();

            blockTypes = blockTypes
                .Concat(arrowExpression.SelectMany(node => NodeAnaliser.BruteForceTypeSearch(node, model)))
                .Concat(accessorListExpression.SelectMany(node => NodeAnaliser.BruteForceTypeSearch(node, model)));
            
            return identifier.Concat(generic).Concat(blockTypes).Distinct();
        }

        [NotNull]
        private static IEnumerable<string> GetDependenciesInMethods([NotNull] SyntaxNode declaration, [NotNull] SemanticModel model)
        {
            IEnumerable<string> returnType = GetDependenciesInProperties(declaration, model);
            IEnumerable<string> parameters = AnalyseParameters(declaration.ChildNodes().OfType<ParameterListSyntax>().Single(), model);

            SyntaxNode blockNode = declaration.ChildNodes().OfType<BlockSyntax>().SingleOrDefault();
            IEnumerable<string> block = blockNode != null
                ? AnalyseBlock(declaration.ChildNodes().OfType<BlockSyntax>().Single(), model)
                : new string[] { };

            return returnType.Concat(parameters).Concat(block);
        }

        [NotNull]
        private static IEnumerable<string> AnalyseParameters([NotNull] SyntaxNode parameterListNode, [NotNull] SemanticModel model)
        {
            return parameterListNode
                .ChildNodes()
                .OfType<ParameterSyntax>()
                .SelectMany(x => GetDependenciesInProperties(x, model));
        }

        [NotNull]
        private static IEnumerable<string> AnalyseBlock([NotNull] SyntaxNode blockNode, [NotNull] SemanticModel model)
        {
            return NodeAnaliser.BruteForceTypeSearch(blockNode, model);
        }

        public static string ExtractAssembly(SyntaxNode typeDeclarationNode, SemanticModel model)
        {
            throw new System.NotImplementedException();
        }

        public static EntityType ExtractEntityType(SyntaxNode typeDeclarationNode, SemanticModel model)
        {
            throw new System.NotImplementedException();
        }
    }
}