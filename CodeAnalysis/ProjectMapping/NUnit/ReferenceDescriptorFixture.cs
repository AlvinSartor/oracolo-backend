﻿using CodeAnalysis.ProjectMapping.NUnitExtensions;
using NUnit.Framework;

namespace CodeAnalysis.ProjectMapping.NUnit
{
    [TestFixture]
    internal sealed class ReferenceDescriptorFixture
    {
        [Test]
        public void ReferenceDescriptorIsInitializedCorrectly()
        {
            const string name = "foo";
            string path = $@"c:\ProjectStructure\FooSolution\Assemblies\foo.dll";
            var csprojDescriptor = new TestCsprojDescriptor();

            var referenceDescriptor = new ReferenceDescriptor(name, path, csprojDescriptor);

            // csproj descriptor
            Assert.That(referenceDescriptor.Csproj.FilePath, Is.EqualTo(csprojDescriptor.FilePath));

            // reference descriptor
            Assert.That(referenceDescriptor.AssemblyName, Is.EqualTo("foo"));
            Assert.That(referenceDescriptor.DllPath, Is.EqualTo(path));
        }
    }
}
