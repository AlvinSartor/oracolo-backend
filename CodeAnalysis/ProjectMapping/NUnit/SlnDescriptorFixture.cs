﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CodeAnalysis.Mapper.Helpers;
using CodeAnalysis.ProjectMapping.Interfaces;
using CodeAnalysis.ProjectMapping.NUnitExtensions;
using JetBrains.Annotations;
using NUnit.Framework;

namespace CodeAnalysis.ProjectMapping.NUnit
{
    [TestFixture]
    internal sealed class SlnDescriptorFixture
    {
        [Test]
        public void SlnDescriptorIsInitializedCorrectly()
        {
            string path = $@"c:\ProjectStructure\FooSolution\Foo.sln";

            var slnDescriptor = new SlnDescriptor(path, CsprojFinderFunc, CsprojOutputPathFinderFunc, CsharpFinderFunc, DllFinderFunc);

            // sln descriptor
            Assert.That(slnDescriptor.SlnName, Is.EqualTo("Foo"));
            Assert.That(slnDescriptor.FilePath, Is.EqualTo(path));

            // csproj descriptors
            var csprojs = slnDescriptor.Csprojs.Values.ToImmutableList();
            Assert.That(csprojs.Count, Is.EqualTo(2));
            Assert.That(csprojs[0].CsprojName, Is.EqualTo("Bam"));
            Assert.That(csprojs[1].CsprojName, Is.EqualTo("Bar"));

            // files descriptors
            ImmutableList<FileDescriptor> filesList = slnDescriptor.Csprojs.SelectMany(x => x.Value.Files).ToImmutableList();

            Assert.That(filesList.Count, Is.EqualTo(6));
            Assert.That(filesList.All(file => file.Csproj.CsprojName == "Bam" || file.Csproj.CsprojName == "Bar"));
            Assert.That(filesList.Count(x => x.FileName.Equals("class1.cs")), Is.EqualTo(2));
            Assert.That(filesList.Count(x => x.FileName.Equals("class2.cs")), Is.EqualTo(2));
            Assert.That(filesList.Count(x => x.FileName.Equals("class3.cs")), Is.EqualTo(2));
        }

        [Test]
        public void InternalReferencesCanBeIdentified()
        {
            string path = $@"c:\ProjectStructure\FooSolution\Foo.sln";

            var slnDescriptor = new SlnDescriptor(path, CsprojFinderFunc, CsprojOutputPathFinderFunc, CsharpFinderFunc, DllFinderFunc);

            var internalReference = new ReferenceDescriptor("Bam", CsprojOutputPathFinderFunc("", "Bar"), new TestCsprojDescriptor());
            var externalReference = new ReferenceDescriptor("ThisIsExternal", CsprojOutputPathFinderFunc("", "External"), new TestCsprojDescriptor());

            Assert.True(slnDescriptor.IsReferenceInternal(internalReference));
            Assert.False(slnDescriptor.IsReferenceInternal(externalReference));
        }

        [Test]
        public void AllInternalReferencesCanBeFetched()
        {
            string path = $@"c:\ProjectStructure\FooSolution\Foo.sln";

            var slnDescriptor = new SlnDescriptor(path, CsprojFinderFunc, CsprojOutputPathFinderFunc, CsharpFinderFunc, DllFinderFunc);

            ICsprojDescriptor bamCsproj = slnDescriptor.Csprojs.Values.Single(csproj => csproj.CsprojName.Equals("Bam"));
            ICsprojDescriptor barCsproj = slnDescriptor.Csprojs.Values.Single(csproj => csproj.CsprojName.Equals("Bar"));

            ImmutableList<ICsprojDescriptor> bamInternalReferences = slnDescriptor.InternalReferencesOf(bamCsproj);
            ImmutableList<ICsprojDescriptor> barInternalReferences = slnDescriptor.InternalReferencesOf(barCsproj);

            CollectionAssert.AreEquivalent(new List<ICsprojDescriptor> {barCsproj}, bamInternalReferences);
            CollectionAssert.AreEquivalent(new List<ICsprojDescriptor>(), barInternalReferences);
        }

        [NotNull]
        private static ImmutableList<DllNameAndPath> DllFinderFunc([NotNull] string csprojPath)
        {
            return csprojPath.EndsWith("Bam.csproj")
                ? new List<DllNameAndPath>
                {
                    new DllNameAndPath("Bar", CsprojOutputPathFinderFunc("", "Bar")),
                    new DllNameAndPath("SomethingExternal", CsprojOutputPathFinderFunc("", "SomethingExternal")),
                }.ToImmutableList()
                : new List<DllNameAndPath> {new DllNameAndPath("SomethingExternal", CsprojOutputPathFinderFunc("", "SomethingExternal")),}
                    .ToImmutableList();
        }

        [NotNull]
        private static string CsprojOutputPathFinderFunc([NotNull] string csprojPath, [NotNull] string csprojName)
        {
            return $@"c:\ProjectStructure\Output\{csprojName}.dll";
        }

        [NotNull]
        private static ImmutableList<string> CsharpFinderFunc([NotNull] string csprojPath)
        {
            return new List<string>
            {
                Path.Combine(csprojPath, "Files", "class1.cs"), Path.Combine(csprojPath, "Files", "class2.cs"),
                Path.Combine(csprojPath, "Files", "class3.cs")
            }.ToImmutableList();
        }

        [NotNull]
        private static ImmutableList<CsprojNameAndPath> CsprojFinderFunc([NotNull] string csprojPath)
        {
            return new List<CsprojNameAndPath>
            {
                new CsprojNameAndPath("Bam", Path.Combine(csprojPath, "ProjectBam", "Bam.csproj")),
                new CsprojNameAndPath("Bar", Path.Combine(csprojPath, "ProjectBar", "Bar.csproj")),
            }.ToImmutableList();
        }

        [Test, Explicit]
        public void PerformancesTest()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            string projectSlnPath = $@"D:\Development\Projects\Starfix NG\Source\StarfixDevelopment.sln";

            var slnDescriptor = new SlnDescriptor(projectSlnPath,
                                                  SlnAnaliser.GetAssembliesNamesAndPathsFromSlnFile,
                                                  CsprojAnaliser.GetOutputPathsFromCsproj,
                                                  CsprojAnaliser.GetAllCSharpFilePathsFromCsproj,
                                                  CsprojAnaliser.GetAssembliesPathsFromCsproj);

            stopwatch.Stop();

            Console.WriteLine($"Elapsed time: {stopwatch.ElapsedMilliseconds}");
            Console.WriteLine($"Tot references: {slnDescriptor.Csprojs.Values.Sum(csproj => csproj.References.Count)}");
            Console.WriteLine(
                $"Tot references (distinct): {slnDescriptor.Csprojs.Values.SelectMany(x => x.References).Select(x => x.DllPath).Distinct().Count()}");
        }
    }
}