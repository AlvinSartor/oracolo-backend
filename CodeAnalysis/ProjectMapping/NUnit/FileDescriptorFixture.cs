﻿using System;
using CodeAnalysis.ProjectMapping.NUnitExtensions;
using JetBrains.Annotations;
using NUnit.Framework;

namespace CodeAnalysis.ProjectMapping.NUnit
{
    [TestFixture]
    internal sealed class FileDescriptorFixture
    {
        [Test]
        public void FileDescriptorIsInitializedCorrectly()
        {
            string path = $@"c:\ProjectStructure\FooSolution\BamProject\classes\foo.cs";
            var csprojDescriptor = new TestCsprojDescriptor();

            var fileDescriptor = new FileDescriptor(path, csprojDescriptor);

            // csproj descriptor
            Assert.That(fileDescriptor.Csproj.FilePath, Is.EqualTo(csprojDescriptor.FilePath));

            // file descriptor
            Assert.That(fileDescriptor.FilePath, Is.EqualTo(path));
            Assert.That(fileDescriptor.FileName, Is.EqualTo("foo.cs"));
            Assert.That(fileDescriptor.FileNameWithoutExtension, Is.EqualTo("foo"));
        }

        [TestCase("foo.cs")]
        [TestCase("foo.yolo")]
        [TestCase("c:\\ProjectStructure\\FooSolution\\BamProject\\classes\\foo")]
        public void InvalidPathCausesException([NotNull] string path)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var _ = new FileDescriptor(path, new TestCsprojDescriptor());
            });            
        }
    }
}
