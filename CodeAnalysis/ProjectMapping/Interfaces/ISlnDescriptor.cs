﻿using System;
using System.Collections.Immutable;
using JetBrains.Annotations;

namespace CodeAnalysis.ProjectMapping.Interfaces
{    
    public interface ISlnDescriptor : IEquatable<ISlnDescriptor>
    {
        /// <summary>
        /// Gets the name of this Sln.
        /// </summary>
        [NotNull] string SlnName { get; }

        /// <summary>
        /// Gets the file path of this Sln.
        /// </summary>
        [NotNull] string FilePath { get; }

        /// <summary>
        /// Gets the csprojs dictiornaries (key = csprojName).
        /// </summary>
        [NotNull] ImmutableDictionary<string, ICsprojDescriptor> Csprojs { get; }

        /// <summary>
        /// Reloads the content of this SLN.
        /// </summary>
        void ReloadSlnContent();

        /// <summary>
        /// Determines whether the specified reference is internal to the project.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns>
        ///   <c>true</c> if it is a reference to an existing csproj; otherwise, <c>false</c>.
        /// </returns>
        bool IsReferenceInternal([NotNull] ReferenceDescriptor reference);

        /// <summary>
        /// Retrieve the internals references of the specified csproj.
        /// </summary>
        /// <param name="csproj">The csproj which references we are interested in.</param>
        [NotNull]
        ImmutableList<ICsprojDescriptor> InternalReferencesOf([NotNull] ICsprojDescriptor csproj);
    }
}
