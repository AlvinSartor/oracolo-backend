﻿using System;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using CodeAnalysis.Mapper.Helpers;
using CodeAnalysis.ProjectMapping.Interfaces;
using JetBrains.Annotations;

namespace CodeAnalysis.ProjectMapping
{
    public sealed class SlnDescriptor : ISlnDescriptor
    {
        [NotNull] private readonly Func<ImmutableDictionary<string, ICsprojDescriptor>> m_SlnToCsproj;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SlnDescriptor" /> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="slnToCsproj">The function used to retrieve the csproj files from a sln file.</param>
        /// <param name="csprojOutputPath">The function used to retrieve the csproj output path (to dll).</param>
        /// <param name="csprojToCSharp">The function used to retrieve the cSharp files from a csproj file.</param>
        /// <param name="csprojToReferences">The function used to retrieve the reference files from a csproj file</param>
        public SlnDescriptor(
            [NotNull] string filePath,
            [NotNull] Func<string, ImmutableList<CsprojNameAndPath>> slnToCsproj,
            [NotNull] Func<string, string, string> csprojOutputPath,
            [NotNull] Func<string, ImmutableList<string>> csprojToCSharp,
            [NotNull] Func<string, ImmutableList<DllNameAndPath>> csprojToReferences)
        {
            FilePath = filePath;
            SlnName = Path.GetFileNameWithoutExtension(FilePath);

            m_SlnToCsproj = () => slnToCsproj(filePath)
                                  .Select(nameAndPath => new CsprojDescriptor(nameAndPath, this, csprojOutputPath, csprojToCSharp, csprojToReferences))
                                  .ToImmutableDictionary(x => x.CsprojName, x => (ICsprojDescriptor) x);

            Csprojs = m_SlnToCsproj();
        }

        /// <inheritdoc />
        public string SlnName { get; }

        /// <inheritdoc />
        public string FilePath { get; }

        /// <inheritdoc />
        public ImmutableDictionary<string, ICsprojDescriptor> Csprojs { get; private set; }

        /// <inheritdoc />
        public void ReloadSlnContent()
        {
            Csprojs = m_SlnToCsproj();
        }

        /// <inheritdoc />
        public bool IsReferenceInternal(ReferenceDescriptor reference)
        {
            return Csprojs.ContainsKey(reference.AssemblyName);
        }

        /// <inheritdoc />
        public ImmutableList<ICsprojDescriptor> InternalReferencesOf(ICsprojDescriptor csproj)
        {
            return csproj.References.Where(IsReferenceInternal).Select(reference => Csprojs[reference.AssemblyName]).ToImmutableList();
        }

        /// <inheritdoc />
        public bool Equals(ISlnDescriptor other)
        {
            return other != null
                   && other.SlnName == SlnName
                   && other.FilePath == FilePath;
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            return other is ISlnDescriptor descriptor && Equals(descriptor);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = StringComparer.InvariantCulture.GetHashCode(SlnName);
                hashCode = (hashCode * 397) ^ StringComparer.InvariantCulture.GetHashCode(FilePath);
                return hashCode;
            }
        }
    }
}