﻿using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.ProjectMapping
{
    public sealed class ReferenceDescriptor
    {
        /// <summary>
        /// Gets the name of the referenced assembly.
        /// </summary>
        [NotNull] public string AssemblyName { get; }

        /// <summary>
        /// Gets the path of the referenced assembly.
        /// </summary>
        [NotNull] public string DllPath { get; }

        /// <summary>
        /// Gets the csproj this reference belongs to.
        /// </summary>
        [NotNull] public ICsprojDescriptor Csproj { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDescriptor"/> class.
        /// </summary>
        /// <param name="name">The reference name.</param>
        /// <param name="path">The reference path.</param>
        /// <param name="csprojDescriptor">The csproj this reference belongs to.</param>
        public ReferenceDescriptor([NotNull] string name, [NotNull] string path, [NotNull] ICsprojDescriptor csprojDescriptor)
        {
            AssemblyName = name.NotNull(nameof(name));
            DllPath = path.NotNull(nameof(path));
            Csproj = csprojDescriptor.NotNull(nameof(csprojDescriptor));
        }
    }
}
