﻿using System;
using System.Collections.Immutable;
using System.Linq;
using CodeAnalysis.Mapper.Helpers;
using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;

namespace CodeAnalysis.ProjectMapping
{
    public sealed class CsprojDescriptor : ICsprojDescriptor
    {
        [NotNull] private readonly Func<ImmutableList<FileDescriptor>> m_CsprojToFiles;
        [NotNull] private readonly Func<ImmutableList<ReferenceDescriptor>> m_CsprojToReferences;

        /// <inheritdoc />
        public string CsprojName { get; }

        /// <inheritdoc />
        public string FilePath { get; }

        /// <inheritdoc />
        public string OutputPath { get; }
        
        /// <inheritdoc />
        public ImmutableList<FileDescriptor> Files { get; private set; }

        /// <inheritdoc />
        public ImmutableList<ReferenceDescriptor> References { get; private set; }

        /// <inheritdoc />
        public ISlnDescriptor SlnDescriptor { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CsprojDescriptor"/> class.
        /// </summary>
        /// <param name="nameAndPath">The name and path of this Csproj file.</param>
        /// <param name="slnDescriptor">The SLN this csproj belongs to.</param>
        /// <param name="csprojOutputPath">The function used to retrieve the output path of this csproj.</param>
        /// <param name="csprojToFiles">The function used to retrieve the content of this csproj.</param>
        /// <param name="csprojToRefs">The function used to retrieve the references of this csproj.</param>
        public CsprojDescriptor(
            [NotNull] CsprojNameAndPath nameAndPath, 
            [NotNull] ISlnDescriptor slnDescriptor, 
            [NotNull] Func<string, string, string> csprojOutputPath,
            [NotNull] Func<string, ImmutableList<string>> csprojToFiles,
            [NotNull] Func<string, ImmutableList<DllNameAndPath>> csprojToRefs)
        {
            nameAndPath.AssertNotNull(nameof(nameAndPath));

            CsprojName = nameAndPath.Name;
            FilePath = nameAndPath.Path;
            SlnDescriptor = slnDescriptor.NotNull(nameof(slnDescriptor));

            csprojOutputPath.AssertNotNull(nameof(csprojOutputPath));
            OutputPath = csprojOutputPath(FilePath, CsprojName);

            csprojToFiles.AssertNotNull(nameof(csprojToFiles));
            m_CsprojToFiles = () => csprojToFiles(FilePath)
                .Select(csFilePath => new FileDescriptor(csFilePath, this))
                .ToImmutableList();
            Files = m_CsprojToFiles();

            csprojToRefs.AssertNotNull(nameof(csprojToRefs));
            m_CsprojToReferences = () => csprojToRefs(FilePath)
                .Select(dllNameAndPath => new ReferenceDescriptor(dllNameAndPath.Name, dllNameAndPath.Path, this))
                .ToImmutableList();
            References = m_CsprojToReferences();
        }

        /// <inheritdoc />
        public void ReloadCsprojContent()
        {
            Files = m_CsprojToFiles();
            References = m_CsprojToReferences();
        }

        /// <inheritdoc />
        public bool Equals(ICsprojDescriptor other)
        {
            return other != null
                   && other.CsprojName == CsprojName
                   && other.FilePath == FilePath
                   && other.OutputPath == OutputPath
                   && other.SlnDescriptor.SlnName == SlnDescriptor.SlnName;
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            return other is ICsprojDescriptor descriptor && Equals(descriptor);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = StringComparer.InvariantCulture.GetHashCode(CsprojName);
                hashCode = (hashCode * 397) ^ StringComparer.InvariantCulture.GetHashCode(FilePath);
                hashCode = (hashCode * 397) ^ StringComparer.InvariantCulture.GetHashCode(OutputPath);
                hashCode = (hashCode * 397) ^ SlnDescriptor.GetHashCode();
                return hashCode;
            }
        }

        /// <inheritdoc />
        public override string ToString() => CsprojName;
    }
}
