﻿using System.Collections.Immutable;
using CodeAnalysis.ProjectMapping.Interfaces;

namespace CodeAnalysis.ProjectMapping.NUnitExtensions
{
    internal sealed class TestCsprojDescriptor : ICsprojDescriptor
    {
        public TestCsprojDescriptor()
        {
            CsprojName = "Bam.csproj";

            FilePath = $@"c:\ProjectStructure\FooSolution\BamProject\Bam.csproj";
            OutputPath = $@"c:\ProjectStructure\FooSolution\Output\Bam.dll";

            SlnDescriptor = new TestSlnDescriptor();
            Files = ImmutableList<FileDescriptor>.Empty;
            References = ImmutableList<ReferenceDescriptor>.Empty;
        }

        /// <inheritdoc />
        public string CsprojName { get; }

        /// <inheritdoc />
        public string FilePath { get; }

        /// <inheritdoc />
        public string OutputPath { get; }

        /// <inheritdoc />
        public ImmutableList<FileDescriptor> Files { get; }

        /// <inheritdoc />
        public ImmutableList<ReferenceDescriptor> References { get; }

        /// <inheritdoc />
        public ISlnDescriptor SlnDescriptor { get; }

        /// <inheritdoc />
        public void ReloadCsprojContent() { }

        /// <inheritdoc />
        public bool Equals(ICsprojDescriptor other)
        {
            return other != null && other.CsprojName == CsprojName;
        }
    }
}
