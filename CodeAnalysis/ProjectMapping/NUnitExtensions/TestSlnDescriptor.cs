﻿using System.Collections.Immutable;
using CodeAnalysis.ProjectMapping.Interfaces;

namespace CodeAnalysis.ProjectMapping.NUnitExtensions
{
    internal sealed class TestSlnDescriptor : ISlnDescriptor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestSlnDescriptor"/> class.
        /// </summary>
        public TestSlnDescriptor()
        {
            FilePath = $@"c:\ProjectStructure\FooSolution\foo.sln";
            SlnName = "foo.sln";

            Csprojs = ImmutableDictionary<string, ICsprojDescriptor>.Empty;
        }

        /// <inheritdoc />
        public string SlnName { get; }

        /// <inheritdoc />
        public string FilePath { get; }

        /// <inheritdoc />
        public ImmutableDictionary<string, ICsprojDescriptor> Csprojs { get; }

        /// <inheritdoc />
        public void ReloadSlnContent() { }

        /// <inheritdoc />
        public bool IsReferenceInternal(ReferenceDescriptor reference)
        {
            return false;
        }

        /// <inheritdoc />
        public ImmutableList<ICsprojDescriptor> InternalReferencesOf(ICsprojDescriptor csproj)
        {
            return ImmutableList<ICsprojDescriptor>.Empty;
        }

        /// <inheritdoc />
        public bool Equals(ISlnDescriptor other)
        {
            return other != null && other.SlnName == SlnName;
        }
    }
}
