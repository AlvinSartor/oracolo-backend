﻿using System;
using System.Collections.Immutable;
using CodeAnalysis.Level3_Refinement;
using JetBrains.Annotations;

namespace CodeAnalysis.Level4_Fusion
{
    public sealed class EntitiesRelationshipsManager : IDisposable
    {        
        [NotNull] private readonly RefinementManager m_RefinementManager;
        [NotNull] private readonly EntitiesRelationshipCache m_EntitiesRelationshipCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntitiesRelationshipsManager"/> class.
        /// </summary>
        public EntitiesRelationshipsManager()
        {
            m_RefinementManager = new RefinementManager();
            m_EntitiesRelationshipCache = new EntitiesRelationshipCache();
        }

        /// <summary>
        /// Loads the files contained in the specified directory to the internal.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        public void LoadDirectory([NotNull] string directoryPath)
        {
            ImmutableList<EntityInfo> entities = m_RefinementManager.RefineDirectory(directoryPath);
            m_EntitiesRelationshipCache.AddEntitiesToCache(entities);
        }

        /// <summary>
        /// Loads the specified files to the internal cache.
        /// </summary>
        /// <param name="paths">The paths of the files to load.</param>
        public void LoadFiles([NotNull] ImmutableList<string> paths)
        {
            ImmutableList<EntityInfo> entities = m_RefinementManager.RefineFiles(paths);
            m_EntitiesRelationshipCache.AddEntitiesToCache(entities);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            m_RefinementManager.Dispose();
        }
    }
}
