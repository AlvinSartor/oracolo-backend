﻿using System;
using System.Collections.Generic;
using AdvancedGraph;
using CodeAnalysis.Level2_Extraction_DataStructure;
using CodeAnalysis.Level3_Refinement;
using JetBrains.Annotations;

namespace CodeAnalysis.Level4_Fusion
{
    internal sealed class EntitiesRelationshipCache
    {
        private readonly Dictionary<string, EntityDescription> m_LoadedEntities;
        private readonly Dictionary<string, EntityDescription> m_OnlyReferencedEntities;

        private readonly Graph<EntityDescription> m_Inheritances;
        private readonly Graph<EntityDescription> m_Dependencies;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntitiesRelationshipCache"/> class.
        /// </summary>
        public EntitiesRelationshipCache()
        {
            m_LoadedEntities = new Dictionary<string, EntityDescription>();
            m_OnlyReferencedEntities = new Dictionary<string, EntityDescription>();

            m_Inheritances = new Graph<EntityDescription>();
            m_Dependencies = new Graph<EntityDescription>();
        }

        /// <summary>
        /// Adds the entities to the cache.
        /// </summary>
        /// <param name="entitiesList">The entities list.</param>
        public void AddEntitiesToCache([NotNull] IReadOnlyCollection<EntityInfo> entitiesList)
        {
            //(IEnumerable<EntityDescription> alreadyLoaded, IEnumerable<EntityDescription> notLoadedYet) = entitiesList
            //    .Select(x => new EntityDescription(x))
            //    .Divide(x => m_LoadedEntities.ContainsKey(x.FullName));

            //// adding nodes to graphs
            //notLoadedYet.ForEach(entity => AddEntityToGraphs(entity, true));

            //// cleaning nodes to reload
            //alreadyLoaded.ForEach(ClearConnectionsInGraphs);

            //// adding connections for each node
            //foreach (var entity in entitiesList)
            //{
            //    Node<EntityDescription> inheritanceNode = m_Inheritances.GetNode(m_LoadedEntities[entity.FullName]);
            //    foreach (var inheritance in entity.Inheritances)
            //    {
            //        //AddUnloadedEntityIfDoesntExist(inheritance);
            //        inheritanceNode.AddConnection(m_Inheritances[m_LoadedEntities[inheritance]]);
            //    }

            //    Node<EntityDescription> dependencyNode = m_Dependencies.GetNode(m_LoadedEntities[entity.FullName]);
            //    foreach (var dependency in entity.Dependencies)
            //    {
            //        //AddUnloadedEntityIfDoesntExist(dependency);
            //        dependencyNode.AddConnection(m_Dependencies[m_LoadedEntities[dependency]]);
            //    }
            //}
        }

        private void AddUnloadedEntityIfDoesntExist(EntityInfo entity)
        {
            //if (m_OnlyReferencedEntities.ContainsKey(entity.FullName) || m_LoadedEntities.ContainsKey(entity.FullName))
            //{
            //    return;
            //}

            //var entityDescription = new EntityDescription(entity);
            //AddEntityToGraphs(entityDescription, false);
        }

        // ==>> TODO: REFACTOR
        private void AddEntityToGraphs([NotNull] EntityDescription entity, bool hasBeenLoaded)
        {
            string key = entity.FullName;

            if (m_LoadedEntities.ContainsKey(key))
            {
                throw new ArgumentException($"The entity '{key}' has already been added to the cache.");
            }

            // the entity has never been seen before
            if (!m_OnlyReferencedEntities.ContainsKey(key))
            {
                m_Inheritances.Add(entity);
                m_Dependencies.Add(entity);

                if (hasBeenLoaded)
                {
                    m_LoadedEntities.Add(key, entity);
                }
                else
                {
                    m_OnlyReferencedEntities.Add(key, entity);
                }
            }
            // the entity was cached, if it has been loaded now, we just move it to the loaded dictionary
            else if (hasBeenLoaded)
            {
                m_OnlyReferencedEntities.Remove(key);
                m_LoadedEntities.Add(key, entity);
            }
        }

        private void ClearConnectionsInGraphs([NotNull] EntityDescription entity)
        {
            m_Inheritances.GetNode(entity).ClearConnections();
            m_Dependencies.GetNode(entity).ClearConnections();
        }

        ///// <summary>
        ///// Gets the inheritance arity of the specified entity.
        ///// </summary>
        ///// <param name="entityName">The entity to check.</param>
        //public int InheritanceArity([NotNull] string entityName)
        //{
        //    return Entities.ContainsKey(entityName) && InheritanceGraph.Contains(Entities[entityName])
        //        ? InheritanceGraph[Entities[entityName]].OutDegree
        //        : throw new ArgumentException($"The entity '{entityName}' has not been found.");
        //}

        ///// <summary>
        ///// Gets the dependency arity of the specified entity.
        ///// </summary>
        ///// <param name="entityName">The entity to check.</param>
        //public int DependencyArity([NotNull] string entityName)
        //{
        //    return Entities.ContainsKey(entityName) && DependencyGraph.Contains(Entities[entityName])
        //        ? DependencyGraph[Entities[entityName]].OutDegree
        //        : throw new ArgumentException($"The entity '{entityName}' has not been found.");
        //}

        ///// <summary>
        ///// Gets the sum between inheritance and dependency arity of the specified entity.
        ///// </summary>
        ///// <param name="entityName">The entity to check.</param>
        //public int OverallArity([NotNull] string entityName)
        //{
        //    return Entities.ContainsKey(entityName) && DependencyGraph.Contains(Entities[entityName]) &&
        //           InheritanceGraph.Contains(Entities[entityName])
        //        ? InheritanceArity(entityName) + DependencyArity(entityName)
        //        : throw new ArgumentException($"The entity '{entityName}' has not been found.");
        //}

        ///// <summary>
        ///// Gets a tuple containing dependencies and inheritances of the specified entiy.
        ///// </summary>
        ///// <param name="entityName">Name of the entity.</param>
        //public (IEnumerable<INode<EntityDescription>> inheritances, IEnumerable<INode<EntityDescription>> dependencies) this
        //    [[NotNull] string entityName] =>
        //    Entities.ContainsKey(entityName) && DependencyGraph.Contains(Entities[entityName]) && InheritanceGraph.Contains(Entities[entityName])
        //        ? (InheritanceGraph[Entities[entityName]].Neighbors, DependencyGraph[Entities[entityName]].Neighbors)
        //        : throw new ArgumentException($"The entity '{entityName}' has not been found.");
    }
}