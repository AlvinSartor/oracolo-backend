﻿using CodeAnalysis.Level2_Extraction_DataStructure;
using NUnit.Framework;

namespace CodeAnalysis.Level4_Fusion.NUnit
{
    [TestFixture]
    internal sealed class EntityDescriptionFixture
    {
        [Test]
        public void EntityDescriptionIsInitializedCorrectly()
        {
            string filePath = $@"Nel\Mezzo\Del\Cammin\Di\Nostra\Vita.cs";
            const string fullName = "A.B.C.FooClass";

            var entityDescription = new EntityDescription(filePath, fullName);

            Assert.That(entityDescription.FilePath, Is.EqualTo(filePath));
            Assert.That(entityDescription.FullName, Is.EqualTo(fullName));
            Assert.That(entityDescription.Name, Is.EqualTo("FooClass"));
        }

        [Test]
        public void EntityDescriptionFromEntityInfoIsInitializedCorrectly()
        {
            //string filePath = $@"Nel\Mezzo\Del\Cammin\Di\Nostra\Vita.cs";
            //const string fullName = "A.B.C.FooClass";

            //var entityInfo = new EntityInfo(filePath, fullName, new List<string>(), new List<string>());
            //var entityDescription = new EntityDescription(entityInfo);

            //Assert.That(entityDescription.FilePath, Is.EqualTo(filePath));
            //Assert.That(entityDescription.FullName, Is.EqualTo(fullName));
            //Assert.That(entityDescription.Name, Is.EqualTo("FooClass"));
        }

        [Test]
        public void EqualContentTranslatesInEqualEntitiesDescription()
        {
            string filePath = $@"Nel\Mezzo\Del\Cammin\Di\Nostra\Vita.cs";
            const string fullName = "A.B.C.FooClass";

            var entityDescription1 = new EntityDescription(filePath, fullName);
            var entityDescription2 = new EntityDescription(filePath, fullName);

            Assert.That(entityDescription1, Is.EqualTo(entityDescription2));
        }

        [Test]
        public void NotEqualContentTranslatesInEqualEntitiesDescription()
        {
            string filePath1 = $@"Nel\Mezzo\Del\Cammin\Di\Nostra\Vita.cs";
            const string fullName1 = "A.B.C.FooClass";

            var entityDescription1 = new EntityDescription(filePath1, fullName1);

            string filePath2 = $@"Nel\Mezzo\Del\Cammin\Di\Nostra\YOLO.cs";
            const string fullName2 = "A.B.C.BarBaz";

            var entityDescription2 = new EntityDescription(filePath2, fullName2);

            Assert.That(entityDescription1, Is.Not.EqualTo(entityDescription2));
        }
    }
}
