﻿using System;
using System.IO;
using System.Reflection;
using JetBrains.Annotations;

namespace CodeAnalysis.NUnitExtensions
{
    internal static class TestClassesFetcher
    {
        /// <summary>
        /// Function used to get the path of the test data.
        /// </summary>
        /// <param name="pathOfInterest">The path of interest. For example "Components\Individual.cs"</param>
        [NotNull]
        public static string GetPathOfTestData([NotNull] string pathOfInterest)
        {
            string assemblyPath = Assembly.GetExecutingAssembly().CodeBase.Substring(8);
            string projectFolder = Directory.GetParent(assemblyPath).Parent?.Parent?.FullName;

            if (projectFolder == null)
            {
                throw new Exception("Something is wrong with the test directory path.");
            }

            string testFolder = Path.Combine(projectFolder, $"NUnitExtensions\\PLAYGROUND\\");
            string finalPath = Path.Combine(testFolder, pathOfInterest);

            return finalPath;
        }

    }
}
