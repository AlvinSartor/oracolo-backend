﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

#if DEBUG

[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyCulture("")]
[assembly: AssemblyCompany("Fugro Offshore Surveys")]
[assembly: AssemblyTrademark("Starfix")]

//#if NOT_CLS_COMPLIANT
//[assembly: CLSCompliant(false)]
//#else
//[assembly: CLSCompliant(true)]
//#endif

// This is a temporary measure until Microsoft release a CLSCompliant version
// of the Reactive Extensions (Rx).

[assembly: CLSCompliant(false)]
[assembly: ComVisible(false)]
[assembly: SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: NeutralResourcesLanguage("en", UltimateResourceFallbackLocation.MainAssembly)]
