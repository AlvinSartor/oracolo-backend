﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Util;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components
{
    public sealed class GenerationStatistics<T> : IImmutable
    {
        private readonly IndividualsWithFitness<T> m_BestIndividual;

        private readonly double m_BestFitness;
        private readonly double m_AverageFitness;
        private readonly double m_WorstFitness;

        private readonly double m_ImprovementOfAverage;
        private readonly double m_ImprovementOfBest;

        private readonly double m_AverageGeneticDiversity;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenerationStatistics{T}"/> class.
        /// </summary>>
        public GenerationStatistics(
            [NotNull] ImmutableList<IndividualsWithFitness<T>> individuals,
            [NotNull] Configuration configuration,
            [CanBeNull] GenerationStatistics<T> lastStatistic,
            [NotNull] Func<Individual<T>, Individual<T>, double> geneticDiversityFunction)
        {
            ArgumentHelper.AssertNotNull(individuals, nameof(individuals));
            ArgumentHelper.AssertNotNull(configuration, nameof(configuration));
            ArgumentHelper.AssertNotNull(geneticDiversityFunction, nameof(geneticDiversityFunction));

            double[] fitnesses = individuals.Select(x => x.GetFitness()).ToArray();
            bool isMaximization = configuration.IsMaximizationProblem;
            m_AverageFitness = fitnesses.Average();

            m_BestIndividual = isMaximization ? individuals.MaxBy(x => x.GetFitness()) : individuals.MinBy(x => x.GetFitness());
            m_BestFitness = m_BestIndividual.GetFitness();
            m_WorstFitness = isMaximization ? fitnesses.Min() : fitnesses.Max();

            m_ImprovementOfAverage = lastStatistic == null ? 0 : CalculateGrowth(m_AverageFitness, lastStatistic.AverageFitness, isMaximization);
            m_ImprovementOfBest = lastStatistic == null ? 0 : CalculateGrowth(m_BestFitness, lastStatistic.BestFitness, isMaximization);

            m_AverageGeneticDiversity = configuration.CalculateGeneticDiversity
                ? CalculateGeneticDiversity(individuals.Select(x => x.GetIndividual()).ToImmutableList(), geneticDiversityFunction)
                : 0;
        }

        private static double CalculateGrowth(double actualValue, double lastValue, bool positiveMeansGrowth)
        {
            double range = actualValue - lastValue;
            return positiveMeansGrowth ? range : -range;
        }

        private static double CalculateGeneticDiversity(
            [NotNull] IReadOnlyCollection<Individual<T>> individuals,
            [NotNull] Func<Individual<T>, Individual<T>, double> geneticDiversityFunction)
        {
            IEnumerable<double> distances = individuals.Select(individual1 =>
                    individuals.Select(individual2 => geneticDiversityFunction(individual1, individual2)).Average());
            return distances.Average();
        }

        /// <summary>
        /// Gets the best individual of the generation.
        /// </summary>
        [NotNull]
        public IndividualsWithFitness<T> BestIndividual => m_BestIndividual;

        /// <summary>
        /// Gets the average fitness of the generation.
        /// </summary>
        public double AverageFitness => m_AverageFitness;

        /// <summary>
        /// Gets the best fitness of the generation.
        /// </summary>
        public double BestFitness => m_BestFitness;

        /// <summary>
        /// Gets the worst fitness of the generation.
        /// </summary>
        public double WorstFitness => m_WorstFitness;

        /// <summary>
        /// Gets the absolute improvement of the average fitness compared to the previous generation.
        /// </summary>
        public double ImprovementOfAverage => m_ImprovementOfAverage;

        /// <summary>
        /// Gets the absolute improvement of the best fitness compared to the previous generation.
        /// </summary>
        public double ImprovementOfBest => m_ImprovementOfBest;

        /// <summary>
        /// Gets the average genetic diversity.
        /// <para>This value is 0 if the function to caluclate the distance hasn't been specified. </para>
        /// </summary>
        public double AverageGeneticDiversity => m_AverageGeneticDiversity;

        /// <summary>
        /// Determines whether the fitness has improved.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the fitness of the best individual has improved; otherwise, <c>false</c>.
        /// </returns>
        public bool HasFitnessImproved()
        {
            return m_ImprovementOfBest > 0;
        }
    }
}