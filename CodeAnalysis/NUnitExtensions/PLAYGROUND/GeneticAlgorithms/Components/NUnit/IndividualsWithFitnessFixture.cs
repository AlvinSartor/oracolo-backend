﻿using Fugro.NUnitExtensions;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Components.NUnit
{
    [TestFixture]
    internal sealed class IndividualsWithFitnessFixture : BaseContractsFixture<IndividualsWithFitness<char>>
    {
        /// <inheritdoc />
        protected override IndividualsWithFitness<char> AllocatePopulated()
        {
            var individual = new Individual<char>("foo".ToCharArray());
            return new IndividualsWithFitness<char>(individual, 15d);
        }

        /// <inheritdoc />
        protected override IndividualsWithFitness<char> AllocateDefault()
        {
            var individual = new Individual<char>("bar".ToCharArray());
            return new IndividualsWithFitness<char>(individual, 1d);
        }

        [Test]
        public void IndividualWithFitnessIsInstantiatedCorrectly()
        {
            var individual = new Individual<char>("foo".ToCharArray());
            var individualWithFitness = new IndividualsWithFitness<char>(individual, 15d);

            Assert.That(individualWithFitness.GetFitness(), Is.EqualTo(15));
            Assert.That(individualWithFitness.GetIndividual(), Is.EqualTo(individual));
            Assert.That(individualWithFitness.GetGenotype(), Is.EqualTo(individual.Genome));
        }

        [Test]
        public void OperatorsReturnExpectedResults()
        {
            var individual1 = new Individual<char>("foo".ToCharArray());
            var individualWithFitness1 = new IndividualsWithFitness<char>(individual1, 10000);

            var individual2 = new Individual<char>("bar".ToCharArray());
            var individualWithFitness2 = new IndividualsWithFitness<char>(individual2, -500);

            Assert.True(individualWithFitness1 > individualWithFitness2);
            Assert.False(individualWithFitness1 < individualWithFitness2);

            Assert.False(individualWithFitness2 > individualWithFitness1);
            Assert.True(individualWithFitness2 < individualWithFitness1);
        }
    }
}