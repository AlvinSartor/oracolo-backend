﻿using Fugro.Collections;
using Fugro.NUnitExtensions;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Components.NUnit
{
    [TestFixture]
    internal sealed class IndividualFixture : BaseContractsFixture<Individual<char>>
    {
        [Test]
        public void IndividualIsInstantiatedCorrectly()
        {
            var individual = new Individual<char>("foo".ToCharArray());

            ImmutableList<char> genome = individual.Genome;

            Assert.That(genome.Count, Is.EqualTo(3));

            Assert.That(genome[0], Is.EqualTo('f'));
            Assert.That(genome[1], Is.EqualTo('o'));
            Assert.That(genome[2], Is.EqualTo('o'));
        }

        [Test]
        public void IndividualsHaveDifferentGuids()
        {
            var individual1 = new Individual<char>("foo".ToCharArray());
            var individual2 = new Individual<char>("bar".ToCharArray());

            Assert.That(individual1.Id, Is.Not.EqualTo(individual2.Id));
        }

        /// <inheritdoc />
        protected override Individual<char> AllocatePopulated()
        {
            return new Individual<char>("Foo".ToCharArray());
        }

        /// <inheritdoc />
        protected override Individual<char> AllocateDefault()
        {
            return new Individual<char>("Bar".ToCharArray());
        }
    }
}
