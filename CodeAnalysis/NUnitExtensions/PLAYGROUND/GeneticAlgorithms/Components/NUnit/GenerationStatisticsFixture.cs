﻿using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.NUnitExtensions;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Components.NUnit
{
    [TestFixture]
    internal sealed class GenerationStatisticsFixturen : BaseContractsFixture<GenerationStatistics<char>>
    {
        [NotNull, ItemNotNull]
        private static ImmutableList<IndividualsWithFitness<char>> GetIndividuals(double multiplier = 1)
        {
            return new List<IndividualsWithFitness<char>>
            {
                new IndividualsWithFitness<char>(new Individual<char>("Foo".ToCharArray()), 1 * multiplier),
                new IndividualsWithFitness<char>(new Individual<char>("Bar".ToCharArray()), 2 * multiplier),
                new IndividualsWithFitness<char>(new Individual<char>("Baz".ToCharArray()), 3 * multiplier),
                new IndividualsWithFitness<char>(new Individual<char>("Ban".ToCharArray()), 4 * multiplier),
            }.ToImmutableList();
        }

        [Test]
        public void ValuesAreInstantiatedCorrectly()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            var statistics = new GenerationStatistics<char>(generation, Configuration.Default(), null, CalculateGeneticDiversity);

            Assert.That(statistics.AverageFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Average()));
            Assert.That(statistics.BestFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Max()));
            Assert.That(statistics.WorstFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Min()));

            Assert.That(statistics.ImprovementOfAverage, Is.EqualTo(0));
            Assert.That(statistics.ImprovementOfBest, Is.EqualTo(0));
            Assert.That(statistics.AverageGeneticDiversity, Is.EqualTo(0));
        }

        [Test]
        public void GrowthIsShownAfterTheSecondGeneration()
        {
            Configuration configuration = Configuration.Default();
            bool isMaximizationProblem = configuration.IsMaximizationProblem;

            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            var statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.ImprovementOfAverage, Is.EqualTo(0));
            Assert.That(statistics.ImprovementOfBest, Is.EqualTo(0));

            ImmutableList<IndividualsWithFitness<char>> generation2 = GetIndividuals(2);
            statistics = new GenerationStatistics<char>(generation2, configuration, statistics, CalculateGeneticDiversity);

            double expectedAverage = GetAverage(generation2) - GetAverage(generation);
            double expectedBest = GetBest(generation2, isMaximizationProblem) - GetBest(generation, isMaximizationProblem);

            Assert.That(statistics.ImprovementOfAverage, Is.EqualTo(expectedAverage));
            Assert.That(statistics.ImprovementOfBest, Is.EqualTo(expectedBest));

            ImmutableList<IndividualsWithFitness<char>> generation3 = GetIndividuals();
            statistics = new GenerationStatistics<char>(generation3, configuration, statistics, CalculateGeneticDiversity);

            expectedAverage = GetAverage(generation3) - GetAverage(generation2);
            expectedBest = GetBest(generation3, isMaximizationProblem) - GetBest(generation2, isMaximizationProblem);

            Assert.That(statistics.ImprovementOfAverage, Is.EqualTo(expectedAverage));
            Assert.That(statistics.ImprovementOfBest, Is.EqualTo(expectedBest));
        }

        [Test]
        public void BestFitnessCalculationIsInfluencedByIsMaximizationProblemProperty()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            Configuration configuration = Configuration.Default().WithIsMaximizationProblem(true);
            var statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.BestFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Max()));

            configuration = Configuration.Default().WithIsMaximizationProblem(false);
            statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.BestFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Min()));
        }

        [Test]
        public void WorseFitnessCalculationIsInfluencedByIsMaximizationProblemProperty()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            Configuration configuration = Configuration.Default().WithIsMaximizationProblem(true);
            var statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.WorstFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Min()));

            configuration = Configuration.Default().WithIsMaximizationProblem(false);
            statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.WorstFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Max()));
        }

        [Test]
        public void AverageFitnessCalculationIsNotInfluencedByIsMaximizationProblemProperty()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            Configuration configuration = Configuration.Default().WithIsMaximizationProblem(true);
            var statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            double averageFitness = statistics.AverageFitness;

            configuration = Configuration.Default().WithIsMaximizationProblem(false);
            statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            double averageFitness2 = statistics.AverageFitness;

            Assert.That(averageFitness, Is.EqualTo(averageFitness2));
            Assert.That(averageFitness, Is.EqualTo(generation.Select(x => x.GetFitness()).Average()));
        }

        private static double GetAverage<T>([NotNull] IEnumerable<IndividualsWithFitness<T>> individuals)
        {
            return individuals.Select(x => x.GetFitness()).Average();
        }

        private static double GetBest<T>([NotNull] IEnumerable<IndividualsWithFitness<T>> individuals, bool isMaximizationProblem)
        {
            return isMaximizationProblem ? individuals.Select(x => x.GetFitness()).Max() : individuals.Select(x => x.GetFitness()).Min();
        }

        [Test]
        public void GeneticDiversityIsCalculatedOnlyIfRequired()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            Configuration configuration = Configuration.Default()
                .WithCalculateGeneticDiversity(false);

            var statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.AverageGeneticDiversity, Is.EqualTo(0));

            configuration = Configuration.Default()
                .WithCalculateGeneticDiversity(true);

            statistics = new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);

            Assert.That(statistics.AverageGeneticDiversity, Is.EqualTo(42));
        }

        private static double CalculateGeneticDiversity([NotNull] Individual<char> individual1, [NotNull] Individual<char> individual2) => 42;

        /// <inheritdoc />
        protected override GenerationStatistics<char> AllocatePopulated()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals();
            Configuration configuration = Configuration.Default();

            return new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);
        }

        /// <inheritdoc />
        protected override GenerationStatistics<char> AllocateDefault()
        {
            ImmutableList<IndividualsWithFitness<char>> generation = GetIndividuals(2);
            Configuration configuration = Configuration.Default();

            return new GenerationStatistics<char>(generation, configuration, null, CalculateGeneticDiversity);
        }
    }
}