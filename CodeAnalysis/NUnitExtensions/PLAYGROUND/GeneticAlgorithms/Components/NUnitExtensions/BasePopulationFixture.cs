﻿using System;
using System.Linq;
using Fugro.Collections;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Components.NUnitExtensions
{
    [TestFixture]
    public abstract class BasePopulationFixture<T>
    {
        [NotNull]
        protected abstract Population<T> PopulationImplementation([NotNull] Configuration configuration);

        [Test]
        public void IfTheConfigurationIsSpecifiedItIsApplied()
        {
            Configuration expectedConfiguration = Configuration.Default();
            Population<T> population = PopulationImplementation(expectedConfiguration);

            Assert.That(population.Configuration, Is.EqualTo(expectedConfiguration));
        }

        [Test]
        public void DefaultBestFitnessIsMinDoubleIfItIsMaximizationProblem()
        {
            Configuration configuration = Configuration.Default().WithIsMaximizationProblem(true);

            Population<T> population = PopulationImplementation(configuration);

            Assert.That(population.BestFitness, Is.EqualTo(double.MinValue));
        }

        [Test]
        public void DefaultBestFitnessIsMaxDoubleIfItIsMinimizationProblem()
        {
            Configuration configuration = Configuration.Default().WithIsMaximizationProblem(false);

            Population<T> population = PopulationImplementation(configuration);

            Assert.That(population.BestFitness, Is.EqualTo(double.MaxValue));
        }

        [Test]
        public void PopulationThrowsAnErrorIfTriesToGetTheBestIndividualBeforeOfEvolution()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                ImmutableList<T> individual = PopulationImplementation(Configuration.Default()).BestIndividual;
            });
        }

        [Test]
        public void ExceptionIsThrownIfTriesToUpdateWithoutGettingTheIndividualsFirst()
        {
            Configuration configuration = Configuration.Default();

            Population<T> population = PopulationImplementation(configuration);
            double[] fitnesses = Enumerable.Repeat(0d, configuration.PopulationSize).ToArray();
            Assert.Throws<InvalidOperationException>(() => population.UpdateWithResults(fitnesses));
        }

        [Test]
        public void GenerationCounterGrowsEachGenerations()
        {
            Configuration configuration = Configuration.Default()
                .WithIndividualsToReplace(1d)
                .WithMaximumGenerations(100);

            Population<T> population = PopulationImplementation(configuration);
            var counter = 1;

            while (counter < 10)
            {
                Assert.That(population.GenerationNumber, Is.EqualTo(counter));

                population.GetIndividuals();
                double[] fitnesses = Enumerable.Repeat(0d, configuration.PopulationSize).ToArray();
                population.UpdateWithResults(fitnesses);

                counter++;
            }
        }

        [Test]
        public void TheEvolutionEndsWhenReachesMaximumNumberOfGenerations()
        {
            Configuration configuration = Configuration.Default()
                .WithMaximumGenerations(100);

            Population<T> population = PopulationImplementation(configuration);

            while (!population.HasFinished)
            {
                population.GetIndividuals();
                double[] fitnesses = Enumerable.Repeat(0d, configuration.PopulationSize).ToArray();
                population.UpdateWithResults(fitnesses);
            }

            Assert.That(population.GenerationNumber, Is.EqualTo(configuration.MaximumGenerationToStopEvolutionaryProcess));
        }

        [Test]
        public void TheEvolutionEndsWhenReachesMaximumNumberOfGenerationsWithoutImprovements()
        {
            Configuration configuration = Configuration.Default()
                .WithMaximumGenerationsWithoutImprovements(100);

            Population<T> population = PopulationImplementation(configuration);

            while (!population.HasFinished)
            {
                population.GetIndividuals();
                double[] fitnesses = Enumerable.Repeat(0d, configuration.PopulationSize).ToArray();
                population.UpdateWithResults(fitnesses);
            }

            Assert.That(population.GenerationNumber - 1, Is.EqualTo(configuration.MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess));
        }

        [Test]
        public void TheEvolutionEndsWhenReachesMinimumFitnessInMaximizationProblem()
        {
            Configuration configuration = Configuration.Default()
                .WithIsMaximizationProblem(true)
                .WithMinimumFitnessToStopProcess(0.985)
                .WithMaximumGenerations(1000);

            Population<T> population = PopulationImplementation(configuration);

            while (!population.HasFinished)
            {
                population.GetIndividuals();
                double fitness = population.GenerationNumber * 0.1d;
                double[] fitnesses = Enumerable.Repeat(fitness, configuration.PopulationSize).ToArray();
                population.UpdateWithResults(fitnesses);
            }

            Assert.That(population.BestFitness, Is.GreaterThanOrEqualTo(configuration.MinimumFitnessToStopEvolutionaryProcess));
        }

        [Test]
        public void TheEvolutionEndsWhenReachesMinimumFitnessInMinimizationProblem()
        {
            Configuration configuration = Configuration.Default()
                .WithIsMaximizationProblem(false)
                .WithMaximumGenerations(1000)
                .WithMinimumFitnessToStopProcess(-0.985);

            Population<T> population = PopulationImplementation(configuration);

            while (!population.HasFinished)
            {
                population.GetIndividuals();
                double fitness = population.GenerationNumber * -0.1d;
                double[] fitnesses = Enumerable.Repeat(fitness, configuration.PopulationSize).ToArray();
                population.UpdateWithResults(fitnesses);
            }

            Assert.That(population.BestFitness, Is.LessThanOrEqualTo(configuration.MinimumFitnessToStopEvolutionaryProcess));
        }
    }
}