﻿using Fugro.Collections;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components
{
    /// <summary>
    /// Wrapper class that contains an individual and the relative fitness.
    /// This class also implements functions that simplify the comparison between individuals.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Fugro.IImmutable" />
    public sealed class IndividualsWithFitness<T> : IImmutable
    {
        [NotNull] private readonly Individual<T> m_Individual;
        private readonly double m_Fitness;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndividualsWithFitness{T}"/> class.
        /// </summary>
        /// <param name="individual">The individual.</param>
        /// <param name="fitness">The fitness.</param>
        public IndividualsWithFitness([NotNull] Individual<T> individual, double fitness)
        {
            ArgumentHelper.AssertNotNull(individual, nameof(individual));

            m_Individual = individual.NotNull(nameof(individual));
            m_Fitness = fitness;
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="individual1">The first individual.</param>
        /// <param name="individual2">The second individual.</param>
        /// <returns>
        /// true if the fitness of the Individual1 is greater than the fitness of the individual2.
        /// </returns>
        public static bool operator >([NotNull] IndividualsWithFitness<T> individual1, [NotNull] IndividualsWithFitness<T> individual2)
        {
            ArgumentHelper.AssertNotNull(individual1, nameof(individual1));
            ArgumentHelper.AssertNotNull(individual2, nameof(individual2));

            return individual1.m_Fitness > individual2.m_Fitness;
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="individual1">The first individual.</param>
        /// <param name="individual2">The second individual.</param>
        /// <returns>
        /// true if the fitness of the Individual1 is lower than the fitness of the individual2.
        /// </returns>
        public static bool operator <([NotNull] IndividualsWithFitness<T> individual1, [NotNull] IndividualsWithFitness<T> individual2)
        {
            ArgumentHelper.AssertNotNull(individual1, nameof(individual1));
            ArgumentHelper.AssertNotNull(individual2, nameof(individual2));

            return individual1.m_Fitness < individual2.m_Fitness;
        }

        /// <summary>
        /// The genotype of the individual.
        /// </summary>
        [NotNull]
        public ImmutableList<T> GetGenotype()
        {
            return GetIndividual().Genome;
        }

        /// <summary>
        /// The genotype of the individual.
        /// </summary>
        [NotNull]
        public Individual<T> GetIndividual()
        {
            return m_Individual;
        }

        /// <summary>
        /// Gets the fitness of the individual.
        /// </summary>
        public double GetFitness()
        {
            return m_Fitness;
        }
    }
}
