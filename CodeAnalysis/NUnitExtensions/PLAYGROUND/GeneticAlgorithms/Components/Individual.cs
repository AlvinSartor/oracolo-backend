﻿using System;
using System.Collections.Generic;
using Fugro.Collections;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components
{
    public class Individual<T> : IImmutable
    {
        public readonly Guid Id;

        [NotNull]
        public ImmutableList<T> Genome { get; }

        public Individual([NotNull] IEnumerable<T> genome)
        {
            ArgumentHelper.AssertNotNull(genome, nameof(genome));
            Genome = genome.ToImmutableList();
            Id = Guid.NewGuid();
        }
    }
}
