﻿using System.Collections.Generic;
using Fugro.GeneticAlgorithms.Components.NUnitExtensions;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components.Implementations.NUnit
{
    internal sealed class NumericalPopulationFixture : BasePopulationFixture<double>
    {
        [NotNull, ItemNotNull]
        private static IList<IList<double>> GetParameters()
        {
            return new List<IList<double>>(new[]
            {
                new List<double>(new[] { 0.0, 1.0 }),
                new List<double>(new[] { 1.0, 2.0 }),
                new List<double>(new[] { -10.0, 25.0 }),
                new List<double>(new[] { int.MinValue, 0.0 }),
                new List<double>(new[] { 0.0, int.MaxValue }),
                new List<double>(new[] { int.MaxValue, (double)int.MaxValue }),
                new List<double>(new[] { 1.0, -11.0 }),
            });
        }

        /// <inheritdoc />
        protected override Population<double> PopulationImplementation(Configuration configuration)
        {
            return new NumericalPopulation(GetParameters(), configuration);
        }
    }
}
