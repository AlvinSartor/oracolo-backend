﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components.Implementations
{
    /// <summary>
    /// A numerical population is a collection of individuals with numerical genotypes.
    /// <para>This class is an implementation of the abstract population that work with numerical genotypes. </para>
    /// </summary>
    public sealed class NumericalPopulation : Population<double>
    {
        private readonly ImmutableList<ImmutableList<double>> m_Parameters;
        private readonly ImmutableList<double> m_MinParameters;
        private readonly ImmutableList<double> m_MaxParameters;

        private double[] m_LastFitnesses;

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericalPopulation"/> class.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="configuration">The configuration.</param>
        public NumericalPopulation([NotNull] IList<IList<double>> parameters, [CanBeNull] Configuration configuration = null)
            : base(configuration)
        {
            ArgumentHelper.AssertNotNull(parameters, nameof(parameters));

            m_Parameters = parameters.Select(x => x.ToImmutableList()).ToImmutableList();
            m_MinParameters = parameters.Select(x => x.Min()).ToImmutableList();
            m_MaxParameters = parameters.Select(x => x.Max()).ToImmutableList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericalPopulation"/> class.
        /// </summary>
        /// <param name="nrGenes">The nr genes.</param>
        /// <param name="minBoundary">The minimum boundary.</param>
        /// <param name="maxBoundary">The maximum boundary.</param>
        /// <param name="configuration">The configuration.</param>
        public NumericalPopulation(int nrGenes, double minBoundary, double maxBoundary, [CanBeNull] Configuration configuration = null)
            : base(configuration)
        {
            m_Parameters = Enumerable.Repeat(new ImmutableList<double>(new[] { minBoundary, maxBoundary }), nrGenes).ToImmutableList();
            m_MinParameters = Enumerable.Repeat(minBoundary, nrGenes).ToImmutableList();
            m_MaxParameters = Enumerable.Repeat(maxBoundary, nrGenes).ToImmutableList();
        }

        /// <inheritdoc />
        public override void UpdateWithResults(double[] fitnesses)
        {
            m_LastFitnesses = fitnesses.NotNull(nameof(fitnesses));
            base.UpdateWithResults(fitnesses);
        }

        /// <inheritdoc />
        protected override ImmutableList<Individual<double>> Initialization()
        {
            return Operators.Initialization.RealInitialization(Configuration.PopulationSize, m_Parameters);
        }

        /// <inheritdoc />
        protected override ImmutableList<double> Crossover(ImmutableList<double> mostFitParent, ImmutableList<double> lessFitParent)
        {
            return Operators.Crossover.BLXAlphaBetaCrossover(mostFitParent, lessFitParent, Configuration.AlphaCrossoverValue, Configuration.BetaCrossoverValue);
        }

        /// <inheritdoc />
        protected override ImmutableList<double> Mutation(ImmutableList<double> genome)
        {
            double alphaValue = Configuration.AlphaMutationValue;
            double mutationProbability = Configuration.MutationProbability;

            return Operators.Mutation.BLXAlphaMultipointMutation(genome, m_MinParameters, m_MaxParameters, alphaValue, mutationProbability);
        }

        /// <inheritdoc />
        protected override Queue<IndividualsWithFitness<double>> Selection(
            ImmutableList<IndividualsWithFitness<double>> individuals,
            double[] fitnesses)
        {
            var numberOfIndividuals = (int)(Configuration.IndividualsToReplace * Configuration.PopulationSize * 2);
            bool isMaximizationProblem = Configuration.IsMaximizationProblem;

            return Operators.Selection.TournamentSelection(individuals, fitnesses, isMaximizationProblem, numberOfIndividuals, Configuration.TournamentSize);
        }

        /// <inheritdoc />
        protected override ImmutableList<Individual<double>> Replacement(
            ImmutableList<Individual<double>> oldPopulation,
            ImmutableList<Individual<double>> newBorns)
        {
            return Operators.Replacement.LessFitReplacement(oldPopulation, m_LastFitnesses, newBorns);
        }

        /// <summary>
        /// Calculates the genetic diversity between two individuals.
        /// <para> This version of CalculateGeneticDiversity simply returns the hamming distance between all genes. </para>
        /// </summary>
        /// <param name="individual1">The first indivdual.</param>
        /// <param name="individual2">The second individual.</param>
        /// <returns>A double value that represent the distance between two genomes. </returns>
        protected override double CalculateGeneticDiversity(Individual<double> individual1, Individual<double> individual2)
        {
            return individual1.Genome.Select((gene, i) => Math.Abs(gene - individual2.Genome[i])).Sum();
        }
    }
}