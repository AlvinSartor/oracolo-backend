﻿using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Components
{
    /// <summary>
    /// A configuration class contains all parameters used by the genetic algorithm manager to perform the evolution.
    /// </summary>
    public class Configuration : IImmutable
    {
        // - - CROSSOVER - -

        /// <summary>
        /// The Alpha value used when performing a BLX-α crossover.
        /// </summary>
        public readonly double AlphaCrossoverValue;

        /// <summary>
        /// The Beta value used when performing a BLX-αβ crossover.
        /// </summary>
        public readonly double BetaCrossoverValue;

        // - - MUTATION - -

        /// <summary>
        /// The Alpha value used when performing a BLX-α mutation.
        /// </summary>
        public readonly double AlphaMutationValue;

        /// <summary>
        /// The probability of performing a mutation on the genome.
        /// </summary>
        public readonly double MutationProbability;

        // - - SELECTION - -

        /// <summary>
        /// The size of the torunament when using the Tournament Selection algorithm.
        /// </summary>
        public readonly int TournamentSize;

        /// <summary>
        /// The pressure exerted on the individuals when using the Ranking Selection algorithm.
        /// </summary>
        public readonly int GeneticPressureForRankingSelection;

        /// <summary>
        /// Specifies if the Selection algorithms have to consider the highest fitness as the best or the lowest (when false).
        /// </summary>
        public readonly bool IsMaximizationProblem;

        // - - REPLACEMENT - -

        /// <summary>
        /// The percentage of individuals that have to be replaced.
        /// </summary>
        public readonly double IndividualsToReplace;

        /// <summary>
        /// The size of the population.
        /// </summary>
        public readonly int PopulationSize;

        /// <summary>
        /// The size of the population.
        /// </summary>
        public readonly int PopulationArchiveSize;

        // - - TERMINATION CONDITIONS - -

        /// <summary>
        /// Defines the minimum fitness an individual has to reach to stop the evolutionary process.
        /// </summary>
        public readonly double MinimumFitnessToStopEvolutionaryProcess;

        /// <summary>
        /// Defines the maximum number of generations without improvements on the fitness before of stopping the evolutionary process.
        /// </summary>
        public readonly int MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess;

        /// <summary>
        /// Defines the maximum number of generations before of stopping the evolutionary process.
        /// </summary>
        public readonly int MaximumGenerationToStopEvolutionaryProcess;

        // - - STATISTICS - -

        /// <summary>
        /// Defines the maximum number of generations before of stopping the evolutionary process.
        /// </summary>
        public readonly bool CalculateGeneticDiversity;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        /// <param name="alphaCrossoverValue">The alpha crossover value.</param>
        /// <param name="betaCrossoverValue">The beta crossover value.</param>
        /// <param name="alphaMutationValue">The alpha mutation value.</param>
        /// <param name="mutationProbability">The mutation probability.</param>
        /// <param name="tournamentSize">Size of the tournament.</param>
        /// <param name="geneticPressureForRankingSelection">The genetic pressure for ranking selection.</param>
        /// <param name="isMaximizationProblem"><c>true</c> if it is a maximization problem.</param>
        /// <param name="individualsToReplace">The percentage of individuals to replace.</param>
        /// <param name="populationSize">Size of the population.</param>
        /// <param name="populationArchiveSize">Number of individuals that will be kept in the population archive.</param>
        /// <param name="minimumFitnessToStopEvolutionaryProcess">The minimum fitness an individual has to reach to stop the evolutionary process.</param>
        /// <param name="maximumGenerationWithoutImprovementsToStopEvolutionaryProcess">The maximum number of generations without improvements on the fitness before of stopping the evolutionary process.</param>
        /// <param name="maximumGenerationToStopEvolutionaryProcess">The maximum number of generations before of stopping the evolutionary process.</param>
        /// <param name="calculateGeneticDiversity">Specifies if the algorithm needs to calculate the average genetic diversity for each individuals in every generation.</param>
        public Configuration(double alphaCrossoverValue,
            double betaCrossoverValue,
            double alphaMutationValue,
            double mutationProbability,
            int tournamentSize,
            int geneticPressureForRankingSelection,
            bool isMaximizationProblem,
            double individualsToReplace,
            int populationSize,
            int populationArchiveSize,
            double minimumFitnessToStopEvolutionaryProcess,
            int maximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
            int maximumGenerationToStopEvolutionaryProcess,
            bool calculateGeneticDiversity)
        {
            AlphaCrossoverValue = alphaCrossoverValue;
            BetaCrossoverValue = betaCrossoverValue;
            AlphaMutationValue = alphaMutationValue;
            MutationProbability = mutationProbability;
            TournamentSize = tournamentSize;
            GeneticPressureForRankingSelection = geneticPressureForRankingSelection;
            IsMaximizationProblem = isMaximizationProblem;
            IndividualsToReplace = individualsToReplace;
            PopulationSize = populationSize;
            PopulationArchiveSize = populationArchiveSize;
            MinimumFitnessToStopEvolutionaryProcess = minimumFitnessToStopEvolutionaryProcess;
            MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess = maximumGenerationWithoutImprovementsToStopEvolutionaryProcess;
            MaximumGenerationToStopEvolutionaryProcess = maximumGenerationToStopEvolutionaryProcess;
            CalculateGeneticDiversity = calculateGeneticDiversity;
        }

        /// <summary>
        /// Gets the default configuration.
        /// </summary>
        [NotNull]
        public static Configuration Default()
        {
            const double DefaultAlphaCrossover = 0.15;
            const double DefaultBetaCrossover = 0.08;

            const double DefaultAlphaMutation = 0.15;
            const double DefaultMutationProbability = 0.03;

            const int DefaultTournamentSize = 5;
            const int DefaultGeneticPressureForRanking = 2;
            const bool DefaultIsMaximizationProblem = true;

            const int DefaultPopulationSize = 100;
            const int DefaultPopulationArchiveSize = 100;
            const double DefaultIndividualsToReplace = 0.9;

            const double DefaultMinimumFitnessRequired = 0.97;
            const int DefaultMaximumNumberOfGenerationsWithoutImprovements = int.MaxValue;
            const int DefaultMaximumNumberOfGenerations = int.MaxValue;

            const bool DefaultCalculateGeneticDiversity = false;

            return new Configuration(
                DefaultAlphaCrossover,
                DefaultBetaCrossover,
                DefaultAlphaMutation,
                DefaultMutationProbability,
                DefaultTournamentSize,
                DefaultGeneticPressureForRanking,
                DefaultIsMaximizationProblem,
                DefaultIndividualsToReplace,
                DefaultPopulationSize,
                DefaultPopulationArchiveSize,
                DefaultMinimumFitnessRequired,
                DefaultMaximumNumberOfGenerationsWithoutImprovements,
                DefaultMaximumNumberOfGenerations,
                DefaultCalculateGeneticDiversity);
        }

        #endregion

        #region WithMethods

        [NotNull, Pure]
        public Configuration WithAlphaCrossover(double value)
        {
            return new Configuration(
                value,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize, PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithBetaCrossover(double value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                value,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithAlphaMutation(double value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                value,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithMutationProbability(double value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                value,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithTournamentSize(int value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                value,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithGeneticPressureForRanking(int value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                value,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithIsMaximizationProblem(bool value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                value,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithIndividualsToReplace(double value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                value,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithPopulationSize(int value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                value,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithPopulationArchiveSize(int value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                value,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithMinimumFitnessToStopProcess(double value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                value,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithMaximumGenerationsWithoutImprovements(int value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                value,
                MaximumGenerationToStopEvolutionaryProcess,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithMaximumGenerations(int value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                value,
                CalculateGeneticDiversity);
        }

        [NotNull, Pure]
        public Configuration WithCalculateGeneticDiversity(bool value)
        {
            return new Configuration(
                AlphaCrossoverValue,
                BetaCrossoverValue,
                AlphaMutationValue,
                MutationProbability,
                TournamentSize,
                GeneticPressureForRankingSelection,
                IsMaximizationProblem,
                IndividualsToReplace,
                PopulationSize,
                PopulationArchiveSize,
                MinimumFitnessToStopEvolutionaryProcess,
                MaximumGenerationWithoutImprovementsToStopEvolutionaryProcess,
                MaximumGenerationToStopEvolutionaryProcess,
                value);
        }

        #endregion
    }
}