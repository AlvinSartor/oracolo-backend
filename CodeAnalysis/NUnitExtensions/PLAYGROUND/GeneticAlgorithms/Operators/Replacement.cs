﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Util;
using JetBrains.Annotations;
using Random = Fugro.GeneticAlgorithms.Util.Random;

namespace Fugro.GeneticAlgorithms.Operators
{
    /// <summary>
    /// The replacement algorithms are used to replace the individuals of the current generation with the ones of the previous one.
    ///
    /// <para>These are standard replacement algorithms.
    ///
    /// Even though it is adviced to use these standard algorithms,
    /// the user can always write a personalised algorithm that fits better with the problem.
    /// </para>
    /// </summary>
    public static class Replacement
    {
        /// <summary>
        /// The new population replaces completely the old one.
        /// </summary>
        /// <typeparam name="T">The type of the individual</typeparam>
        /// <param name="oldPopulation">The old population.</param>
        /// <param name="newBorns">The new borns.</param>
        /// <remarks>
        /// <para> Axiomatic complexity: O(n) </para>
        /// <para> The size of the new population has to be the same as the old one, otherwise an exception will be thrown. </para>
        /// </remarks>
        /// <returns>The new population.</returns>
        /// <exception cref="InvalidOperationException"></exception>
        [NotNull]
        public static ImmutableList<T> GenerationalReplacement<T>([NotNull] ImmutableList<T> oldPopulation, [NotNull] ImmutableList<T> newBorns)
        {
            ArgumentHelper.AssertNotNull(oldPopulation, nameof(oldPopulation));
            ArgumentHelper.AssertNotNull(newBorns, nameof(newBorns));

            if (newBorns.Count != oldPopulation.Count)
            {
                var errorMessage = "The size of the new population doesn't match the old one!";
                errorMessage += "\nOld population size: " + oldPopulation.Count;
                errorMessage += "\nNew population size: " + newBorns.Count;

                throw new InvalidOperationException(errorMessage);
            }

            return new ImmutableList<T>(newBorns);
        }

        /// <summary>
        /// The new individuals replace random individuals of the last population.
        /// </summary>
        /// <typeparam name="T">The type of the individual</typeparam>
        /// <param name="oldPopulation">The old population.</param>
        /// <param name="newBorns">The new borns.</param>
        /// <remarks>
        /// <para> Axiomatic complexity: O(n) </para>
        /// <para> The size of the new population has to be less or equal than the previous one. </para>
        /// </remarks>
        /// <returns>The new population.</returns>
        /// <exception cref="InvalidOperationException"></exception>
        [NotNull]
        public static ImmutableList<T> RandomReplacement<T>([NotNull] ImmutableList<T> oldPopulation, [NotNull] ImmutableList<T> newBorns)
        {
            ArgumentHelper.AssertNotNull(oldPopulation, nameof(oldPopulation));
            ArgumentHelper.AssertNotNull(newBorns, nameof(newBorns));

            ThrowIfTheNewPopulationIsMoreThanThePastOne(oldPopulation, newBorns);

            if (oldPopulation.Count == newBorns.Count)
            {
                return GenerationalReplacement(oldPopulation, newBorns);
            }

            List<T> newPopulation = newBorns.ToList();
            List<T> oldPopulationCopy = oldPopulation.ToList();
            int populationSize = oldPopulation.Count;

            while (newPopulation.Count < populationSize)
            {
                int randomIndex = Random.GetRandomInRange(0, oldPopulationCopy.Count);
                T individual = oldPopulationCopy[randomIndex];
                oldPopulationCopy.RemoveAt(randomIndex);
                newPopulation.Add(individual);
            }

            return newPopulation.ToImmutableList();
        }

        /// <summary>
        /// The new individuals replace the most similar individuals of the last population.
        /// This replacement algorithm is meant to keep the genetic diversity as high as possible.
        /// </summary>
        /// <typeparam name="T">The type of the individual</typeparam>
        /// <param name="oldPopulation">The old population.</param>
        /// <param name="newBorns">The new borns.</param>
        /// <param name="calculateDistanceFunc">Function used to define how much two individuals are alike.</param>
        /// <remarks>
        /// <para> Axiomatic complexity: O(n*n) </para>
        /// <para> The size of the new population has to be less or equal than the previous one. </para>
        /// </remarks>
        /// <returns>The new population.</returns>
        /// <exception cref="InvalidOperationException"></exception>
        [NotNull]
        public static ImmutableList<T> MostSimilarReplacement<T>(
            [NotNull] ImmutableList<T> oldPopulation,
            [NotNull] ImmutableList<T> newBorns,
            [NotNull] Func<T, T, double> calculateDistanceFunc)
        {
            ArgumentHelper.AssertNotNull(oldPopulation, nameof(oldPopulation));
            ArgumentHelper.AssertNotNull(newBorns, nameof(newBorns));
            ArgumentHelper.AssertNotNull(calculateDistanceFunc, nameof(calculateDistanceFunc));

            ThrowIfTheNewPopulationIsMoreThanThePastOne(oldPopulation, newBorns);

            if (oldPopulation.Count == newBorns.Count)
            {
                return GenerationalReplacement(oldPopulation, newBorns);
            }

            List<T> newPopulation = newBorns.ToList();
            List<T> oldPopulationCopy = oldPopulation.ToList();

            foreach (var individual in newPopulation)
            {
                T mostSimilar = oldPopulationCopy.MinBy(individual2 => calculateDistanceFunc(individual, individual2));
                oldPopulationCopy.Remove(mostSimilar);
            }

            return newPopulation.WithAddition(oldPopulationCopy);
        }

        /// <summary>
        /// The less fit indivduals of the last generation are discarded in favour of the new generation.
        /// </summary>
        /// <typeparam name="T">The type of the individual</typeparam>
        /// <param name="oldPopulation">The old population.</param>
        /// <param name="fitnesses">The fitnesses of the old population</param>
        /// <param name="newBorns">The new borns.</param>
        /// <remarks>
        /// <para> Axiomatic complexity: O(n*Log(n)) </para>
        /// <para> The size of the new population has to be less or equal than the previous one. </para>
        /// </remarks>
        /// <returns>The new population.</returns>
        /// <exception cref="InvalidOperationException"></exception>
        [NotNull]
        public static ImmutableList<T> LessFitReplacement<T>(
            [NotNull] ImmutableList<T> oldPopulation,
            [NotNull] double[] fitnesses,
            [NotNull] ImmutableList<T> newBorns)
        {
            ArgumentHelper.AssertNotNull(oldPopulation, nameof(oldPopulation));
            ArgumentHelper.AssertNotNull(fitnesses, nameof(fitnesses));
            ArgumentHelper.AssertNotNull(newBorns, nameof(newBorns));

            ThrowIfTheNewPopulationIsMoreThanThePastOne(oldPopulation, newBorns);
            ThrowIfTheFitnessesDontMatchPopulation(oldPopulation, fitnesses);

            if (oldPopulation.Count == newBorns.Count)
            {
                return GenerationalReplacement(oldPopulation, newBorns);
            }

            var newPopulation = new List<T>(newBorns);
            int populationSize = oldPopulation.Count;

            T[] sortedPopulation = oldPopulation.ToArray();
            Array.Sort(fitnesses, sortedPopulation);

            IEnumerable<T> bestElements = sortedPopulation.Reverse().Take(populationSize - newPopulation.Count);

            newPopulation.AddRange(bestElements);

            return newPopulation.ToImmutableList();
        }


        private static void ThrowIfTheNewPopulationIsMoreThanThePastOne<T>(
            [NotNull] ICollection<T> oldPopulation,
            [NotNull] ICollection<T> newBorns)
        {
            if (newBorns.Count > oldPopulation.Count)
            {
                var errorMessage = "The size of the new population is greater than the old one!";
                errorMessage += "\nOld population size: " + oldPopulation.Count;
                errorMessage += "\nNew population size: " + newBorns.Count;

                throw new InvalidOperationException(errorMessage);
            }
        }

        private static void ThrowIfTheFitnessesDontMatchPopulation<T>(
            [NotNull] ICollection<T> oldPopulation,
            [NotNull] ICollection<double> fitnesses)
        {
            if (oldPopulation.Count != fitnesses.Count)
            {
                var errorMessage = "The size of the fitnesses array and the population don't match!";
                errorMessage += "\nOld population size: " + oldPopulation.Count;
                errorMessage += "\nFitnesses array size: " + fitnesses.Count;

                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
