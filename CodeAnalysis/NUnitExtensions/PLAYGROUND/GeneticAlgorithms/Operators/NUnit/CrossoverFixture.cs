﻿using System.Linq;
using Fugro.Collections;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Operators.NUnit
{
    [TestFixture]
    internal sealed class CrossoverFixture
    {
        [Test]
        public void BLXAlphaCrossoverReturnsGenomesInExpectedRange()
        {
            ImmutableList<double> parent1 = Enumerable.Repeat(2d, 10).ToImmutableList();
            ImmutableList<double> parent2 = Enumerable.Repeat(1d, 10).ToImmutableList();

            const double alpha = 0.3;
            const double min = 1 - alpha;
            const double max = 2 + alpha;

            for (var i = 0; i < 1000; i++)
            {
                ImmutableList<double> son = Crossover.BLXAlphaCrossover(parent1, parent2, alpha);
                foreach (var gene in son)
                {
                    Assert.That(gene, Is.InRange(min, max));
                }
            }
        }

    }
}
