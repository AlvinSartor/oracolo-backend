﻿using System.Collections.Generic;
using System.Linq;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Components;
using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Operators.NUnit
{
    [TestFixture]
    internal sealed class InitializationFixture
    {
        [Test]
        public void BinaryInitializationCreatesAValidPopulation()
        {
            const int populationSize = 25;
            const int genomeSize = 10;

            IList<Individual<bool>> population = Initialization.BinaryInitialization(genomeSize, populationSize);

            Assert.That(population.Count == populationSize);
            Assert.That(population.All(x => x.Genome.Count == genomeSize));
        }

        [Test]
        public void PositionalInitializationCreatesAValidPopulation()
        {
            const int populationSize = 25;
            var values = new List<string>(new[] { "foo", "bar", "baz", "bap", "foobar", "barbar", "bazbar", "bapbar" });

            IList<Individual<string>> population = Initialization.PositionalInitialization(populationSize, values);

            Assert.That(population.Count == populationSize);
            Assert.That(population.All(x => x.Genome.Count == values.Count));
            Assert.That(values.All(value => population.All(x => x.Genome.Contains(value))), "Some values are missing");
        }

        [Test]
        public void NumericalInitializationWithParametersCreatesAValidPopulation()
        {
            const int populationSize = 25;

            var parameter1 = new ImmutableList<double>(new[] { 0d });
            var parameter2 = new ImmutableList<double>(new[] { 1d, 2, 3 });
            var parameter3 = new ImmutableList<double>(new[] { -1d, -2, -3 });
            var parameter4 = new ImmutableList<double>(new[] { 0.01, 0.03 });
            var parameter5 = new ImmutableList<double>(new double[] { float.MinValue, float.MaxValue });
            var parameters = new ImmutableList<ImmutableList<double>>(new[] { parameter1, parameter2, parameter3, parameter4, parameter5 });

            IList<Individual<double>> population = Initialization.RealInitialization(populationSize, parameters);

            Assert.That(population.Count == populationSize);
            Assert.That(population.All(x => x.Genome.Count == parameters.Count));
            Assert.That(population.All(x => x.Genome.Select((gene, i) => gene >= parameters[i].Min() && gene <= parameters[i].Max()).All(y => y)));
        }

        [Test]
        public void NumericalInitializationWithBoundsCreatesAValidPopulation()
        {
            const int genomeSize = 10;
            const int populationSize = 25;
            const double upperBound = 128.256;
            const double lowerBound = -512.1024;

            IList<Individual<double>> population = Initialization.RealInitialization(genomeSize, populationSize, lowerBound, upperBound);

            Assert.That(population.Count == populationSize);
            Assert.That(population.All(x => x.Genome.Count == genomeSize));
            Assert.That(population.All(x => x.Genome.Select((gene, i) => gene >= lowerBound && gene <= upperBound).All(y => y)));
        }

    }
}
