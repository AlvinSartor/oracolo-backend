﻿using Fugro.Collections;
using Fugro.GeneticAlgorithms.Util;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Operators
{
    /// <summary>
    /// The mutation algorithms are used to mutate a genome, allowing the EXPLORATION of the search space.
    ///
    /// <para>These are standard mutation algorithms.
    ///
    /// As these algorithms are only for certain types,
    /// the user may write a personalised algorithm that fits better with the problem.
    /// </para>
    /// </summary>
    public static class Mutation
    {
        /// <summary>
        /// Return a new genotype in which a BLX-α mutation has been performed on multiple genes (with a certain probability).
        /// </summary>
        /// <param name="genotype">The genotype we want to mutate.</param>
        /// <param name="minRanges">An array containing the minimum ranges.</param>
        /// <param name="maxRanges">An array containing the maximum ranges.</param>
        /// <param name="alpha">The α value.</param>
        /// <param name="mutationProbability">The probability that a mutation occurs to every single gene</param>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull]
        public static ImmutableList<double> BLXAlphaMultipointMutation(
            [NotNull] ImmutableList<double> genotype,
            [NotNull] ImmutableList<double> minRanges,
            [NotNull] ImmutableList<double> maxRanges,
            double alpha,
            double mutationProbability)
        {
            ArgumentHelper.AssertNotNull(genotype, nameof(genotype));
            ArgumentHelper.AssertNotNull(minRanges, nameof(minRanges));
            ArgumentHelper.AssertNotNull(maxRanges, nameof(maxRanges));

            var newGenotype = new double[genotype.Count];
            for (var i = 0; i < genotype.Count; i++)
            {
                newGenotype[i] = Random.GetRandomDouble() > mutationProbability
                    ? genotype[i]
                    : BLXAlpha.GetBLXAlphaValue(minRanges[i], maxRanges[i], alpha);
            }

            return newGenotype.ToImmutableList();
        }

        /// <summary>
        /// Return a new genotype in which a BLX-α mutation has been performed on only 1 gene (with a certain probability).
        /// </summary>
        /// <param name="genotype">The genotype we want to mutate.</param>
        /// <param name="minRanges">An array containing the minimum ranges.</param>
        /// <param name="maxRanges">An array containing the maximum ranges.</param>
        /// <param name="alpha">The α value.</param>
        /// <param name="mutationProbability">The probability that a mutation occurs to every single gene</param>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull]
        public static ImmutableList<double>  BLXAlphaSinglepointMutation(
            [NotNull] ImmutableList<double>  genotype,
            [NotNull] ImmutableList<double>  minRanges,
            [NotNull] ImmutableList<double> maxRanges,
            double alpha,
            double mutationProbability)
        {
            ArgumentHelper.AssertNotNull(genotype, nameof(genotype));
            ArgumentHelper.AssertNotNull(minRanges, nameof(minRanges));
            ArgumentHelper.AssertNotNull(maxRanges, nameof(maxRanges));

            double[] newGenotype = genotype.ToArray();

            if (Random.GetRandomDouble() <= mutationProbability)
            {
                int randomIndex = Random.GetRandomIndex(newGenotype);
                newGenotype[randomIndex] = BLXAlpha.GetBLXAlphaValue(minRanges[randomIndex], maxRanges[randomIndex], alpha);
            }

            return newGenotype.ToImmutableList();
        }
    }
}
