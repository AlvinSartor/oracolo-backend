﻿using System;
using Fugro.Collections;
using Fugro.GeneticAlgorithms.Util;
using JetBrains.Annotations;
using Random = Fugro.GeneticAlgorithms.Util.Random;

namespace Fugro.GeneticAlgorithms.Operators
{
    /// <summary>
    /// The crossover algorithms are used to obtain a child genome from 2 genome parents, allowing the EXPLOITATION of the search space.
    ///
    /// <para>These are standard crossover algorithms.
    ///
    /// As these algorithms are only for certain types,
    /// the user may write a personalised algorithm that fits better with the problem.
    /// </para>
    /// </summary>
    public static class Crossover
    {
        /// <summary>
        /// Gets a new genotype using the BLX-αβ algorithm.
        /// </summary>
        /// <param name="bestGenotype">The genotype of the most fit parent.</param>
        /// <param name="worseGenotype">The genotype of the least fit parent.</param>
        /// <param name="alpha">The α value.</param>
        /// <param name="beta">The β value.</param>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull]
        public static ImmutableList<double> BLXAlphaBetaCrossover(
            [NotNull] ImmutableList<double> bestGenotype,
            [NotNull] ImmutableList<double> worseGenotype,
            double alpha,
            double beta)
        {
            ArgumentHelper.AssertNotNull(worseGenotype, nameof(worseGenotype));
            ArgumentHelper.AssertNotNull(bestGenotype, nameof(bestGenotype));

            var genotype = new double[bestGenotype.Count];

            for (var i = 0; i < bestGenotype.Count; i++)
            {
                genotype[i] = BLXAlpha.GetBLXAlphaBetaValue(bestGenotype[i], worseGenotype[i], alpha, beta);
            }

            return genotype.ToImmutableList();
        }

        /// <summary>
        /// Gets a new genotype using the BLX-α algorithm.
        /// </summary>
        /// <param name="bestGenotype">The genotype of the most fit parent.</param>
        /// <param name="worseGenotype">The genotype of the least fit parent.</param>
        /// <param name="alpha">The α value.</param>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull]
        public static ImmutableList<double> BLXAlphaCrossover(
            [NotNull] ImmutableList<double> bestGenotype,
            [NotNull] ImmutableList<double> worseGenotype,
            double alpha)
        {
            ArgumentHelper.AssertNotNull(worseGenotype, nameof(worseGenotype));
            ArgumentHelper.AssertNotNull(bestGenotype, nameof(bestGenotype));

            return BLXAlphaBetaCrossover(bestGenotype, worseGenotype, alpha, alpha);
        }

        /// <summary>
        /// Gets a new genotype using the a single-point binary crossover algorithm.
        /// </summary>
        /// <param name="parent1">The first parent.</param>
        /// <param name="parent2">The second parent.</param>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull]
        public static ImmutableList<T> BinaryCrossover<T>([NotNull] ImmutableList<T> parent1, [NotNull] ImmutableList<T> parent2)
        {
            ArgumentHelper.AssertNotNull(parent1, nameof(parent1));
            ArgumentHelper.AssertNotNull(parent2, nameof(parent2));

            int crossoverPoint = Random.GetRandomIndex(parent1);
            var newGenome = new T[parent1.Count];

            for (var i = 0; i < newGenome.Length; i++)
            {
                newGenome[i] = i <= crossoverPoint ? parent1[i] : parent2[i];
            }

            return newGenome.ToImmutableList();
        }

        /// <summary>
        /// Gets a new genotype using the a multi-point binary crossover algorithm.
        /// </summary>
        /// <param name="parent1">The first parent.</param>
        /// <param name="parent2">The second parent.</param>
        /// <remarks> Axiomatic complexity: O(n) </remarks>
        [NotNull]
        public static ImmutableList<T> MultiPointBinaryCrossover<T>([NotNull] ImmutableList<T> parent1, [NotNull] ImmutableList<T> parent2)
        {
            ArgumentHelper.AssertNotNull(parent1, nameof(parent1));
            ArgumentHelper.AssertNotNull(parent2, nameof(parent2));

            int crossoverPoint1 = Random.GetRandomIndex(parent1);
            int crossoverPoint2 = Random.GetRandomIndex(parent1);

            int minPoint = Math.Min(crossoverPoint1, crossoverPoint2);
            int maxPoint = Math.Max(crossoverPoint1, crossoverPoint2);

            var newGenome = new T[parent1.Count];

            for (var i = 0; i < newGenome.Length; i++)
            {
                newGenome[i] = i < minPoint || i > maxPoint ? parent1[i] : parent2[i];
            }

            return newGenome.ToImmutableList();
        }

    }
}