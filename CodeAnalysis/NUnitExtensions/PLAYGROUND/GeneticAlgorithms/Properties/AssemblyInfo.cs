﻿using System.Reflection;
using Fugro;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Fugro.GeneticAlgorithms")]
[assembly: AssemblyDescription("Library containing Genetic Algorithms.")]

[assembly: RecognizedWord("BLX")]
