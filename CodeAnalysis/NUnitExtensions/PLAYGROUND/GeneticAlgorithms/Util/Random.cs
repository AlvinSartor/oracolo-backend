﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Util
{
    /// <summary>
    /// Class used to handle random values.
    /// </summary>
    public static class Random
    {
        private static System.Random s_Random;

        [NotNull]
        private static System.Random GetRandom()
        {
            return s_Random ?? (s_Random = new System.Random());
        }

        /// <summary>
        /// Get a random double in the range [0 inclusive; 1 exclusive].
        /// </summary>
        public static double GetRandomDouble()
        {
            return GetRandom().NextDouble();
        }

        /// <summary>
        /// Get a random boolean.
        /// </summary>
        public static bool GetRandomBool()
        {
            return GetRandomDouble() >= 0.5d;
        }

        /// <summary>
        /// Gets a random double in the range [min; max], both inclusive.
        /// </summary>
        /// <param name="min">The minimum value.</param>
        /// <param name="max">The maximum value.</param>
        public static double GetRandomInRange(double min, double max)
        {
            double range = max - min;
            if (double.IsInfinity(range))
            {
                throw new ArgumentException("Range overflow");
            }

            double randomNumber = GetRandomDouble();

            return min + randomNumber * range;
        }

        /// <summary>
        /// Gets a random integer in the range [min inclusive; max exclusive].
        /// </summary>
        /// <param name="min">The minimum value.</param>
        /// <param name="max">The maximum value.</param>
        public static int GetRandomInRange(int min, int max)
        {
            return GetRandom().Next(min, max);
        }

        /// <summary>
        /// Gets a random index in the list.
        /// </summary>
        /// <param name="list">The list</param>
        public static int GetRandomIndex<T>([NotNull] IList<T> list)
        {
            ArgumentHelper.AssertNotNull(list, nameof(list));
            return GetRandomInRange(0, list.Count);
        }

        /// <summary>
        /// Gets a random index in the list.
        /// </summary>
        /// <param name="list">The list</param>
        [NotNull]
        public static T GetRandomElement<T>([NotNull] IList<T> list)
        {
            ArgumentHelper.AssertNotNull(list, nameof(list));
            return list[GetRandomIndex(list)];
        }

    }
}
