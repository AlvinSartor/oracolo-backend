﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fugro.GeneticAlgorithms.Util
{
    public static class Extensions
    {
        /// <summary>
        /// Calculates a cumulative value for an enumerable of doubles
        /// </summary>
        /// <param name="numbers">Numbers to sum</param>
        /// <returns>Cumulative sum</returns>
        [NotNull]
        public static IEnumerable<double> CumulativeSum([NotNull] this IEnumerable<double> numbers)
        {
            ArgumentHelper.AssertNotNull(numbers, nameof(numbers));
            double sum = 0;

            foreach (var number in numbers)
            {
                sum = sum + number;
                yield return sum;
            }
        }

        /// <summary>
        /// Returns the maximum element of a collection using a function to calculate its value.
        /// <para> If there are multiple occurrences of maximum element, the first one will be returned.</para>
        /// </summary>
        /// <typeparam name="T">The type of object in the list.</typeparam>
        /// <param name="collection">The collection of objects.</param>
        /// <param name="valueExtractor">The function to get the value.</param>
        /// <exception cref="ArgumentException">collection is null</exception>
        /// <exception cref="ArgumentException">the collection is empty</exception>
        /// <exception cref="ArgumentException">valueExtractor is null</exception>
        public static T MaxBy<T>([NotNull] this ICollection<T> collection, [NotNull] Func<T, double> valueExtractor)
        {
            ArgumentHelper.AssertNotNull(collection, nameof(collection));
            ArgumentHelper.AssertNotEmpty(collection, nameof(collection));
            ArgumentHelper.AssertNotNull(valueExtractor, nameof(valueExtractor));

            T currentMax = default(T);
            double currentMaxValue = double.MinValue;

            foreach (var item in collection)
            {
                double value = valueExtractor(item);
                if (value > currentMaxValue)
                {
                    currentMax = item;
                    currentMaxValue = value;
                }
            }

            return currentMax;
        }

        /// <summary>
        /// Returns the minimum element of a collection using a function to calculate its value.
        /// <para> If there are multiple occurrences of minimum element, the first one will be returned.</para>
        /// </summary>
        /// <typeparam name="T">The type of object in the collection.</typeparam>
        /// <param name="collection">The collection of objects.</param>
        /// <param name="valueExtractor">The function to get the value.</param>
        /// <exception cref="ArgumentException">collection is null</exception>
        /// <exception cref="ArgumentException">the collection is empty</exception>
        /// <exception cref="ArgumentException">valueExtractor is null</exception>
        public static T MinBy<T>([NotNull] this ICollection<T> collection, [NotNull] Func<T, double> valueExtractor)
        {
            ArgumentHelper.AssertNotNull(collection, nameof(collection));
            ArgumentHelper.AssertNotEmpty(collection, nameof(collection));
            ArgumentHelper.AssertNotNull(valueExtractor, nameof(valueExtractor));

            T currentMin = default(T);
            double currentMinValue = double.MaxValue;

            foreach (var item in collection)
            {
                double value = valueExtractor(item);
                if (value < currentMinValue)
                {
                    currentMin = item;
                    currentMinValue = value;
                }
            }

            return currentMin;
        }

        /// <summary>
        /// Returns a shuffled copy of the list passed as argument.
        /// </summary>
        /// <typeparam name="T">The type of the list.</typeparam>
        /// <param name="list">The list.</param>
        [NotNull, ItemCanBeNull]
        public static IList<T> ShuffledCopy<T>([NotNull, ItemCanBeNull] this IList<T> list)
        {
            ArgumentHelper.AssertNotNull(list, nameof(list));

            var newList = new List<T>(list);

            for (int i = 0; i < list.Count; i++)
            {
                newList.Swap(i, Random.GetRandomIndex(newList));
            }

            return newList;
        }

        /// <summary>
        /// Swaps the elements of the specified list.
        /// </summary>
        /// <typeparam name="T">List type.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="index1">The index of the first element.</param>
        /// <param name="index2">The index of the second element.</param>
        public static void Swap<T>([NotNull] this IList<T> list, int index1, int index2)
        {
            ArgumentHelper.AssertNotNull(list, nameof(list));
            ArgumentHelper.AssertRange(index1, nameof(index1), 0, list.Count - 1);
            ArgumentHelper.AssertRange(index2, nameof(index2), 0, list.Count - 1);

            T keeper = list[index1];
            list[index1] = list[index2];
            list[index2] = keeper;
        }

    }
}
