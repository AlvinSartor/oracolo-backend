﻿using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Util.NUnit
{
    [TestFixture]
    internal sealed class BLXAlphaFixture
    {
        [Test]
        public void BLXAlphaValueIsIntoExpectedInterval()
        {
            const double a = 60;
            const double b = 80;
            const double alpha = 0.2;

            const double min = a - (b - a) * alpha;
            const double max = b + (b - a) * alpha;

            for (var i = 0; i < 1000; i++)
            {
                Assert.That(BLXAlpha.GetBLXAlphaValue(a, b, alpha), Is.InRange(min, max));
                Assert.That(BLXAlpha.GetBLXAlphaValue(b, a, alpha), Is.InRange(min, max));
            }
        }

        [Test]
        public void BLXAlphaBetaValueIsIntoExpectedInterval()
        {
            const double a = 60;
            const double b = 80;
            const double alpha = 0.2;
            const double beta = 0.1;

            double min = a - (b - a) * alpha;
            double max = b + (b - a) * beta;

            for (var i = 0; i < 1000; i++)
            {
                Assert.That(BLXAlpha.GetBLXAlphaBetaValue(a, b, alpha, beta), Is.InRange(min, max));
            }

            min = a - (b - a) * beta;
            max = b + (b - a) * alpha;

            for (var i = 0; i < 1000; i++)
            {
                Assert.That(BLXAlpha.GetBLXAlphaBetaValue(b, a, alpha, beta), Is.InRange(min, max));
            }
        }
    }
}
