﻿using NUnit.Framework;

namespace Fugro.GeneticAlgorithms.Util.NUnit
{
    [TestFixture]
    internal sealed class RandomFixture
    {
        [Test]
        public void RandomDoubleFallsInsideInterval()
        {
            const double min = 8.6;
            const double max = 12.9;

            for (var i = 0; i < 1000; i++)
            {
                Assert.That(Random.GetRandomInRange(min, max), Is.InRange(min, max));
            }
        }

        [Test]
        public void RandomIntegerFallsInsideInterval()
        {
            const int min = 8;
            const int max = 22;

            for (var i = 0; i < 1000; i++)
            {
                Assert.That(Random.GetRandomInRange(min, max), Is.InRange(min, max));
            }
        }

    }
}
