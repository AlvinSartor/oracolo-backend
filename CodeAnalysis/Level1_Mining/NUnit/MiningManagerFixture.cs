using System.Threading.Tasks;
using NUnit.Framework;

namespace CodeAnalysis.Level1_Mining.NUnit
{
    [TestFixture]
    internal sealed class MiningManagerFixture
    {
        private MiningManager m_MiningManager;

        [SetUp]
        public void SetUp()
        {
            m_MiningManager = new MiningManager();
        }

        [TearDown]
        public void TearDown()
        {
            m_MiningManager.Dispose();
        }

        [Test]
        public async Task Test1()
        {
            string filePath = $@"D:\Development\Projects\Starfix NG\Source\Fugro\PlugIns\Codecs\Measurements\CodaOut\CodaOutEncoderProperties.cs";
            await m_MiningManager.LoadFromFile(filePath);
            await m_MiningManager.LoadFromFile(filePath);
            await m_MiningManager.LoadFromFile(filePath);
        }
    }
}