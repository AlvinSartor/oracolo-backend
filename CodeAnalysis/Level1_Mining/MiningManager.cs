﻿using System;
using Extensions;
using System.Linq;
using JetBrains.Annotations;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using CodeAnalysis.Level0_Mapping;
using System.Collections.Immutable;
using System.Diagnostics;
using Microsoft.CodeAnalysis.MSBuild;
using CodeAnalysis.Level1_Mining_DataStructure;
using CodeAnalysis.Level0_Mapping_DataStructure.Interfaces;
using System.Reflection;

namespace CodeAnalysis.Level1_Mining
{
    /// <summary>
    /// This is the fusion point between the knowledge about the system (MappingManager)
    /// and the MSWorkspace (providing Solution/Projects/Compilations)
    /// </summary>
    internal sealed class MiningManager : IDisposable
    {
        [NotNull] private readonly MappingManager m_MappingManager;
        [NotNull] private readonly MSBuildWorkspace m_Workspace;

        [NotNull] private readonly Dictionary<ICsprojDescriptor, CompilationUnit> m_CompilationsCache;
        //[NotNull] private readonly ImmutableDictionary<ICsprojDescriptor, Project> m_ProjectMapping;

        /// <summary>
        /// Initializes a new instance of the <see cref="MiningManager"/> class.
        /// </summary>
        public MiningManager()
        {
            m_CompilationsCache = new Dictionary<ICsprojDescriptor, CompilationUnit>();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            m_MappingManager = new MappingManager();

            stopwatch.Stop();
            Console.WriteLine($"Mapping manager instantiation: {stopwatch.ElapsedMilliseconds} ms");

            stopwatch.Restart();

            m_Workspace = MSBuildWorkspace.Create();
            //Solution solution = m_Workspace.OpenSolutionAsync(MappingManager.ProjectSlnPath).Result;

            stopwatch.Stop();
            Console.WriteLine($"Workspace instantiation: {stopwatch.ElapsedMilliseconds} ms");

            //m_ProjectMapping = MapProjects(m_MappingManager.SlnDescriptor, solution);
        }

        /// <summary>
        /// Loads the specified file path, returning the compilation unit of its whole assembly.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The compilation unit of the specified file.</returns>
        public async Task<CompilationUnit> LoadFromFile([NotNull] string filePath)
        {
            filePath.AssertNotNull(nameof(filePath));
            ICsprojDescriptor csproj = m_MappingManager.SlnDescriptor.GetCsprojOf(filePath);

            return csproj != null
                ? await Load(csproj)
                : null;
        }

        /// <summary>
        /// Loads the specified assembly, returning its compilation.
        /// </summary>
        /// <param name="assemblyName">The assemblyName.</param>
        /// <returns>The compilation unit of the specified assembly.</returns>
        public async Task<CompilationUnit> LoadFromAssembly([NotNull] string assemblyName)
        {
            assemblyName.AssertNotNull(nameof(assemblyName));

            return m_MappingManager.SlnDescriptor.Csprojs.TryGetValue(assemblyName, out ICsprojDescriptor csproj)
                ? await Load(csproj)
                : null;
        }

        private async Task<CompilationUnit> Load([NotNull] ICsprojDescriptor csproj)
        {
            if (!m_CompilationsCache.ContainsKey(csproj))
            {
                Stopwatch s = new Stopwatch();
                s.Start();

                Project project = await m_Workspace.OpenProjectAsync(csproj.FilePath);

                s.Stop();
                Console.WriteLine($"Time for getting project: {s.ElapsedMilliseconds} ms");

                s.Restart();

                Compilation compilation = await project.GetCompilationAsync();

                s.Stop();
                Console.WriteLine($"Time for getting compilation: {s.ElapsedMilliseconds} ms");

                s.Restart();

                m_CompilationsCache.Add(csproj, new CompilationUnit(compilation, csproj));

                s.Stop();
                Console.WriteLine($"Time for getting Compilation Unit: {s.ElapsedMilliseconds} ms");
            }

            return m_CompilationsCache[csproj];
        }


        //[NotNull]
        //private static ImmutableDictionary<ICsprojDescriptor, Project> MapProjects(
        //    [NotNull] ISlnDescriptor slnDescriptor, [NotNull] Solution solution)
        //{
        //    ImmutableList<Project> projects = solution.Projects.ToImmutableList();

        //    if (slnDescriptor.Csprojs.Count != projects.Count)
        //    {
        //        throw new ArgumentException("The project from the MappingManager do not match with the projects from the Workspace.");
        //    }

        //    return projects.ToImmutableDictionary(p => slnDescriptor.Csprojs[p.AssemblyName], p => p);
        //}

        [NotNull]
        public ImmutableList<string> WorkspaceExceptions => m_Workspace.Diagnostics.Select(d => d.Message).ToImmutableList();

        /// <inheritdoc />
        public void Dispose()
        {
            m_Workspace.Dispose();
        }


        internal static class ReferencesLoader
        {
            /// <summary>
            /// Gets the base references.
            /// </summary>
            [NotNull]
            public static ImmutableList<string> GetBaseReferences()
            {
                return new List<string>
                {
                    typeof(object).GetTypeInfo().Assembly.Location,
                }.ToImmutableList();
            }

            /// <summary>
            /// Creates the references from file.
            /// </summary>
            /// <param name="referencesPaths">The references paths.</param>
            [NotNull]
            public static ImmutableList<PortableExecutableReference> CreateReferencesFromFiles([NotNull] IEnumerable<string> referencesPaths)
            {
                referencesPaths.AssertNotNull(nameof(referencesPaths));
                return referencesPaths.Select(x => MetadataReference.CreateFromFile(x)).ToImmutableList();
            }
        }
    }
}